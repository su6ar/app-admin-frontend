/* global moment:false */
(function() {
  'use strict';

  /* eslint-disable */
  angular
    .module('minotaur')
    .constant('moment', moment)
    .constant('_', window._)
    .constant('SERVER', 'https://api.kominexpres.cz')
    .constant('REST_PREFIX', '/app/v1')
    .constant('L', L)
    .constant('PHP_PATH', 'https://admin.kominexpres.cz/app/components/modules/fileupload/')
    .constant('PHP_IMAGE_UPLOAD_FILENAME', 'ImageUpload.php')
    .constant('PHP_IMAGE_CHANGE_CATEGORY_FILENAME', 'ImageChangeCategory.php')
    .constant('PHP_FILE_UPLOAD_FILENAME', 'FileUpload.php')
    .constant('IMAGE_PATH', 'https://lifes.kominexpres.cz/uploads/')
    .constant('FILE_PATH', 'https://lifes.kominexpres.cz/uploads/')
    .constant('IMAGE_SMALLEST', '_6')
    .constant('IMAGE_ORIGINAL', '_1')
    .constant('IMAGE_MEDIUM', '_4')
    .constant('MIME_TYPES', [{
      type: 'MIME_TYPE_CSV',
      marking: 'csv',
    }, {
      type: 'MIME_TYPE_DOC',
      marking: 'msword',
    }, {
      type: 'MIME_TYPE_PDF',
      marking: 'pdf',
    }, {
      type: 'MIME_TYPE_TXT',
      marking: 'plain',
    }, {
      type: 'MIME_TYPE_RTF',
      marking: 'rtf',
    }, {
      type: 'MIME_TYPE_MDB',
      marking: 'vnd.ms-access',
    }, {
      type: 'MIME_TYPE_XLS',
      marking: 'vnd.ms-excel',
    }, {
      type: 'MIME_TYPE_PPT',
      marking: 'vnd.ms-powerpoint',
    }, {
      type: 'MIME_TYPE_ODS',
      marking: 'vnd.oasis.opendocument.spreadsheet',
    }, {
      type: 'MIME_TYPE_ODT',
      marking: 'vnd.oasis.opendocument.text',
    }, {
      type: 'MIME_TYPE_PPTX',
      marking: 'vnd.openxmlformats-officedocument.presentationml.presentation',
    }, {
      type: 'MIME_TYPE_XLSX',
      marking: 'vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    }, {
      type: 'MIME_TYPE_DOCX',
      marking: 'vnd.openxmlformats-officedocument.wordprocessingml.document',
    }])
    .constant('ALERT_OPTIONS', {
      colors: [
        {name:'success'},
        {name:'warning'},
        {name:'danger'},
        {name:'info'},
        {name:'default'},
        {name:'theme'}
      ],
      durations: [
        {name:'never close', value: 9999*9999},
        {name:'1 second', value: 1000},
        {name:'5 seconds', value: 5000},
        {name:'10 seconds', value: 10000}
      ],
      icons: [
        {name: 'none', value: ''},
        {name: 'warning', value: 'fa-warning'},
        {name: 'check', value: 'fa-check'},
        {name: 'user', value: 'fa-user'}
      ],
      msg: 'Place alert text here...'
    });
  /*esling-enable*/

})();
