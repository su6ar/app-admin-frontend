(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ManufacturerAddController', ManufacturerAddController);

  /** @ngInject */
  function ManufacturerAddController(ManufacturerService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');

    vm.add = function () {
      ManufacturerService.post(vm.murName).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
