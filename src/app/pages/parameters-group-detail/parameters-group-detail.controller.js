(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ParametersGroupDetailController', ParametersGroupDetailController);

  /** @ngInject */
  function ParametersGroupDetailController($stateParams, ParameterGroupsService, ParametersService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = {};
    vm.item.parameters = [];
    vm.item.selected = [];

    ParameterGroupsService.getOne($stateParams.pgp_id).then(function (response) {
      vm.item.pgp_id = response.data.pgp_id;
      vm.item.pgp_name = response.data.pgp_name;
      vm.item.selected = response.data.parameters;
    });

    ParametersService.get().then(function (response) {
      ParametersService.setList(response.data);
      vm.item.parameters = ParametersService.getList();
    });

    vm.update = function () {
      var parameters = [];
      angular.forEach(vm.item.selected, function (value) {
        parameters.push(value.prr_id);
      });

      var data = {
        pgp_name: vm.item.pgp_name,
        parameters: parameters
      };

      ParameterGroupsService.put(data, $stateParams.pgp_id).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    }
  }


})();

