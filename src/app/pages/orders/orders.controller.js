(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('OrdersController', OrdersController);

  /** @ngInject */
  function OrdersController($filter, $uibModal, OrdersService, $rootScope) {
    var $translation = $filter('translate');
    var vm = this;
    vm.statusList = OrdersService.getStatusList();
    vm.statuses = [];
    vm.amr_deleted_order_show = $rootScope.currentUser.amr_deleted_order_show;
    vm.selectedAll = false;
    vm.selectedCeyId = null;
    vm.items = [];
    vm.itemsSelected = [];
    vm.limit = { options: [
      {id: 1, name: 10},
      {id: 2, name: 25},
      {id: 3, name: 50},
      {id: 4, name: 100}
    ], selected: {id: 1, name: 10}};
    vm.offset = 0;
    vm.page = { current: 1, total: 1 };
    vm.search = {
      string: "",
      strings: []
    };
    vm.sort = {
      columns: [
        {name: "id", sort: 2},
        {name: "customer", sort: 0},
        {name: "is_company", sort: 0},
        {name: "status", sort: 0},
        {name: "deleted_order", sort: 0},
        {name: "poy_name", sort: 0},
        {name: "cellnumber", sort: 0},
        {name: "order_total", sort: 0}
      ],
      options: [
        "sorting", "sorting_asc", "sorting_desc"
      ],
      active: 0
    };

    angular.forEach(vm.statusList, function (value) {
      vm.statuses.push(value.charAt(0).toUpperCase() + value.slice(1));
    });

    vm.selectAll = function () {
      angular.forEach(vm.items, function(order) {
        order.selected = vm.selectedAll;
      });
    };

    vm.searchData = function (index) {
      var value = index !== -1 ? vm.search[vm.sort.columns[index].name].toString() : vm.search.string.toString();
      var a = "";
      if (index !== -1) {
        if (value.length === 0) {
          vm.search.strings = vm.search.strings.filter(function (item) {
            return item !== index;
          });
        } else if (vm.search.strings.indexOf(index) === -1) {
          vm.search.strings.push(index);
        }
      }
      vm.items = OrdersService.getList();
      for (var i = 0; i < vm.search.strings.length; i++) {
        vm.items = vm.items.filter(function (el) {
          if (el[vm.sort.columns[vm.search.strings[i]].name] === null) {
            return a.indexOf(vm.search[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase()) > -1;
          } else {
            return el[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase().indexOf(vm.search[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase()) > -1;
          }
        });
      }
      if (index === -1 || vm.search.string.length > 0) {
        vm.items = vm.items.filter(function (el) {
          for (var j = 0; j < vm.sort.columns.length; j++) {
            if (el[vm.sort.columns[j].name] === null) {
              if (a.indexOf(vm.search.string.toString().toLowerCase()) > -1) {
                return true;
              }
            } else {
              if (el[vm.sort.columns[j].name].toString().toLowerCase().indexOf(vm.search.string.toString().toLowerCase()) > -1) {
                return true;
              }
            }
          }
          return false;
        });
      }
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.items.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.updateSort(vm.sort.active, false);
      if (vm.search.strings.length > 0 || vm.search.string.length > 0) {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS') + ' (' + $translation('Global.Table.FILTERED_FROM') + ' ' + OrdersService.getList().length + ' ' + $translation('Global.Table.RECORDS') + ')';
      } else {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS');
      }
    };

    vm.updateSort = function (index, change) {
      vm.sort.active = index;
      if (change) {
        if (vm.sort.columns[index].sort === 1) {
          vm.sort.columns[index].sort = 2;
        } else {
          vm.sort.columns[index].sort = 1;
        }
        for (var i = 0; i < vm.sort.columns.length; i++) {
          if (index === i) continue;
          vm.sort.columns[i].sort = 0;
        }
      }
      vm.items.sort(function(a,b) {
        var nameA, nameB;
        if (vm.sort.columns[index].sort===1) {
          if (index===0||index===2||index===4||index===6||index===7) {
            return a[vm.sort.columns[index].name] - b[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name] === null ? "NULL" : a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name] === null ? "NULL" : b[vm.sort.columns[index].name].toUpperCase();
            return nameA.localeCompare(nameB);
          }
        } else {
          if (index===0||index===2||index===4||index===6||index===7) {
            return b[vm.sort.columns[index].name] - a[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name] === null ? "NULL" : a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name] === null ? "NULL" : b[vm.sort.columns[index].name].toUpperCase();
            return nameB.localeCompare(nameA);
          }
        }
      });
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    vm.updateData = function () {
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.items.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.offset = (vm.limit.selected.name*vm.page.current)-vm.limit.selected.name;
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    vm.itemSelected = function(offset, limit) {
      return vm.items.filter(function(value, index) {
        return index >= offset && index < offset+limit;
      });
    };

    OrdersService.get().then(function (response) {
      OrdersService.setList(response.data);
      vm.items = OrdersService.getList();
      vm.page.current = 1;
      vm.updateSort(vm.sort.active, false);
      vm.updateData();
      vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS');

      angular.forEach(vm.items, function (value, key) {
        vm.items[key].ord_date = new Date(value.ord_date);
      });
    });

    vm.modalOpen = function (note, title) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_default_tmpl.html',
        controller: 'ModalInstanceController',
        controllerAs: 'modal',
        size: 'sm',
        resolve: {
          items: function () {
            return note;
          },
          title: function () {
            return title;
          }
        }
      });
    };

    vm.modalDeleteSingle = function (id) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_single_tmpl.html',
        controller: 'ModalsDeleteSingleController',
        controllerAs: 'modal',
        resolve: {
          id: function () {
            return id;
          },
          service: function () {
            return OrdersService;
          },
          ceyId: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    vm.modalDeleteMultiple = function () {
      var orders = getSelectedOrders();

      if (orders.length === 0) {
        return false;
      }
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_multiple_tmpl.html',
        controller: 'ModalsDeleteMultipleController',
        controllerAs: 'modal',
        resolve: {
          items: function () {
            return orders;
          },
          service: function () {
            return OrdersService;
          },
          ceyId: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    var getSelectedOrders = function () {
      var orders = [];
      for (var i = 0; i < vm.items.length; i++)
      {
        if (vm.items[i].selected) {
          orders.push({
            id: vm.items[i].id,
            index: i
          });
        }
      }
      return orders;
    };

    vm.modalChangeMultipleEmail = function (status) {
      var orders = getSelectedOrders();

      if (orders.length === 0) {
        return false;
      }

      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_change_multiple_email_tmpl.html',
        controller: 'ModalsChangeMultipleEmailController',
        controllerAs: 'modal',
        resolve: {
          status: function () {
            return status;
          },
          items: function () {
            return orders;
          },
          service: function () {
            return OrdersService;
          },
          attri: function () {
            return "status";
          }
        }
      });
    };
  }


})();
