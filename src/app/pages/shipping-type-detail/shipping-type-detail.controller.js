(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ShippingTypeDetailController', ShippingTypeDetailController);

  /** @ngInject */
  function ShippingTypeDetailController($stateParams, ShippingTypeService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');

    ShippingTypeService.getOne($stateParams.id).then(function (response) {
      vm.dtaName = response.data.dta_name;
    });

    vm.update = function () {
      ShippingTypeService.put($stateParams.id, vm.dtaName).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

