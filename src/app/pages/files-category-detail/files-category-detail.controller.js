(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('FilesCategoryDetailController', FilesCategoryDetailController);

  /** @ngInject */
  function FilesCategoryDetailController($stateParams, FileCategoriesService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');

    FileCategoriesService.getOne($stateParams.id).then(function (response) {
      var data = response.data;
      vm.fcyName = data.fcy_name;
      vm.fcyId = data.fcy_id;
    });

    vm.update = function () {
      FileCategoriesService.put(vm.fcyId, vm.fcyName).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS'));
      }, function (response) {
        var message;
        try {
          message = response.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

