(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('OrdersDetailController', OrdersDetailController);

  /** @ngInject */
  function OrdersDetailController($stateParams, OrdersService, $filter, $rootScope) {
    var $translation = $filter('translate');
    var vm = this;
    vm.order = null;

    OrdersService.getOne($stateParams.id).then(function (response) {
      angular.forEach(response.data.order_histories, function (value, key) {
          response.data.order_histories[key].date_from = new Date(response.data.order_histories[key].date_from);
      });
      response.data.order_info.odr_date = new Date(response.data.order_info.odr_date);
      angular.forEach(response.data.order_notes, function (value, key) {
        response.data.order_notes[key].one_date = new Date(response.data.order_notes[key].one_date);
      });
      vm.order = response.data;
    });

    vm.postNote = function postNote() {
      OrdersService.postNote($rootScope.currentUser.act_id, vm.content, vm.title, $stateParams.id).then(function (response) {
        vm.alert = {
          msg: $translation('Global.Alert.SUCCESS') + '!',
          type: 'success',
          timeout: 9999*9999,
          icon: '',
          closeable: false,
          closeall: true,
          focus: true
        };
        angular.forEach(response.data, function (value, key) {
          response.data[key].one_date = new Date(response.data[key].one_date);
        });
        vm.order.order_notes = response.data;
      }, function (error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        vm.alert = {
          msg: message,
          type: 'danger',
          timeout: 9999*9999,
          icon: '',
          closeable: false,
          closeall: true,
          focus: true
        };
      });
    };
  }


})();
