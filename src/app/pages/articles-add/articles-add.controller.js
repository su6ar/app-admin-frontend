(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ArticlesAddController', ArticlesAddController);

  /** @ngInject */
  function ArticlesAddController(ArticlesService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = {};

    vm.add = function () {
      var data = {
        ate_name: vm.item.ate_name,
        ahy_content: vm.item.ahy_content
      };

      ArticlesService.post(data).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
