(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('FilesAddController', FilesAddController);

  /** @ngInject */
  function FilesAddController(FileUploader, PHP_PATH, PHP_FILE_UPLOAD_FILENAME, MIME_TYPES, toastr, $filter, FileCategoriesService, FilesService, TokenService) {
    var vm = this;
    var uploader = vm.uploader = new FileUploader({
      //url: 'https://admin.kominexpres.cz:3001/fileupload.php'
      url: PHP_PATH + PHP_FILE_UPLOAD_FILENAME
    });
    var $translation = $filter('translate');

    FileCategoriesService.get().then(function (response) {
      FileCategoriesService.setList(response.data);
      vm.categories = FileCategoriesService.getList();
      vm.selected = { fcy_name: vm.categories[0].fcy_name, fcy_id: vm.categories[0].fcy_id };
    });

    // FILTERS

    uploader.filters.push({
      name: 'customFilter',
      fn: function() {
        var vm = this;
        return vm.queue.length < 10;
      }
    });

    uploader.filters.push({
      name: 'fileFilter',
      fn: function(item /*{File|FileLikeObject}, options*/) {
        var result = false;
        var type = item.type.slice(item.type.lastIndexOf('/') + 1);
        for (var i = 0; i < MIME_TYPES.length; i++) {
          if (MIME_TYPES[i].marking === type) {
            result = true;
            break;
          }
        }
        return result;
      }
    });

    // CALLBACKS

    /*uploader.onWhenAddingFileFailed = function(item, filter, options) {
     // $log.info('onWhenAddingFileFailed', item, filter, options);
    };*/
    uploader.onAfterAddingFile = function(fileItem) {
      fileItem.selected = vm.selected;
    };
   /* uploader.onAfterAddingAll = function(addedFileItems) {
     // $log.info('onAfterAddingAll', addedFileItems);
    };*/
    uploader.onBeforeUploadItem = function(item) {
      item.formData = [{
        fcy_id: item.selected.fcy_id,
        fcy_name: item.selected.fcy_name,
        token: TokenService.isToken()
      }];
      //$log.info('onBeforeUploadItem', item);
    };
    /*uploader.onProgressItem = function(fileItem, progress) {
     // $log.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
     // $log.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      //$log.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
     // $log.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
     // $log.info('onCancelItem', fileItem, response, status, headers);
    };*/
    uploader.onCompleteItem = function(fileItem, response) {
      //$log.info('onCompleteItem', fileItem, response, status, headers);
      if (response.status_code === 200) {
        FilesService.post(response.fcy_id, response.fie_path, response.fie_name, response.fie_type, response.fie_size).then(function () {
          toastr.success($translation('Global.Alert.SUCCESS') + '! ');
        }, function (error) {
          var message;
          try {
            message = error.data.message.toString();
          } catch(e) {
            message = $translation('Global.Error.UNKNOWN')
          }
          vm.fileItemError(fileItem);
          toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
        });
      } else {
        vm.fileItemError(fileItem);
        toastr.error(response.response, $translation('Global.Alert.ERROR') + '! ');
      }
    };
    /*uploader.onCompleteAll = function() {
      //$log.info('onCompleteAll');
    };*/

    vm.fileItemError = function (fileItem) {
      fileItem.isError = true;
      fileItem.isSuccess = false;
      fileItem.isUploaded = false;
      fileItem.progress = 0;
      fileItem.uploader.progress = 0;
    };
  }

})();
