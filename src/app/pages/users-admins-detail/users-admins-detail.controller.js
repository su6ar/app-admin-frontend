(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('UsersAdminsDetailController', UsersAdminsDetailController);

  /** @ngInject */
  function UsersAdminsDetailController($stateParams, UsersAdminsService) {
    var vm = this;
    vm.item = {};

    UsersAdminsService.getOne($stateParams.act_id).then(function (response) {
      vm.item = response.data;
    });
  }


})();

