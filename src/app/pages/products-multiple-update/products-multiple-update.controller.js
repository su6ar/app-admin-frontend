(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ProductsMultipleUpdateController', ProductsMultipleUpdateController);

  /** @ngInject */
  function ProductsMultipleUpdateController(ProductsService, localStorageService, $state, $stateParams, ParameterGroupsService, toastr, ManufacturerService, $filter, ImagesService, IMAGE_MEDIUM, IMAGE_PATH, IMAGE_ORIGINAL, ImageCategoriesService, FilesService, CategoriesService, DTOptionsBuilder, DTColumnDefBuilder, PaymentsService, ShippingService, MetaKeywordsService, GroupsService) {
    var PRODUCT_UPDATE_IDS = "productUpdateIds";
    if ((angular.isUndefined($stateParams.ids) || $stateParams.ids === null) && (angular.isUndefined(localStorageService.get(PRODUCT_UPDATE_IDS, "localStorage")) || localStorageService.get(PRODUCT_UPDATE_IDS, "localStorage") === null)) {
      $state.go("error.page404");
    }
    var $translation = $filter('translate');
    var notSelectedCodes = "0x6e6564";
    var vm = this;
    vm.ids = null;
    if (angular.isDefined($stateParams.ids) && $stateParams.ids !== null) {
      localStorageService.set(PRODUCT_UPDATE_IDS, $stateParams.ids, "localStorage");
    }
    vm.ids = localStorageService.get(PRODUCT_UPDATE_IDS, "localStorage");
    vm.item = {};
    var regexPhyName = "{";
    for (var i = 0; i < vm.ids.length; i++) {
      regexPhyName += "([^;}]+)";
      if (i !== vm.ids.length - 1) {
        regexPhyName += ";{1}";
      }
    }
    regexPhyName += "}{1,}";
    vm.regexPhyName = new RegExp(regexPhyName);
    vm.pathImages = IMAGE_PATH;
    vm.originalImage = IMAGE_ORIGINAL;
    vm.mediumImage = IMAGE_MEDIUM;
    vm.selectedIcyId = null;
    vm.item.sellable = true;
    vm.item.syncFeedStatus = true;
    vm.item.active = true;
    vm.item.sale = false;
    vm.selectedAllFiles = false;
    vm.selectedAllImages = false;
    vm.isSelectedImage = false;
    vm.imageSelectedMain = null;
    vm.parameterGroups = { model: 0, options: [] };
    vm.manufacturer = { options: [] };
    vm.metaKeywords = { options: [], selected: [] };
    vm.categories = { items: [], selected: [], pceMain: null };
    vm.checkbox = { metaKeywords: false, categories: false, files: false, image: false, warrantyNull: false, weightNull: false, notInStock: false, inStock: false, sale: false, weight: false, warranty: false, parameters: false, code: false, manufacturer: false, barcode: false, phyName: false, options: false, descriptionShort: false, descriptionLong: false, descriptionMeta: false, priceNoVat: false, priceVat: false, availability: false, active: false, sellable: false, syncFeedStatus: false };
    vm.dtOptions = DTOptionsBuilder.newOptions()
      .withBootstrap()
      .withOption('order', [[1, 'asc']])
      .withDOM('<"row"<"col-md-8 col-sm-12"><"col-md-4 col-sm-12"<"pull-right"f>>>t<"row"<"col-md-4 col-sm-12"><"col-md-4 col-sm-12"<"inline-controls text-center"i>><"col-md-4 col-sm-12"p>>')
      .withLanguage({
        "sLengthMenu": $translation("Global.Table.VIEW") + ' _MENU_ ' + $translation("Global.Table.RECORDS"),
        "sInfo": $translation("Global.Table.FOUND") + ' _TOTAL_ ' + $translation("Global.Table.RECORDS"),
        "sSearch": $translation("Global.Table.SEARCH"),
        "sEmptyTable": $translation("Global.Table.NO_DATA"),
        "sZeroRecords": $translation("Global.Table.NO_MATCHING"),
        "sInfoEmpty": $translation("Global.Table.INFO_EMPTY"),
        "oPaginate": {
          "sPage": $translation("Global.Table.PAGE"),
          "sPageOf": $translation("Global.Table.OF"),
          "sFirst": "<i class='fa fa-angle-double-left'></i>",
          "sPrevious": "<i class='fa fa-angle-left'></i>",
          "sNext": "<i class='fa fa-angle-right'></i>",
          "sLast": "<i class='fa fa-angle-double-right'></i>"
        }
      })
      .withPaginationType('full_numbers')
      .withColumnFilter();

    vm.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(0).notSortable(),
      DTColumnDefBuilder.newColumnDef(2).notSortable(),
      DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];

    vm.isEdited = function () {
      if ((vm.checkbox.phyName && vm.item.phy_name.trim().length === 0) ||
        (vm.checkbox.code && vm.item.code.trim().length === 0) ||
        (vm.checkbox.categories && vm.categories.selected.length === 0)) return true;
      var shipping = false;
      angular.forEach(vm.shipping, function (value) {
        if (value.enabled) {
          shipping = true;
          return false;
        }
      });
      angular.forEach(vm.payment, function (value) {
        if (value.enabled) {
          shipping = true;
          return false;
        }
      });
      angular.forEach(vm.groups, function (value) {
        if (value.enabled || value.delete) {
          shipping = true;
          return false;
        }
      });
      return (shipping | vm.checkbox.phyName | vm.checkbox.code | vm.checkbox.categories | vm.checkbox.metaKeywords | vm.checkbox.files | vm.checkbox.image | vm.checkbox.warrantyNull | vm.checkbox.weightNull | vm.checkbox.notInStock | vm.checkbox.inStock | vm.checkbox.sale | vm.checkbox.weight | vm.checkbox.warranty | vm.checkbox.parameters | vm.checkbox.manufacturer | vm.checkbox.barcode | vm.checkbox.options | vm.checkbox.descriptionShort | vm.checkbox.descriptionLong | vm.checkbox.descriptionMeta | vm.checkbox.priceNoVat | vm.checkbox.priceVat | vm.checkbox.availability | vm.checkbox.active | vm.checkbox.sellable | vm.checkbox.syncFeedStatus) === 0 ? true : false;
    };

    vm.changeNoVat = function () {
      vm.item.price_vat = parseFloat(vm.item.price_no_vat * 1.21);
    };

    vm.changeVat = function () {
      vm.item.price_no_vat = parseFloat(vm.item.price_vat/1.21);
    };

    vm.selectAllFiles = function () {
      angular.forEach(vm.files, function (value) {
        value.selected = vm.selectedAllFiles;
      });
    };

    vm.shippingChange = function (item) {
      if (item.dontShow) {
        item.selected = false;
        item.price = null;
      }
      if (!item.selected) {
        item.price = null;
      }
    };

    vm.selectAllImages = function () {
      angular.forEach(vm.images, function(image) {
        image.selected = vm.selectedAllImages;
      });
    };

    vm.selectImage = function(index) {
      vm.images[index].selected = !vm.images[index].selected;
    };

    vm.selectImageMain = function (index) {
      if (vm.imageSelectedMain !== null && angular.isDefined(vm.images[vm.imageSelectedMain])) {
        vm.images[vm.imageSelectedMain].selectedMain = false;
      }
      vm.imageSelectedMain = index;
      vm.images[index].selectedMain = true;
    };

    vm.selectImageCategory = function (icyId) {
      vm.selectedIcyId = icyId;
      ImagesService.getByIcyId(icyId).then(function (response) {
        vm.images = response.data;
      });
    };

    vm.selectCategory = function (item) {
      if (item.selected && !vm.categories.selected.includes(item.cey_id)) {
        vm.categories.selected.push(item.cey_id);
      }
      if (!item.selected) {
        vm.categories.selected = vm.categories.selected.filter(function (x) {
          return x !== item.cey_id;
        });
      }
    };

    vm.selectCategoryMain = function (ceyId) {
      vm.categories.pceMain = ceyId;
      if (!vm.categories.selected.includes(ceyId)) {
        vm.categories.selected.push(ceyId);
      }
    };

    ParameterGroupsService.get().then(function (response) {
      vm.parameterGroups.options = response.data;
    });

    ManufacturerService.get().then(function (response) {
      vm.manufacturer.options = response.data;
      vm.manufacturer.model = vm.manufacturer.options[0].id;
    });

    ImageCategoriesService.get().then(function (response) {
      vm.imageCategories = response.data;
    });

    CategoriesService.get().then(function (response) {
      vm.categories.items = response.data;
    });

    FilesService.get().then(function (response) {
      vm.files = response.data;
    });

    PaymentsService.get().then(function (response) {
      vm.payment = response.data;
      angular.forEach(vm.payment, function (value) {
        value.enable = true;
      });
    });

    ShippingService.get().then(function (response) {
      vm.shipping = response.data;
    });

    MetaKeywordsService.get().then(function (response) {
      vm.metaKeywords.options = response.data;
    });

    GroupsService.get().then(function (response) {
      vm.groups = response.data;
      angular.forEach(vm.groups, function (value) {
        value.enabled = false;
      });
    });

    var getAvailability = function () {
      if (vm.checkbox.availability || vm.checkbox.notInStock || vm.checkbox.inStock) {
        if (vm.checkbox.availability) return angular.isUndefined(vm.item.availability) ? 1 : vm.item.availability;
        if (vm.checkbox.notInStock) return -1;
        return 0;
      }
      return notSelectedCodes;
    };

    var getWarranty = function () {
      if (vm.checkbox.warranty || vm.checkbox.warrantyNull) {
        if (vm.checkbox.warrantyNull) return null;
        return angular.isUndefined(vm.item.warranty) ? 0 : vm.item.warranty;
      }
      return notSelectedCodes;
    };

    var getWeight = function () {
      if (vm.checkbox.weight || vm.checkbox.weightNull) {
        if (vm.checkbox.weightNull) return null;
        return angular.isUndefined(vm.item.weight) ? 0 : vm.item.weight
      }
      return notSelectedCodes;
    };

    var getShipping = function () {
      var shipping = [];
      angular.forEach(vm.shipping, function (value) {
        if (!value.enabled) return false;
        if (!value.selected && !value.dontShow) {
          shipping.push({
            spg_id: value.spg_id,
            show_shipping: false,
            fixed_price: null,
            delete: true
          });
          return false;
        }
        if (value.selected) {
          shipping.push({
            spg_id: value.spg_id,
            show_shipping: true,
            fixed_price: angular.isUndefined(value.price) || value.price === null ? 0 : parseFloat(value.price),
            delete: false
          });
        } else {
          shipping.push({
            spg_id: value.spg_id,
            show_shipping: false,
            fixed_price: null,
            delete: false
          });
        }
      });
      return shipping.length === 0 ? notSelectedCodes : shipping;
    };

    var getPayment = function () {
      var payments = [];
      angular.forEach(vm.payment, function(value) {
        if (!value.enabled) return false;
        payments.push({
          id: value.id,
          enable: value.enable
        });
      });
      return payments.length === 0 ? notSelectedCodes : payments;
    };

    var getGroups = function () {
      var group_discounts = [];
      angular.forEach(vm.groups, function(value) {
        if (!value.enabled && !value.delete) return false;
        if (value.enabled) {
          group_discounts.push({
            discount: angular.isUndefined(value.discount) || value.discount === null ? 0 : parseFloat(value.discount),
            grp_id: value.grp_id,
            delete: false
          });
        }
        if (value.delete) {
          group_discounts.push({
            discount: null,
            grp_id: value.grp_id,
            delete: true
          });
        }
      });
      return group_discounts.length === 0 ? notSelectedCodes : group_discounts;
    };

    var getParams = function () {
      if (!vm.checkbox.parameters) return notSelectedCodes;
      var myRegex = new RegExp(regexPhyName,"g");
      var array1;
      var codesArray = [];
      var b = 0;
      var pve_value;
      angular.forEach(vm.ids, function () {
        codesArray[b] = [];
        angular.forEach(vm.parameterGroups.model.parameters, function (value) {
          pve_value = angular.isDefined(value.value) && value.value.trim().length > 0 ? value.value.trim() : null;
          while ((array1 = myRegex.exec(pve_value)) !== null) {
            pve_value = pve_value.replace(array1[0], array1[b + 1]);
          }
          codesArray[b].push({
            prr_id: value.prr_id,
            pve_value: pve_value
          });
        });
        b++;
      });

      return codesArray;
    };

    var getImages = function () {
      if (!vm.checkbox.image) return notSelectedCodes;
      var images = [];
      angular.forEach(vm.images, function(value) {
        if (value.selected) {
          images.push({
            iae_id: value.iae_id,
            iae_main: angular.isDefined(value.selectedMain) ? value.selectedMain : false
          });
        }
      });
      return images;
    };

    var getFiles = function () {
      if (!vm.checkbox.files) return notSelectedCodes;
      var files = [];
      angular.forEach(vm.files, function(value) {
        if (value.selected) {
          files.push(value.fie_id);
        }
      });
      return files;
    };

    var getCategories = function () {
      if (!vm.checkbox.categories || vm.categories.selected.length === 0) return notSelectedCodes;
      var categories = [];
      angular.forEach(vm.categories.selected, function(value) {
        categories.push({
          cey_id: value,
          pce_main: value === vm.categories.pceMain
        });
      });
      return categories;
    };

    var getPhyNames = function () {
      if (!vm.checkbox.phyName) return notSelectedCodes;
      var myRegex = new RegExp(regexPhyName,"g");
      var array1;
      var phyNameArray = [];
      angular.forEach(vm.ids, function () {
        phyNameArray.push(vm.item.phy_name.trim());
      });
      while ((array1 = myRegex.exec(vm.item.phy_name.trim())) !== null) {
        for (var j = 0; j < phyNameArray.length; j++) {
          phyNameArray[j] = phyNameArray[j].replace(array1[0], array1[j+1]);
        }
      }
      return phyNameArray;
    };

    var getCodes = function () {
      if (!vm.checkbox.code) return notSelectedCodes;
      var myRegex = new RegExp(regexPhyName,"g");
      var array1;
      var codesArray = [];
      angular.forEach(vm.ids, function () {
        codesArray.push(vm.item.code.trim());
      });
      while ((array1 = myRegex.exec(vm.item.code.trim())) !== null) {
        for (var j = 0; j < codesArray.length; j++) {
          codesArray[j] = codesArray[j].replace(array1[0], array1[j+1]);
        }
      }
      return codesArray;
    };

    var getBarcode = function () {
      if (!vm.checkbox.barcode) return notSelectedCodes;
      if (angular.isUndefined(vm.item.barcode) || vm.item.barcode === null || vm.item.barcode.trim().length < 13) return null;
      var myRegex = new RegExp(regexPhyName,"g");
      var array1;
      var codesArray = [];
      angular.forEach(vm.ids, function () {
        codesArray.push(vm.item.barcode.trim());
      });
      while ((array1 = myRegex.exec(vm.item.barcode.trim())) !== null) {
        for (var j = 0; j < codesArray.length; j++) {
          codesArray[j] = codesArray[j].replace(array1[0], array1[j+1]);
        }
      }
      return codesArray;
    };

    var getDescMeta = function () {
      if (!vm.checkbox.descriptionMeta) return notSelectedCodes;
      if (angular.isUndefined(vm.item.description_meta) || vm.item.description_meta === null || vm.item.description_meta.trim().length === 0) return null;
      var myRegex = new RegExp(regexPhyName,"g");
      var array1;
      var codesArray = [];
      angular.forEach(vm.ids, function () {
        codesArray.push(vm.item.description_meta.trim());
      });
      while ((array1 = myRegex.exec(vm.item.description_meta.trim())) !== null) {
        for (var j = 0; j < codesArray.length; j++) {
          codesArray[j] = codesArray[j].replace(array1[0], array1[j+1]);
        }
      }
      return codesArray;
    };

    var getDesc = function (checkbox, item) {
      if (!checkbox) return notSelectedCodes;
      if (angular.isUndefined(item) || item === null || item.trim().length === 0) return null;
      var myRegex = new RegExp(regexPhyName,"g");
      var array1;
      var codesArray = [];
      angular.forEach(vm.ids, function () {
        codesArray.push(item.trim());
      });
      while ((array1 = myRegex.exec(item.trim())) !== null) {
        for (var j = 0; j < codesArray.length; j++) {
          codesArray[j] = codesArray[j].replace(array1[0], array1[j+1]);
        }
      }
      return codesArray;
    };

    vm.update = function () {
      var data = {
        items: vm.ids,
        pgp_id: vm.checkbox.parameters ? (angular.isDefined(vm.parameterGroups.model.pgp_id) ? vm.parameterGroups.model.pgp_id : null) : notSelectedCodes,
        mur_id: vm.checkbox.manufacturer ? vm.manufacturer.model : notSelectedCodes,
        code: getCodes(),
        availability: getAvailability(),
        active: vm.checkbox.active ? vm.item.active : notSelectedCodes,
        barcode: getBarcode(),
        description_short: getDesc(vm.checkbox.descriptionShort, vm.item.description_short),
        description_long: getDesc(vm.checkbox.descriptionLong, vm.item.description_long),
        warranty: getWarranty(),
        description_meta: getDescMeta(),
        sellable: vm.checkbox.sellable ? vm.item.sellable : notSelectedCodes,
        sync_feed_status: vm.checkbox.sync_feed_status ? vm.item.sync_feed_status : notSelectedCodes,
        phy_name: getPhyNames(),
        price_vat: vm.checkbox.priceVat ? (angular.isUndefined(vm.item.price_vat) ? 0 : vm.item.price_vat) : notSelectedCodes,
        price_no_vat: vm.checkbox.priceVat ? (angular.isUndefined(vm.item.price_no_vat) ? 0 : vm.item.price_no_vat) : notSelectedCodes,
        weight: getWeight(),
        option_title: vm.checkbox.options ? (angular.isDefined(vm.item.option_title) && vm.item.option_title !== null && vm.item.option_title.trim().length > 0 ? vm.item.option_title.trim() : null) : notSelectedCodes,
        shippings: getShipping(),
        categories: getCategories(),
        files: getFiles(),
        images: getImages(),
        group_discounts: getGroups(),
        payments: getPayment(),
        menu_options: vm.checkbox.options ? (angular.isUndefined(vm.item.menu_options) ? [] : vm.item.menu_options) : notSelectedCodes,
        parameter_values: getParams(),
        meta_keywords: vm.checkbox.metaKeywords ? (vm.metaKeywords.selected) : notSelectedCodes,
        sale: vm.checkbox.sale ? (vm.item.sale) : notSelectedCodes
      };

      ProductsService.updateMultiple(data).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

  }


})();
