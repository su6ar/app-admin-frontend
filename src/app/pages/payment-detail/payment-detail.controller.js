(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('PaymentDetailController', PaymentDetailController);

  /** @ngInject */
  function PaymentDetailController(PaymentsService, $stateParams, $filter, toastr) {
    var vm = this;
    var $translation = $filter('translate');

    PaymentsService.getOne($stateParams.id).then(function (response) {
      var data = response.data;
      vm.poyName = data.poy_name;
      vm.cashOnDelivery = data.cash_on_delivery === true ? 1 : 0;
      vm.price = data.price;
      vm.addPercentageOottPrice = data.add_percentage_oott_price;
    });

    vm.update = function() {
      var coy = vm.cashOnDelivery === 1 ? true : null;

      PaymentsService.put($stateParams.id, coy, vm.poyName, vm.addPercentageOottPrice, vm.price).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
