(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('UsersCustomersDetailController', UsersCustomersDetailController);

  /** @ngInject */
  function UsersCustomersDetailController($stateParams, UsersCustomersService) {
    var vm = this;
    vm.item = {};

    UsersCustomersService.getOne($stateParams.act_id).then(function (response) {
      vm.item = response.data;
    });
  }


})();

