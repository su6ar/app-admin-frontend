(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('RefreshPasswordController', RefreshPasswordController);

  /** @ngInject */
  function RefreshPasswordController($stateParams, ALERT_OPTIONS, AlertsService, AccountsService, $state, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    var codeRefresh = $stateParams.codeRefresh;

    AccountsService.codeRefreshExists(codeRefresh).then(function() {
      }, function() {
        $state.go('error.page404');
    });

    vm.refreshPassword = function () {
      vm.alerts = [];
      vm.alert = null;

      AccountsService.refreshPassword(codeRefresh, vm.password).then(function() {
        vm.alerts = AlertsService.showAlert(vm.alerts,
          $translation('Global.Alert.SUCCESS') + '! ' + $translation('Global.Alert.LOGIN_AFTER_REFRESH'),
          ALERT_OPTIONS.colors[0].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
        vm.alert = {
          uisref: "auth.login",
          uisrefMsg: $translation('Global.Label.HERE')
        };
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        vm.alerts = AlertsService.showAlert(vm.alerts,
          message,
          ALERT_OPTIONS.colors[2].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
      });
    }
  }

})();
