(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('UsersCustomersController', UsersCustomersController);

  /** @ngInject */
  function UsersCustomersController($uibModal, GroupsService, $filter, UsersCustomersService) {
    var $translation = $filter('translate');
    var vm = this;

    vm.items = [];
    vm.itemsSelected = [];
    vm.limit = { options: [
      {id: 1, name: 10},
      {id: 2, name: 25},
      {id: 3, name: 50},
      {id: 4, name: 100}
    ], selected: {id: 1, name: 10}};
    vm.offset = 0;
    vm.selectedCeyId = null;
    vm.page = { current: 1, total: 1 };
    vm.search = {
      string: "",
      strings: []
    };
    vm.sort = {
      columns: [
        {name: "act_id", sort: 1},
        {name: "grp_name", sort: 0},
        {name: "act_email", sort: 0},
        {name: "uer_active", sort: 0}
      ],
      options: [
        "sorting", "sorting_asc", "sorting_desc"
      ],
      active: 0
    };
    vm.groups = [];

    GroupsService.get().then(function (response) {
      vm.groups = response.data;
    });

    vm.selectedAll = false;

    vm.selectAll = function () {
      angular.forEach(vm.items, function(type) {
        type.selected = vm.selectedAll;
      });
    };

    vm.searchData = function (index) {
      var value = index !== -1 ? vm.search[vm.sort.columns[index].name].toString() : vm.search.string.toString();
      if (index !== -1) {
        if (value.length === 0) {
          vm.search.strings = vm.search.strings.filter(function (item) {
            return item !== index;
          });
        } else if (vm.search.strings.indexOf(index) === -1) {
          vm.search.strings.push(index);
        }
      }
      vm.items = UsersCustomersService.getList();
      for (var i = 0; i < vm.search.strings.length; i++) {
        vm.items = vm.items.filter(function (el) {
          return el[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase().indexOf(vm.search[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase()) > -1;
        });
      }
      if (index === -1 || vm.search.string.length > 0) {
        vm.items = vm.items.filter(function (el) {
          for (var j = 0; j < vm.sort.columns.length; j++) {
            if (el[vm.sort.columns[j].name].toString().toLowerCase().indexOf(vm.search.string.toString().toLowerCase()) > -1) {
              return true;
            }
          }
          return false;
        });
      }
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.items.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.updateSort(vm.sort.active, false);
      if (vm.search.strings.length > 0 || vm.search.string.length > 0) {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS') + ' (' + $translation('Global.Table.FILTERED_FROM') + ' ' + UsersCustomersService.getList().length + ' ' + $translation('Global.Table.RECORDS') + ')';
      } else {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS');
      }
    };

    vm.updateSort = function (index, change) {
      vm.sort.active = index;
      if (change) {
        if (vm.sort.columns[index].sort === 1) {
          vm.sort.columns[index].sort = 2;
        } else {
          vm.sort.columns[index].sort = 1;
        }
        for (var i = 0; i < vm.sort.columns.length; i++) {
          if (index === i) continue;
          vm.sort.columns[i].sort = 0;
        }
      }
      vm.items.sort(function(a,b) {
        var nameA, nameB;
        if (vm.sort.columns[index].sort===1) {
          if (index===0||index===3) {
            return a[vm.sort.columns[index].name] - b[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameA.localeCompare(nameB);
          }
        } else {
          if (index===0||index===3) {
            return b[vm.sort.columns[index].name] - a[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameB.localeCompare(nameA);
          }
        }
      });
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    vm.updateData = function () {
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.items.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.offset = (vm.limit.selected.name*vm.page.current)-vm.limit.selected.name;
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    vm.itemSelected = function(offset, limit) {
      return vm.items.filter(function(value, index) {
        return index >= offset && index < offset+limit;
      });
    };

    var getSelectedOrders = function () {
      var items = [];
      for (var i = 0; i < vm.items.length; i++)
      {
        if (vm.items[i].selected) {
          items.push({
            id: vm.items[i].act_id
          });
        }
      }
      return items;
    };

    vm.modalChangeGroup = function (grpId) {
      var items = getSelectedOrders();

      if (items.length === 0) {
        return false;
      }

      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_change_rights_tmpl.html',
        controller: 'ModalsChangeUsersController',
        controllerAs: 'modal',
        resolve: {
          item: function () {
            return grpId;
          },
          items: function () {
            return items;
          },
          service: function () {
            return UsersCustomersService;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    UsersCustomersService.get().then(function (response) {
      UsersCustomersService.setList(response.data);
      vm.items = UsersCustomersService.getList();
      vm.page.current = 1;
      vm.updateSort(vm.sort.active, false);
      vm.updateData();
      vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS');
    });
  }


})();

