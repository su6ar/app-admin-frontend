(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('MenuMainAddController', MenuMainAddController);

  /** @ngInject */
  function MenuMainAddController(toastr, $filter, MenuMainService, MenuFooterService, CompanyService, MetaKeywordsService, ArticlesService) {
    var vm = this;
    var $translation = $filter('translate');

    vm.item = {};
    vm.articles = { model: 0, options: [] };
    vm.company = { model: 0, options: [] };
    vm.item.footer = { model: null, options: [] };
    vm.selectedParent = 0;
    vm.item.metaKeywords = {};
    vm.item.metaKeywords.options = [];
    vm.item.metaKeywords.selected = [];

    CompanyService.get().then(function (response) {
      CompanyService.setList(response.data);
      vm.company.options = CompanyService.getList();
      vm.company.model = vm.company.options[0].cct_id;
    });

    MetaKeywordsService.get().then(function (response) {
      MetaKeywordsService.setList(response.data);
      vm.item.metaKeywords.options = MetaKeywordsService.getList();
    });

    ArticlesService.get().then(function (response) {
      ArticlesService.setList(response.data);
      vm.articles.options = ArticlesService.getList();
    });

    MenuMainService.get().then(function (response) {
      MenuMainService.setList(response.data);
      vm.items = [{
        "mim_id": 0,
        "mim_mim_id": null,
        "mim_name": "Hlavní menu",
        "mim_place": null,
        "items": MenuMainService.getList()
      }];
    });

    MenuFooterService.getOnly().then(function (response) {
      MenuFooterService.setList(response.data);
    });

    vm.loadTags = function () {
      return MenuFooterService.getList();
    };

    vm.mimNameChanged = function (tag) {
      if (vm.item.footer.model !== null && vm.item.footer.model.length > 1) {
        vm.item.footer.model.splice(0, 1);
      }

      if (angular.isDefined(tag.mim_id)) {
        MenuFooterService.getOne(tag.mim_id).then(function (response) {
          var data = response.data;
          vm.selectedParent = 0;
          vm.item.meta_description = data.meta_description;
          vm.item.mim_ecommerce = data.mim_ecommerce;
          vm.item.mim_home = data.mim_home;
          vm.item.cct_id = data.cct_id;
          vm.item.metaKeywords.selected = data.meta_keywords;
        });
      }
    };

    vm.add = function () {
      var metaKeywords = [];
      angular.forEach(vm.item.metaKeywords.selected, function (value) {
        metaKeywords.push(value.id);
      });
      var data = {
        mim_id: angular.isDefined(vm.item.footer.model[0].mim_id) ? vm.item.footer.model[0].mim_id : null,
        ate_id: vm.articles.model === 0 ? null : vm.articles.model,
        mim_mim_id: vm.selectedParent === 0 ? null : vm.selectedParent,
        mim_name: vm.item.footer.model[0].mim_name,
        meta_description: angular.isUndefined(vm.item.meta_description) ? null : vm.item.meta_description,
        mim_ecommerce: angular.isUndefined(vm.item.mim_ecommerce) ? null : vm.item.mim_ecommerce,
        mim_home: angular.isUndefined(vm.item.mim_home) ? null : vm.item.mim_home,
        cct_id: vm.company.model,
        meta_keywords: metaKeywords
      };

      MenuMainService.post(data).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

