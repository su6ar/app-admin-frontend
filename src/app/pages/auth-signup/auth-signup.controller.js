(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('SignupController', SignupController);

  /** @ngInject */
  function SignupController($filter, AccountsService, AlertsService, ALERT_OPTIONS) {
    var $translation = $filter('translate');
    var vm = this;

    vm.images = [
      {
        src: 'assets/images/avatars-vector/boy-brown-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/girl-purple-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/girl-red-shirt.svg',
        selected: false
      }
    ];

    vm.isSelected = true;

    vm.selectImage = function(index) {
      vm.images[index].selected = true;

      angular.forEach(vm.images, function(image, key) {
        if(key !== index)
          image.selected = false;
      });
    };

    vm.signUp = function() {
      var firstName = vm.firstName.trim();
      var lastName = vm.lastName.trim();
      var avatar = "";
      angular.forEach(vm.images, function(image) {
        if(image.selected)
          avatar = image.src;
      });

      vm.alerts = [];

      AccountsService.createAccount(vm.email, vm.password, avatar, firstName, lastName).then(function() {
        vm.alerts = AlertsService.showAlert(vm.alerts,
          $translation('Global.Alert.SUCCESS') + '! ' + $translation('Global.Alert.AFTER_SIGN_UP') + '.',
          ALERT_OPTIONS.colors[0].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        vm.alerts = AlertsService.showAlert(vm.alerts,
          message,
          ALERT_OPTIONS.colors[2].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
      });
    }
  }

})();
