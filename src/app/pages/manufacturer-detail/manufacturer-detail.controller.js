(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ManufacturerDetailController', ManufacturerDetailController);

  /** @ngInject */
  function ManufacturerDetailController($stateParams, ManufacturerService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');

    ManufacturerService.getOne($stateParams.id).then(function (response) {
      vm.murName = response.data.mur_name;
    });

    vm.update = function () {
      ManufacturerService.put($stateParams.id, vm.murName).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

