(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('CategoriesController', CategoriesController);

  /** @ngInject */
  function CategoriesController(IMAGE_PATH, IMAGE_SMALLEST, $filter, toastr, CategoriesService, $scope, $uibModal) {
    var vm = this;
    var $translation = $filter('translate');

    vm.items = [];
    vm.selectedAll = false;
    vm.pathImages = IMAGE_PATH;
    vm.smallestImage = IMAGE_SMALLEST;

    vm.selectAll = function () {
      vm.selectedAll = !vm.selectedAll;
      vm.selectAllLoop(vm.items);
    };

    vm.selectAllLoop = function (items) {
      angular.forEach(items, function(item) {
        item.selected = vm.selectedAll;
        vm.selectAllLoop(item.items);
      });
    };

    var getSelected = function (items, selectedArr) {
      angular.forEach(items, function(item) {
        if (item.selected === true) {
          selectedArr.push({ id: item.cey_id });
        }
        getSelected(item.items, selectedArr);
      });
    };

    CategoriesService.get().then(function (response) {
      CategoriesService.setList(response.data);
      vm.items = CategoriesService.getList();
    });

    vm.collapseAll = function() {
      $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    vm.expandAll = function() {
      $scope.$broadcast('angular-ui-tree:expand-all');
    };

    vm.modalDeleteSingle = function (ceyId) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_single_tmpl.html',
        controller: 'ModalsDeleteSingleController',
        controllerAs: 'modal',
        resolve: {
          id: function () {
            return ceyId;
          },
          ceyId: function () {
            return null;
          },
          index: function () {
            return null;
          },
          service: function () {
            return CategoriesService;
          },
          attri: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    vm.update = function () {
      CategoriesService.put(vm.items).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

    vm.modalDeleteMultiple = function () {
      var selectedArr = [];
      getSelected(vm.items, selectedArr);

      if (selectedArr.length === 0) {
        return false;
      }

      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_multiple_tmpl.html',
        controller: 'ModalsDeleteMultipleController',
        controllerAs: 'modal',
        resolve: {
          items: function () {
            return selectedArr;
          },
          ceyId: function () {
            return null;
          },
          service: function () {
            return CategoriesService;
          },
          attri: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };
  }


})();

