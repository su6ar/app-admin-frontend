(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('GroupsDetailController', GroupsDetailController);

  /** @ngInject */
  function GroupsDetailController($state, $stateParams, GroupsService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = {};
    vm.item.grp_default = false;

    GroupsService.getOne($stateParams.grp_id).then(function (response) {
      GroupsService.setList(response.data);
      vm.item = GroupsService.getList();
    });

    vm.change = function (discount, items) {
      for (var i = 0; i < items.length; i++) {
        items[i].discount = discount;
        vm.change(discount, items[i].items);
      }
    };

    var generateCategoryDiscounts = function (newArr, items) {
      for (var i = 0; i < items.length; i++) {
        newArr.push({
          cey_id: items[i].cey_id,
          discount: angular.isUndefined(items[i].discount) || items[i].discount === null ? 0 : items[i].discount
        });
        generateCategoryDiscounts(newArr, items[i].items);
      }
    };

    vm.update = function () {
      var items = [];
      generateCategoryDiscounts(items, vm.item.categories);
      var data = {
        grp_name: vm.item.grp_name,
        grp_default: vm.item.grp_default,
        categories: items
      };

      GroupsService.put(data, vm.item.grp_id).then(function (response) {
        if (response.data.grp_id) {
          $state.go("app.groups.detail", {grp_id: response.data.grp_id});
        }
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
