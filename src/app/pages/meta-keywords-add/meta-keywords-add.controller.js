(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('MetaKeywordsAddController', MetaKeywordsAddController);

  /** @ngInject */
  function MetaKeywordsAddController(MetaKeywordsService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = {};

    vm.add = function () {
      MetaKeywordsService.post(vm.item).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
