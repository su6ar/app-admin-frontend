(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ForgotPasswordController', ForgotPasswordController);

  /** @ngInject */
  function ForgotPasswordController(AccountsService, ALERT_OPTIONS, AlertsService, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.alerts = [];
    vm.alerts = AlertsService.showAlert(vm.alerts,
      $translation('Global.Alert.REFRESH_PASS'),
      ALERT_OPTIONS.colors[3].name,
      ALERT_OPTIONS.icons[0].value,
      ALERT_OPTIONS.durations[0].value,
      false,
      true,
      true
    );

    vm.refreshPassword = function () {
      vm.alert = null;
      var message;
      var type;

      AccountsService.generateCodeRefresh(vm.email).then(function() {
        message = $translation('Global.Alert.SUCCESS');
        type = ALERT_OPTIONS.colors[0].name;
      }, function(error) {
        type = ALERT_OPTIONS.colors[2].name;
        try {
          message = error.data.message;
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN');
        }
      }).finally (function () {
        vm.alerts = AlertsService.showAlert(vm.alerts,
          message,
          type,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
      });
    }
  }

})();
