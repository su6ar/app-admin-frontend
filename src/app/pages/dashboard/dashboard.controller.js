(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('DashboardController', DashboardController);

  /** @ngInject */
  function DashboardController(moment, $log, localStorageService, $filter, dashboardStats, StatisticsService, $locale) {
    var vm = this;
    var $translation = $filter('translate');
    var startDate = "startDate";
    var endDate = "endDate";

    vm.cards = [{
      bgColor: "bg-greensea",
      faIcon: "fa-users",
      value: dashboardStats.users,
      currency: null,
      label: $translation('Global.Label.NEW_USERS'),
      back: [{
        label: $translation('Global.Label.MANAGE_USERS'),
        faIcon: "fa-ellipsis-h",
        fa: "fas",
        sref: "app.users.customers.list"
      }]
    }, {
      bgColor: "bg-lightred",
      faIcon: "fa-shopping-cart",
      value: dashboardStats.orders.count,
      currency: null,
      label: $translation('Global.Label.NEW_ORDERS'),
      back: [{
        label: $translation('Global.Label.MANAGE_ORDERS'),
        faIcon: "fa-ellipsis-h",
        fa: "fas",
        sref: "app.orders.list"
      }]
    }, {
      bgColor: "bg-warning",
      faIcon: "fa-dollar-sign",
      value: dashboardStats.orders.sum,
      currency: "Kč",
      label: $translation('Global.Label.TOTAL'),
      back: [{
        label: $translation('Global.Label.MANAGE_ORDERS'),
        faIcon: "fa-ellipsis-h",
        fa: "fas",
        sref: "app.orders.list"
      }]
    }];

    var getTimeFromMoment = function (option) {
      if (option.isSame(moment().startOf('month'))) {
        return 0;
      }
      if (option.isSame(moment().startOf('day'))) {
        return 1;
      }
      if (option.isSame(moment().endOf('day'))) {
        return 2;
      }
      if (option.isSame(moment().subtract(1, 'day').startOf('day'))) {
        return 3;
      }
      if (option.isSame(moment().subtract(1, 'day').endOf('day'))) {
        return 4;
      }
      if (option.isSame(moment().subtract(6, 'days').startOf('day'))) {
        return 5;
      }
      if (option.isSame(moment().subtract(29, 'days').startOf('day'))) {
        return 6;
      }
      if (option.isSame(moment().subtract(1, 'month').startOf('month'))) {
        return 7;
      }
      return 8;
    };

    var localizedRanges = {};
    localizedRanges[$translation('Date.THIS_MONTH')] = [moment().startOf('month'), moment().endOf('day')],
    localizedRanges[$translation('Date.TODAY')] = [moment().startOf('day'), moment().endOf('day')],
    localizedRanges[$translation('Date.YESTERDAY')] = [moment().subtract(1, 'day').startOf('day'), moment().subtract(1, 'day').endOf('day')],
    localizedRanges[$translation('Date.LAST_7_DAYS')] = [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
    localizedRanges[$translation('Date.LAST_30_DAYS')] = [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
    localizedRanges[$translation('Date.LAST_MONTH')] = [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')];

    vm.datePicker = {
      date: {
        startDate: angular.isUndefined(localStorageService.get(startDate)) || localStorageService.get(startDate) === null ? moment().startOf("day") : (localStorageService.get(startDate) < 10 ? StatisticsService.getTimeFromOption(localStorageService.get(startDate)) : moment.unix(localStorageService.get(startDate))),
        endDate: angular.isUndefined(localStorageService.get(endDate)) || localStorageService.get(endDate) === null ? moment().endOf("day") : (localStorageService.get(endDate) < 10 ? StatisticsService.getTimeFromOption(localStorageService.get(endDate)) : moment.unix(localStorageService.get(endDate)))
      },
      opts: {
        locale: {
          applyLabel: $translation('Global.Button.SUBMIT'),
          format: "DD.MM.YYYY",
          cancelLabel: $translation('Global.Button.CANCEL'),
          customRangeLabel: $translation('Global.Button.CUSTOM_RANGE'),
          daysOfWeek: $locale.DATETIME_FORMATS.SHORTDAY,
          monthNames: $locale.DATETIME_FORMATS.MONTH
        },
        ranges: localizedRanges,
        opens: 'left',
        eventHandlers: {
          'apply.daterangepicker': function (event) {
            $log.log(event);
            var startDateValue = event.model.startDate.unix();
            var endDateValue = event.model.endDate.unix();
            angular.forEach(localizedRanges, function (value, key) {
              if (localizedRanges[key][0].isSame(event.model.startDate) && localizedRanges[key][1].isSame(event.model.endDate)) {
                startDateValue = getTimeFromMoment(event.model.startDate);
                endDateValue = getTimeFromMoment(event.model.endDate);
                return false;
              }
            });
            localStorageService.set(startDate, startDateValue, "localStorage");
            localStorageService.set(endDate, endDateValue, "localStorage");
            StatisticsService.dashboardStats(event.model.startDate.unix(), event.model.endDate.unix()).then(function (response) {
              dashboardStats = response.data;
              vm.cards[0]['value'] = dashboardStats.users;
              vm.cards[1]['value'] = dashboardStats.orders.count;
              vm.cards[2]['value'] = dashboardStats.orders.sum;
            });
          }
        }
      }
    };
  }

})();
