(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ProfileController', ProfileController);

  /** @ngInject */
  function ProfileController(AccountsService, localStorageService, toastr, $filter, $rootScope) {
    var $translation = $filter('translate');
    var vm = this;
    vm.item = $rootScope.currentUser;

    vm.images = [
      {
        src: 'assets/images/avatars-vector/boy-blue-shirt.svg',
        selected: true
      },{
        src: 'assets/images/avatars-vector/boy-brown-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/boy-grey-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/boy-red-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/boy-yellow-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/girl-blue-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/girl-grey-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/girl-purple-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/girl-red-shirt.svg',
        selected: false
      },{
        src: 'assets/images/avatars-vector/girl-wine-shirt.svg',
        selected: false
      }
    ];

    vm.colors = {
      solid: [{
        class: "default",
        value: "header-solid-default"
      }, {
        class: "greensea",
        value: "header-solid-greensea"
      }, {
        class: "lightred",
        value: "header-solid-lightred"
      }, {
        class: "amethyst",
        value: "header-solid-amethyst"
      }, {
        class: "drank",
        value: "header-solid-drank"
      }, {
        class: "gray",
        value: "header-solid-gray"
      }],
      gradient: [{
        class: "default",
        value: null
      }, {
        class: "greensea",
        value: "header-gradient-greensea"
      }, {
        class: "lightred",
        value: "header-gradient-lightred"
      }, {
        class: "amethyst",
        value: "header-gradient-amethyst"
      }, {
        class: "drank",
        value: "header-gradient-drank"
      }, {
        class: "gray",
        value: "header-gradient-gray"
      }],
      primary: [{
        class: "default",
        value: null
      }, {
        class: "greensea",
        value: "primary-color-greensea"
      }, {
        class: "lightred",
        value: "primary-color-lightred"
      }, {
        class: "amethyst",
        value: "primary-color-amethyst"
      }, {
        class: "drank",
        value: "primary-color-drank"
      }, {
        class: "gray",
        value: "primary-color-gray"
      }]
    };

    vm.selectHeader = function(value) {
      vm.item.amr_header_style = value;
      $rootScope.currentUser.amr_header_style = value;
    };
    vm.selectPrimary = function(value) {
      vm.item.amr_primary_color = value;
      $rootScope.currentUser.amr_primary_color = value;
    };

    angular.forEach(vm.images, function (value, key) {
      vm.images[key].selected = value.src === vm.item.amr_avatar;
    });

    vm.selectImage = function(index) {
      vm.images[index].selected = true;

      angular.forEach(vm.images, function(image, key) {
        if(key !== index)
          image.selected = false;
      });
    };

    vm.update = function () {
      var avatar = "";
      angular.forEach(vm.images, function (value) {
        if (value.selected) {
          avatar = value.src;
          return false;
        }
      });

      var data = {
        act_email: vm.item.act_email,
        amr_avatar: avatar,
        amr_deleted_order_show: true,
        amr_deleted_admin_user: true,
        amr_header_style: vm.item.amr_header_style,
        amr_primary_color: vm.item.amr_primary_color,
        amr_first_name: vm.item.amr_first_name,
        amr_last_name: vm.item.amr_last_name,
        new_password: angular.isDefined(vm.item.new_password) && vm.item.new_password.trim().length > 7 ? vm.item.new_password.trim() : null,
        rur_password: vm.item.rur_password.trim()
      };

      AccountsService.put(data).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS'));
        $rootScope.currentUser = undefined;
        localStorageService.clearAll('.*', 'sessionStorage');
        localStorageService.clearAll('.*', 'localStorage');
        AccountsService.getToken(data.act_email, data.new_password === null ? data.rur_password : data.new_password).then(function(response) {
          localStorageService.set("token", response.data.token, "localStorage");

          AccountsService.checkAdmin(response.data.token).then(function (admin) {
            localStorageService.set("currentUser", admin.data, /*localStorageService.getStorageType()*/"localStorage");
            $rootScope.currentUser = admin.data;
          });
        });
      }, function (response) {
        var message;
        try {
          message = response.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

