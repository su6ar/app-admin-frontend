(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ShippingTypeAddController', ShippingTypeAddController);

  /** @ngInject */
  function ShippingTypeAddController(ShippingTypeService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');

    vm.add = function () {
      ShippingTypeService.post(vm.dtaName).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
