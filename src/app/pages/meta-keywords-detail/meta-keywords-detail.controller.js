(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('MetaKeywordsDetailController', MetaKeywordsDetailController);

  /** @ngInject */
  function MetaKeywordsDetailController($stateParams, MetaKeywordsService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = {};

    MetaKeywordsService.getOne($stateParams.id).then(function (response) {
      MetaKeywordsService.setList(response.data);
      vm.item = MetaKeywordsService.getList();
    });

    vm.update = function () {
      MetaKeywordsService.put({mkd_name: vm.item.mkd_name}, $stateParams.id).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

