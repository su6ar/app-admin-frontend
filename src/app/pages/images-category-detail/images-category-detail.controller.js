(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ImagesCategoryDetailController', ImagesCategoryDetailController);

  /** @ngInject */
  function ImagesCategoryDetailController(ImagesService, ImageResizeService, MarkingService, $stateParams, ImageCategoriesService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    var i = 0;
    var j = 0;
    var h = 0;

    vm.images = [];
    vm.markings = [];
    vm.markingGroups = [
      {
        mig_markings: [ "1", "2", "3", "4", "5", "6" ],
        mig_name: "Produkty",
        mig_id: 0
      },
      {
        mig_markings: [ "1", "m", "6" ],
        mig_name: "Články, novinky, stránky",
        mig_id: 1
      },
      {
        mig_markings: [ "1", "r", "6" ],
        mig_name: "Reference",
        mig_id: 2
      }
    ];

    MarkingService.get().then(function (response) {
      vm.markings = response.data;
    });

    ImageCategoriesService.getOne($stateParams.id).then(function (response) {
      var data = response.data;
      vm.icyName = data.icy_name;
      vm.icyMarkings = data.mig_markings;
      vm.icyId = data.icy_id;

      for (i = 0; i < vm.markingGroups.length; i++) {
        for (j = 0; j < vm.markingGroups[i].mig_markings.length; j++) {
          if (!vm.icyMarkings.includes(vm.markingGroups[i].mig_markings[j])) {
            break;
          }
          if (j === vm.markingGroups[i].mig_markings.length - 1) {
            switch (vm.markingGroups[i].mig_id) {
              case 0:
                vm.markingGroups.p = true;
                break;
              case 1:
                vm.markingGroups.m = true;
                break;
              case 2:
                vm.markingGroups.r = true;
                break;
            }
          }
        }
      }
    });

    ImagesService.getByIcyId($stateParams.id).then(function (response) {
      vm.images = response.data;
    });

    vm.update = function () {
      var migMarkings = [];
      var migMarkingsIds = [];
      if (vm.markingGroups.p === true) {
        for (i = 0; i < vm.markingGroups[0].mig_markings.length; i++) {
          for (j = 0; j < vm.markings.length; j++) {
            if (vm.markingGroups[0].mig_markings[i] === vm.markings[j].mig_marking) {
              migMarkings.push({
                mig_marking: vm.markings[j].mig_marking,
                mig_max_height: vm.markings[j].mig_max_height,
                mig_max_width: vm.markings[j].mig_max_width
              });
              migMarkingsIds.push(vm.markings[j].mig_marking);
            }
          }
        }
      }
      if (vm.markingGroups.m === true) {
        var t1 = false;
        for (i = 0; i < vm.markingGroups[1].mig_markings.length; i++) {
          for (j = 0; j < vm.markings.length; j++) {
            if (vm.markingGroups[1].mig_markings[i] === vm.markings[j].mig_marking) {
              t1 = false;
              for (h = 0; h < migMarkings.length; h++) {
                if (migMarkings[h].mig_marking === vm.markingGroups[1].mig_markings[i]) {
                  t1 = true;
                  break;
                }
              }

              if (t1 === true) break;
              migMarkings.push({
                mig_marking: vm.markings[j].mig_marking,
                mig_max_height: vm.markings[j].mig_max_height,
                mig_max_width: vm.markings[j].mig_max_width
              });
              migMarkingsIds.push(vm.markings[j].mig_marking);
            }
          }
        }
      }
      if (vm.markingGroups.r === true) {
        var t2 = false;
        for (i = 0; i < vm.markingGroups[2].mig_markings.length; i++) {
          for (j = 0; j < vm.markings.length; j++) {
            if (vm.markingGroups[2].mig_markings[i] === vm.markings[j].mig_marking) {
              t2 = false;
              for (h = 0; h < migMarkings.length; h++) {
                if (migMarkings[h].mig_marking === vm.markingGroups[2].mig_markings[i]) {
                  t2 = true;
                  break;
                }
              }

              if (t2 === true) break;
              migMarkings.push({
                mig_marking: vm.markings[j].mig_marking,
                mig_max_height: vm.markings[j].mig_max_height,
                mig_max_width: vm.markings[j].mig_max_width
              });
              migMarkingsIds.push(vm.markings[j].mig_marking);
            }
          }
        }
      }

      ImageResizeService.post(migMarkings, vm.images).then(function () {
        ImageCategoriesService.put(vm.icyId, vm.icyName, migMarkingsIds).then(function () {
          toastr.success($translation('Global.Alert.SUCCESS'));
        }, function (response) {
          var message;
          try {
            message = response.data.message
          } catch(e) {
            message = $translation('Global.Error.UNKNOWN')
          }
          toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
        });
      }, function (response) {
        var message;
        try {
          message = response.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

