(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('CategoriesDetailController', CategoriesDetailController);

  /** @ngInject */
  function CategoriesDetailController(toastr, IMAGE_PATH, ImagesService, ImageCategoriesService, IMAGE_ORIGINAL, IMAGE_MEDIUM, $state, $filter, $stateParams, CategoriesService) {
    var vm = this;
    var $translation = $filter('translate');

    vm.pathImages = IMAGE_PATH;
    vm.originalImage = IMAGE_ORIGINAL;
    vm.mediumImage = IMAGE_MEDIUM;
    vm.item = {};
    vm.parameterGroups = { model: 0, options: [] };
    vm.selectedParent = null;
    vm.imageCategories = [];
    vm.selectedIcyId = null;
    vm.imageSelectedMain = null;
    vm.images = [];
    vm.imagesSelected = null;

    vm.selectImageCategory = function (icyId) {
      vm.selectedIcyId = icyId;
      ImagesService.getByIcyId(icyId).then(function (response) {
        vm.images = response.data;
      });
    };

    vm.isInArray = function (index) {
      for (var i = 0; i < vm.images.length; i++) {
        if (vm.images[i].iae_id === index) {
          return i;
        }
      }
      return false;
    };

    vm.selectImageMain = function (index, selected) {
      if (selected) {
        vm.imagesSelected.selectedMain = true;
      } else if (vm.imagesSelected !== null) {
        vm.imagesSelected.selectedMain = false;
      }
      var ind = vm.isInArray(index);
      if (vm.imageSelectedMain !== null && angular.isDefined(vm.images[vm.imageSelectedMain])) {
        vm.images[vm.imageSelectedMain].selectedMain = false;
      }
      vm.imageSelectedMain = ind;
      vm.images[ind].selectedMain = true;
    };

    ImageCategoriesService.get().then(function (response) {
      vm.imageCategories = response.data;
    });

    CategoriesService.getOne($stateParams.cey_id).then(function (response) {
      CategoriesService.setList(response.data);
      vm.item = CategoriesService.getList();
      vm.item.show_parameters = vm.item.show_parameters === true ? 1 : 0;
      vm.parameterGroups.model = vm.item.pgp_id === null ? 0 : vm.item.pgp_id;
      vm.parameterGroups.options = vm.item.parameter_groups;
      vm.selectedParent = vm.item.cey_cey_id === null ? 0 : vm.item.cey_cey_id;
      if (vm.item.icy_id !== null) {
        ImagesService.getByIcyId(vm.item.icy_id).then(function (response) {
          for (var j = 0; j < response.data.length; j++) {
            if (response.data[j].iae_id === vm.item.iae_id) {
              vm.imagesSelected = response.data[j];
              vm.imagesSelected.selectedMain = true;
            }
          }
        });
      }
    });

    CategoriesService.get().then(function (response) {
      CategoriesService.setList(response.data);
      vm.items = [{
        "cey_id": 0,
        "cey_cey_id": null,
        "cey_name": "Hlavní kategorie",
        "cey_place": null,
        "items": CategoriesService.getList()
      }];
    });

    vm.update = function () {
      var iaeId = null;
      if (vm.imagesSelected !== null && vm.imagesSelected.selectedMain) {
        iaeId = vm.imagesSelected.iae_id;
      } else if (vm.imageSelectedMain !== null && angular.isDefined(vm.images[vm.imageSelectedMain])) {
        iaeId = vm.images[vm.imageSelectedMain].iae_id;
      }

      var data = {
        pgp_id: vm.parameterGroups.model === 0 ? null : vm.parameterGroups.model,
        cey_cey_id_original: vm.item.cey_cey_id,
        cey_cey_id: vm.selectedParent === 0 ? null : vm.selectedParent,
        cey_name: vm.item.cey_name,
        show_parameters: vm.item.show_parameters,
        cey_description: angular.isUndefined(vm.item.cey_description) ? null : vm.item.cey_description,
        discounts: vm.item.discounts,
        iae_id: iaeId
      };

      CategoriesService.putOne($stateParams.cey_id, data).then(function (response) {
        if (response.data.cey_id) {
          $state.go("app.categories.detail", {cey_id: response.data.cey_id});
        }
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

