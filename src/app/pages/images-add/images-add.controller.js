(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ImagesAddController', ImagesAddController);

  /** @ngInject */
  function ImagesAddController(FileUploader, PHP_PATH, PHP_IMAGE_UPLOAD_FILENAME, toastr, $filter, ImageCategoriesService, ImagesService, TokenService) {
    var vm = this;
    var uploader = vm.uploader = new FileUploader({
      url: PHP_PATH + PHP_IMAGE_UPLOAD_FILENAME
    });
    var $translation = $filter('translate');

    ImageCategoriesService.get().then(function (response) {
      ImageCategoriesService.setList(response.data);
      vm.categories = ImageCategoriesService.getList();
      vm.selected = { icy_name: vm.categories[0].icy_name, icy_id: vm.categories[0].icy_id, mig: vm.categories[0].mig };
    });

    // FILTERS

    uploader.filters.push({
      name: 'customFilter',
      fn: function() {
        var vm = this;
        return vm.queue.length < 10;
      }
    });

    uploader.filters.push({
      name: 'imageFilter',
      fn: function(item /*{File|FileLikeObject}, options*/) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // CALLBACKS

    /*uploader.onWhenAddingFileFailed = function(item, filter, options) {
     // $log.info('onWhenAddingFileFailed', item, filter, options);
    };*/
    uploader.onAfterAddingFile = function(fileItem) {
      fileItem.selected = vm.selected;
    };
   /* uploader.onAfterAddingAll = function(addedFileItems) {
     // $log.info('onAfterAddingAll', addedFileItems);
    };*/
    uploader.onBeforeUploadItem = function(item) {
      item.formData = [{
        icy_id: item.selected.icy_id,
        icy_name: item.selected.icy_name,
        token: TokenService.isToken(),
        mig_marking: item.selected.mig.map(function(item) {
          return item['mig_marking'];
        }),
        mig_max_height: item.selected.mig.map(function(item) {
          return item['mig_max_height'] === null ? "-1" : item['mig_max_height'];
        }),
        mig_max_width: item.selected.mig.map(function(item) {
          return item['mig_max_width'] === null ? "-1" : item['mig_max_width'];
        })
      }];
      //$log.info('onBeforeUploadItem', item);
    };
    /*uploader.onProgressItem = function(fileItem, progress) {
     // $log.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
     // $log.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      //$log.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
     // $log.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
     // $log.info('onCancelItem', fileItem, response, status, headers);
    };*/
    uploader.onCompleteItem = function(fileItem, response) {
      //$log.info('onCompleteItem', fileItem, response, status, headers);
      if (response.status_code === 200) {
        ImagesService.post(response.icy_id, response.iae_path, response.iae_name, response.iae_type, response.iae_size, response.mig_marking).then(function () {
          toastr.success($translation('Global.Alert.SUCCESS') + '! ');
        }, function (error) {
          var message;
          try {
            message = error.data.message.toString();
          } catch(e) {
            message = $translation('Global.Error.UNKNOWN')
          }
          vm.fileItemError(fileItem);
          toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
        });
      } else {
        vm.fileItemError(fileItem);
        toastr.error(response.response, $translation('Global.Alert.ERROR') + '! ');
      }
    };
    /*uploader.onCompleteAll = function() {
      //$log.info('onCompleteAll');
    };*/

    vm.fileItemError = function (fileItem) {
      fileItem.isError = true;
      fileItem.isSuccess = false;
      fileItem.isUploaded = false;
      fileItem.progress = 0;
      fileItem.uploader.progress = 0;
    };
  }

})();
