(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('MenuMainController', MenuMainController);

  /** @ngInject */
  function MenuMainController($filter, toastr, MenuMainService, $scope, $uibModal) {
    var vm = this;
    var $translation = $filter('translate');

    vm.items = [];
    vm.selectedAll = false;

    vm.selectAll = function () {
      vm.selectedAll = !vm.selectedAll;
      vm.selectAllLoop(vm.items);
    };

    vm.selectAllLoop = function (items) {
      angular.forEach(items, function(item) {
        item.selected = vm.selectedAll;
        vm.selectAllLoop(item.items);
      });
    };

    var getSelected = function (items, selectedArr) {
      angular.forEach(items, function(item) {
        if (item.selected === true) {
          selectedArr.push({ id: item.mim_id });
        }
        getSelected(item.items, selectedArr);
      });
    };

    MenuMainService.get().then(function (response) {
      MenuMainService.setList(response.data);
      vm.items = MenuMainService.getList();
    });

    vm.collapseAll = function() {
      $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    vm.expandAll = function() {
      $scope.$broadcast('angular-ui-tree:expand-all');
    };

    vm.modalDeleteSingle = function (mimId) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_single_tmpl.html',
        controller: 'ModalsDeleteSingleController',
        controllerAs: 'modal',
        resolve: {
          id: function () {
            return mimId;
          },
          index: function () {
            return null;
          },
          service: function () {
            return MenuMainService;
          },
          ceyId: function () {
            return null;
          },
          attri: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    vm.update = function () {
      MenuMainService.put(vm.items).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

    vm.modalDeleteMultiple = function () {
      var selectedArr = [];
      getSelected(vm.items, selectedArr);

      if (selectedArr.length === 0) {
        return false;
      }

      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_multiple_tmpl.html',
        controller: 'ModalsDeleteMultipleController',
        controllerAs: 'modal',
        resolve: {
          items: function () {
            return selectedArr;
          },
          service: function () {
            return MenuMainService;
          },
          ceyId: function () {
            return null;
          },
          attri: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };
  }


})();

