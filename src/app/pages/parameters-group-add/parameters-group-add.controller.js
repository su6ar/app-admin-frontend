(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ParametersGroupAddController', ParametersGroupAddController);

  /** @ngInject */
  function ParametersGroupAddController(ParameterGroupsService, ParametersService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = {};
    vm.item.parameters = [];
    vm.item.selected = [];

    ParametersService.get().then(function (response) {
      ParametersService.setList(response.data);
      vm.item.parameters = ParametersService.getList();
    });

    vm.add = function () {
      var parameters = [];
      angular.forEach(vm.item.selected, function (value) {
        parameters.push(value.prr_id);
      });

      var data = {
        pgp_name: vm.item.pgp_name,
        parameters: parameters
      };

      ParameterGroupsService.post(data).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
