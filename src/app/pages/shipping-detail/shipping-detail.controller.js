(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ShippingDetailController', ShippingDetailController);

  /** @ngInject */
  function ShippingDetailController($stateParams, ShippingTypeService, ShippingService, toastr, PaymentsService, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.selected = { type: null, payments: [] };

    ShippingService.getOne($stateParams.id).then(function (response) {
      var data = response.data;
      vm.name = data.shy_name;
      vm.showOnDefault = data.show_on_default | 0;
      vm.freePriceAbove = parseFloat(data.free_price_above);
      vm.sumWeightPrice = data.sum_weight_price | 0;
      vm.showTillMaxWeight = parseFloat(data.show_till_max_weight);
      vm.selectedPayments = data.payments;
      vm.selectedType = data.dta_name;
      vm.shyId = data.shy_id;
      vm.spgId = data.spg_id;

      ShippingTypeService.get().then(function (response) {
        ShippingTypeService.setList(response.data);
        vm.types = ShippingTypeService.getList();
        angular.forEach(vm.types, function (value, key) {
          if (value.dta_name === vm.selectedType) {
            vm.selected.type = vm.types[key];
            return false;
          }
        });
      });

      PaymentsService.get().then(function (response) {
        PaymentsService.setList(response.data);
        vm.payments = PaymentsService.getList();
        angular.forEach(vm.selectedPayments, function (value) {
          vm.selected.payments.push(value.pmt_id);
        });

        angular.forEach(vm.payments, function(value, key) {
          if (vm.selected.payments.includes(value.id)) {
            vm.payments[key]["selected"] = true;
          }
        });
      });
    });

    vm.paymentChange = function paymentChange (id) {
      if (vm.selected.payments.includes(id)) {
        vm.selected.payments = vm.selected.payments.filter(function (item) {
          return item !== id;
        });
      } else {
        vm.selected.payments.push(id);
      }
    };

    vm.update = function () {
      var payments = [];
      angular.forEach(vm.selected.payments, function (value) {
        payments.push({
          "pmt_id": value
        });
      });

      var stt = vm.showTillMaxWeight === angular.isUndefined ? null : vm.showTillMaxWeight;
      var fpe = vm.freePriceAbove === angular.isUndefined ? null : vm.freePriceAbove;

      ShippingService.putOne(vm.spgId, vm.selected.type.id, payments, vm.showOnDefault, stt, vm.name, fpe, vm.sumWeightPrice).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
