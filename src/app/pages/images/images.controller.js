(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ImagesController', ImagesController);

  /** @ngInject */
  function ImagesController($uibModal, IMAGE_PATH, IMAGE_SMALLEST, ImageCategoriesService, $filter, ImagesService) {
    var $translation = $filter('translate');
    var vm = this;

    vm.items = [];
    vm.itemsSelected = [];
    vm.limit = { options: [
      {id: 1, name: 10},
      {id: 2, name: 25},
      {id: 3, name: 50},
      {id: 4, name: 100}
    ], selected: {id: 1, name: 10}};
    vm.offset = 0;
    vm.selectedCeyId = null;
    vm.page = { current: 1, total: 1 };
    vm.search = {
      string: "",
      strings: []
    };
    vm.sort = {
      columns: [
        {name: "iae_id", sort: 1},
        {name: "iae_name", sort: 0},
        {name: "iae_type", sort: 0},
        {name: "iae_size", sort: 0},
        {name: "icy_name", sort: 0}
      ],
      options: [
        "sorting", "sorting_asc", "sorting_desc"
      ],
      active: 0
    };
    vm.categories = [];
    vm.pathImages = IMAGE_PATH;
    vm.smallestImage = IMAGE_SMALLEST;

    vm.selectedAll = false;

    vm.selectAll = function () {
      angular.forEach(vm.items, function(type) {
        type.selected = vm.selectedAll;
      });
    };

    vm.searchData = function (index) {
      var value = index !== -1 ? vm.search[vm.sort.columns[index].name].toString() : vm.search.string.toString();
      if (index !== -1) {
        if (value.length === 0) {
          vm.search.strings = vm.search.strings.filter(function (item) {
            return item !== index;
          });
        } else if (vm.search.strings.indexOf(index) === -1) {
          vm.search.strings.push(index);
        }
      }
      vm.items = ImagesService.getList();
      for (var i = 0; i < vm.search.strings.length; i++) {
        vm.items = vm.items.filter(function (el) {
          return el[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase().indexOf(vm.search[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase()) > -1;
        });
      }
      if (index === -1 || vm.search.string.length > 0) {
        vm.items = vm.items.filter(function (el) {
          for (var j = 0; j < vm.sort.columns.length; j++) {
            if (el[vm.sort.columns[j].name].toString().toLowerCase().indexOf(vm.search.string.toString().toLowerCase()) > -1) {
              return true;
            }
          }
          return false;
        });
      }
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.items.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.updateSort(vm.sort.active, false);
      if (vm.search.strings.length > 0 || vm.search.string.length > 0) {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS') + ' (' + $translation('Global.Table.FILTERED_FROM') + ' ' + ImagesService.getList().length + ' ' + $translation('Global.Table.RECORDS') + ')';
      } else {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS');
      }
    };

    vm.updateSort = function (index, change) {
      vm.sort.active = index;
      if (change) {
        if (vm.sort.columns[index].sort === 1) {
          vm.sort.columns[index].sort = 2;
        } else {
          vm.sort.columns[index].sort = 1;
        }
        for (var i = 0; i < vm.sort.columns.length; i++) {
          if (index === i) continue;
          vm.sort.columns[i].sort = 0;
        }
      }
      vm.items.sort(function(a,b) {
        var nameA, nameB;
        if (vm.sort.columns[index].sort===1) {
          if (index===0||index===3) {
            return a[vm.sort.columns[index].name] - b[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameA.localeCompare(nameB);
          }
        } else {
          if (index===0||index===3) {
            return b[vm.sort.columns[index].name] - a[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameB.localeCompare(nameA);
          }
        }
      });
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    ImagesService.get().then(function (response) {
      ImagesService.setList(response.data);
      vm.items = ImagesService.getList();
      vm.page.current = 1;
      vm.updateSort(vm.sort.active, false);
      vm.updateData();
      vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.items.length + ' ' + $translation('Global.Table.RECORDS');
    });

    vm.updateData = function () {
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.items.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.offset = (vm.limit.selected.name*vm.page.current)-vm.limit.selected.name;
      vm.itemsSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    vm.itemSelected = function(offset, limit) {
      return vm.items.filter(function(value, index) {
        return index >= offset && index < offset+limit;
      });
    };

    ImageCategoriesService.get().then(function (response) {
      ImageCategoriesService.setList(response.data);
      vm.categories = ImageCategoriesService.getList();
    });

    vm.modalDeleteSingle = function (id) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_single_tmpl.html',
        controller: 'ModalsDeleteSingleController',
        controllerAs: 'modal',
        resolve: {
          id: function () {
            return id;
          },
          ceyId: function () {
            return null;
          },
          index: function () {
            return null;
          },
          service: function () {
            return ImagesService;
          },
          attri: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    var getSelected = function () {
      var items = [];
      for (var i = 0; i < vm.items.length; i++)
      {
        if (vm.items[i].selected) {
          items.push({
            id: vm.items[i].iae_id,
            iae_path: vm.items[i].iae_path,
            iae_type: vm.items[i].iae_type
          });
        }
      }
      return items;
    };

    vm.modalChangeImageCategory = function(icyId) {
      var items = getSelected();

      if (items.length === 0) {
        return false;
      }

      var name = "";

      for (var i = 0; i < vm.categories.length; i++)
      {
        if (vm.categories[i].icy_id === icyId) {
          name = vm.categories[i].icy_name;
        }
      }

      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_change_image_category_multiple_tmpl.html',
        controller: 'ModalsChangeImageCategoryMultipleController',
        controllerAs: 'modal',
        resolve: {
          icyId: function () {
            return icyId;
          },
          name: function () {
            return name;
          },
          items: function () {
            return items;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    vm.modalDeleteMultiple = function () {
      var items = getSelected();

      if (items.length === 0) {
        return false;
      }
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_multiple_tmpl.html',
        controller: 'ModalsDeleteMultipleController',
        controllerAs: 'modal',
        resolve: {
          items: function () {
            return items;
          },
          ceyId: function () {
            return null;
          },
          service: function () {
            return ImagesService;
          },
          attri: function () {
            return null;
          },
          that: function () {
            return vm;
          }
        }
      });
    };
  }


})();

