(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ProductsDetailController', ProductsDetailController);

  /** @ngInject */
  function ProductsDetailController($log,ProductsService, $stateParams, $q, $scope, ParameterGroupsService, toastr, ManufacturerService, $filter, ImagesService, IMAGE_PATH, IMAGE_MEDIUM, IMAGE_ORIGINAL, ImageCategoriesService, FilesService, CategoriesService, PaymentsService, ShippingService, MetaKeywordsService, GroupsService) {
    var $translation = $filter('translate');
    var vm = this;
    var i;
    var j;
    vm.item = {};
    vm.images = [];
    vm.imagesSelected = [];
    vm.pathImages = IMAGE_PATH;
    vm.originalImage = IMAGE_ORIGINAL;
    vm.mediumImage = IMAGE_MEDIUM;
    vm.selectedIcyId = null;
    vm.selectedAllFiles = false;
    vm.selectedAllImages = false;
    vm.isSelectedImage = false;
    vm.imageSelectedMain = null;
    vm.parameterGroups = { model: 0, options: [] };
    vm.manufacturer = { options: [] };
    vm.metaKeywords = { options: [], selected: [] };
    vm.categories = { items: [], selected: [], pceMain: null };
    vm.files = [];
    vm.filesSelected = [];
    vm.limit = 10;
    vm.offset = 0;
    vm.page = { current: 1, total: 1 };
    vm.search = {
      string: ""
    };
    vm.sort = {
      columns: [
        {name: "fie_name", sort: 1},
        {name: "fie_type", sort: 0},
        {name: "fie_size", sort: 0}
      ],
      options: [
        "sorting", "sorting_asc", "sorting_desc"
      ],
      active: 0
    };

    vm.changeNoVat = function () {
      vm.item.price_vat = parseFloat(vm.item.price_no_vat * 1.21);
    };

    vm.changeVat = function () {
      vm.item.price_no_vat = parseFloat(vm.item.price_vat/1.21);
    };

    vm.selectAllFiles = function () {
      angular.forEach(vm.files, function (value) {
        value.selected = vm.selectedAllFiles;
      });
    };

    vm.shippingChange = function (item) {
      if (item.dontShow) {
        item.selected = false;
        item.price = null;
      }
      if (!item.selected) {
        item.price = null;
      }
    };

    vm.paramChanged = function ($tag, item) {
      item.value = [$tag.text];
    };

    vm.searchItem = function (options, $query) {
      return options.filter(function(name) {
        if (name !== null) {
          return name.includes($query);
        } else return false;
      });
    };

    vm.selectAllImages = function () {
      angular.forEach(vm.images, function(image) {
        image.selected = vm.selectedAllImages;
      });
    };

    vm.selectImage = function(index) {
      vm.images[index].selected = !vm.images[index].selected;
    };

    vm.selectImageMain = function (index) {
      if (vm.imageSelectedMain !== null && angular.isDefined(vm.images[vm.imageSelectedMain])) {
        vm.images[vm.imageSelectedMain].selectedMain = false;
      }
      vm.imageSelectedMain = index;
      vm.images[index].selectedMain = true;
    };

    var isInArray = function (haystack, needle) {
      var rValue = 0;
      angular.forEach(haystack, function (value) {
        if (value.iae_id === needle) {
          rValue = 1;
          return false;
        }
      });
      return rValue;
    };

    vm.selectImageCategory = function (icyId) {
      vm.selectedIcyId = icyId;
      ImagesService.getByIcyId(icyId).then(function (response) {
        vm.images = [];
        for (i = 0; i < vm.imagesSelected.length; i++) {
          vm.images.push(vm.imagesSelected[i]);
          if (vm.imagesSelected[i].selectedMain) {
            vm.imageSelectedMain = i;
          }
        }
        for (j = 0; j < response.data.length; j++) {
          if (isInArray(vm.imagesSelected, response.data[j].iae_id) === 0) {
            vm.images.push(response.data[j]);
          }
        }
      });
    };

    vm.selectCategory = function (item) {
      if (item.selected && !vm.categories.selected.includes(item.cey_id)) {
        vm.categories.selected.push(item.cey_id);
      }
      if (!item.selected) {
        vm.categories.selected = vm.categories.selected.filter(function (x) {
          return x !== item.cey_id;
        });
      }
    };

    vm.selectCategoryMain = function (ceyId) {
      vm.categories.pceMain = ceyId;
      if (!vm.categories.selected.includes(ceyId)) {
        vm.categories.selected.push(ceyId);
      }
    };

    var paramPromise = ParameterGroupsService.get().then(function (response) {
      vm.parameterGroups.options = response.data;
    });

    var manPromise = ManufacturerService.get().then(function (response) {
      vm.manufacturer.options = response.data;
    });

    var imaPromise = ImageCategoriesService.get().then(function (response) {
      vm.imageCategories = response.data;
    });

    var imaAllPromise = ImagesService.get().then(function (response) {
      vm.imagesAll = response.data;
    });

    var catPromise = CategoriesService.get().then(function (response) {
      vm.categories.items = response.data;
    });

    vm.searchData = function () {
      vm.files = FilesService.getList();
      vm.files = vm.files.filter(function (el) {
        for (var j = 0; j < vm.sort.columns.length; j++) {
          if (el[vm.sort.columns[j].name].toString().toLowerCase().indexOf(vm.search.string.toString().toLowerCase()) > -1) {
            return true;
          }
        }
        return false;
      });
      vm.filesSelected = vm.itemSelected(vm.offset, vm.limit);
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.files.length/vm.limit);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.updateSort(vm.sort.active, false);
      if (vm.search.string.length > 0) {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.files.length + ' ' + $translation('Global.Table.RECORDS') + ' (' + $translation('Global.Table.FILTERED_FROM') + ' ' + FilesService.getList().length + ' ' + $translation('Global.Table.RECORDS') + ')';
      } else {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.files.length + ' ' + $translation('Global.Table.RECORDS');
      }
    };

    vm.updateSort = function (index, change) {
      vm.sort.active = index;
      if (change) {
        if (vm.sort.columns[index].sort === 1) {
          vm.sort.columns[index].sort = 2;
        } else {
          vm.sort.columns[index].sort = 1;
        }
        for (var i = 0; i < vm.sort.columns.length; i++) {
          if (index === i) continue;
          vm.sort.columns[i].sort = 0;
        }
      }
      vm.files.sort(function(a,b) {
        var nameA, nameB;
        if (vm.sort.columns[index].sort===1) {
          if (index===2) {
            return a[vm.sort.columns[index].name] - b[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameA.localeCompare(nameB);
          }
        } else {
          if (index===2) {
            return b[vm.sort.columns[index].name] - a[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameB.localeCompare(nameA);
          }
        }
      });
      vm.filesSelected = vm.itemSelected(vm.offset, vm.limit);
    };

    vm.updateData = function () {
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.files.length/vm.limit);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.offset = (vm.limit*vm.page.current)-vm.limit;
      vm.filesSelected = vm.itemSelected(vm.offset, vm.limit);
    };

    vm.itemSelected = function(offset, limit) {
      return vm.files.filter(function(value, index) {
        return index >= offset && index < offset+limit;
      });
    };

    var filePromise = FilesService.get().then(function (response) {
      FilesService.setList(response.data);
      vm.files = FilesService.getList();
      vm.page.current = 1;
      vm.updateSort(vm.sort.active, false);
      vm.updateData();
      vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.files.length + ' ' + $translation('Global.Table.RECORDS');
    });

    var payPromise = PaymentsService.get().then(function (response) {
      vm.payment = response.data;
      angular.forEach(vm.payment, function (value) {
        value.enable = true;
      });
    });

    var shipPromise = ShippingService.get().then(function (response) {
      vm.shipping = response.data;
    });

    var metaPromise = MetaKeywordsService.get().then(function (response) {
      vm.metaKeywords.options = response.data;
    });

    var grPromise = GroupsService.get().then(function (response) {
      vm.groups = response.data;
    });

    var selectCategories = function(items, selected) {
      angular.forEach(items, function (item) {
        angular.forEach(selected, function (sel) {
          if (item.cey_id === sel) {
            item.selected = true;
          }
        });
        selectCategories(item.items, selected);
      });
    };

    vm.collapseAll = function() {
      $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    vm.expandAll = function() {
      $scope.$broadcast('angular-ui-tree:expand-all');
    };

    $q.all([paramPromise, imaAllPromise, catPromise, paramPromise, shipPromise, metaPromise, grPromise, payPromise, filePromise, imaPromise, manPromise]).then(function() {
      ProductsService.getOne($stateParams.put_id).then(function (response) {
        ProductsService.setList(response.data);
        vm.item = ProductsService.getList();
        vm.manufacturer.model = vm.item.mur_id;
        for (i = 0; i < vm.item.categories.length; i++) {
          vm.categories.selected.push(vm.item.categories[i].cey_id);
          if (vm.categories.pceMain === null && (vm.item.categories[i].pce_main || vm.item.categories.length-1 === i)) {
            vm.categories.pceMain = vm.item.categories[i].cey_id;
          }
        }
        for (i = 0; i < vm.item.shippings.length; i++) {
          for (j = 0; j < vm.shipping.length; j++) {
            if (vm.item.shippings[i].spg_id === vm.shipping[j].spg_id) {
              vm.shipping[j].price = vm.item.shippings[i].fixed_price;
              vm.shipping[j].selected = vm.item.shippings[i].show_shipping;
              vm.shipping[j].dontShow = !vm.item.shippings[i].show_shipping;
            }
          }
        }
        for (i = 0; i < vm.item.files.length; i++) {
          for (j = 0; j < vm.files.length; j++) {
            if (vm.item.files[i].fie_id === vm.files[j].fie_id) {
              vm.files[j].selected = true;
            }
          }
        }
        for (i = 0; i < vm.item.group_discounts.length; i++) {
          for (j = 0; j < vm.groups.length; j++) {
            if (vm.item.group_discounts[i].grp_id === vm.groups[j].grp_id) {
              vm.groups[j].discount = vm.item.group_discounts[i].discount;
            }
          }
        }
        for (i = 0; i < vm.item.payments.length; i++) {
          for (j = 0; j < vm.payment.length; j++) {
            if (vm.item.payments[i].pmt_id === vm.payment[j].id) {
              vm.payment[j].enable = false;
            }
          }
        }
        if (vm.item.pgp_id !== null) {
          for (i = 0; i < vm.parameterGroups.options.length; i++) {
            if (vm.parameterGroups.options[i].pgp_id === vm.item.pgp_id) {
              vm.parameterGroups.model = vm.parameterGroups.options[i];
            }
          }
          for (j = 0; j < vm.item.parameter_values.length; j++) {
            for (i = 0; i < vm.parameterGroups.model.parameters.length; i++) {
              if (vm.parameterGroups.model.parameters[i].prr_id === vm.item.parameter_values[j].prr_id) {
                if (vm.item.parameter_values[j].pve_value !== null) {
                  vm.parameterGroups.model.parameters[i].value = [vm.item.parameter_values[j].pve_value];
                }
              }
            }
          }
        }
        for (i = 0; i < vm.item.meta_keywords.length; i++) {
          for (j = 0; j < vm.metaKeywords.options.length; j++) {
            if (vm.metaKeywords.options[j].id === vm.item.meta_keywords[i].mkd_id) {
              vm.metaKeywords.selected.push(vm.metaKeywords.options[j]);
            }
          }
        }
        for (i = 0; i < vm.item.images.length; i++) {
          for (j = 0; j < vm.imagesAll.length; j++) {
            if (vm.item.images[i].iae_id ===  vm.imagesAll[j].iae_id) {
              var image = {
                iae_id: vm.imagesAll[j].iae_id,
                iae_path: vm.imagesAll[j].iae_path,
                iae_name: vm.imagesAll[j].iae_name,
                iae_type: vm.imagesAll[j].iae_type,
                selected: true,
                selectedMain: vm.item.images[i].iae_main
              };
              vm.imagesSelected.push(image);
              vm.images.push(image);
              if (vm.item.images[i].iae_main) {
                vm.imageSelectedMain = vm.images.length-1;
              }
            }
          }
        }
        selectCategories(vm.categories.items, vm.categories.selected);
      });
    });

    vm.update = function () {
      var shipping = [];
      var categories = [];
      var files = [];
      var images = [];
      var group_discounts = [];
      var payments = [];
      var parameter_values = [];
      angular.forEach(vm.shipping, function (value) {
        if (value.selected) {
          shipping.push({
            spg_id: value.spg_id,
            show_shipping: true,
            fixed_price: angular.isUndefined(value.price) || value.price === null ? 0 : parseFloat(value.price)
          });
        }
        if (value.dontShow) {
          shipping.push({
            spg_id: value.spg_id,
            show_shipping: false,
            fixed_price: null
          });
        }
      });
      angular.forEach(vm.categories.selected, function(value) {
        categories.push({
          cey_id: value,
          pce_main: value === vm.categories.pceMain
        });
      });
      angular.forEach(vm.files, function(value) {
        if (value.selected) {
          files.push(value.fie_id);
        }
      });
      angular.forEach(vm.images, function(value) {
        if (value.selected) {
          images.push({
            iae_id: value.iae_id,
            iae_main: angular.isDefined(value.selectedMain) ? value.selectedMain : false
          });
        }
      });
      angular.forEach(vm.groups, function(value) {
        if (value.discount >= 0) {
          group_discounts.push({
            discount: angular.isUndefined(value.discount) || value.discount === null ? 0 : parseFloat(value.discount),
            grp_id: value.grp_id
          });
        }
      });
      angular.forEach(vm.payment, function(value) {
        if (!value.enable) {
          payments.push(value.id);
        }
      });
      angular.forEach(vm.parameterGroups.model.parameters, function(value) {
        parameter_values.push({
          prr_id: value.prr_id,
          pve_value: angular.isDefined(value.value) && value.value.length > 0 && value.value[0].text !== null && value.value[0].text.trim().length > 0 ? value.value[0].text.trim() : null
        });
      });

      var data = {
        pgp_id: angular.isDefined(vm.parameterGroups.model.pgp_id) ? vm.parameterGroups.model.pgp_id : null,
        mur_id: vm.manufacturer.model,
        code: vm.item.code,
        availability: angular.isUndefined(vm.item.availability) ? -1 : vm.item.availability,
        active: vm.item.active,
        barcode: angular.isUndefined(vm.item.barcode) || vm.item.barcode === null || vm.item.barcode.trim().length !== 13 ? null : vm.item.barcode.trim(),
        description_short: angular.isDefined(vm.item.description_short) && vm.item.description_short !== null && vm.item.description_short.trim().length > 0 ? vm.item.description_short.trim() : null,
        description_long: angular.isDefined(vm.item.description_long) && vm.item.description_long !== null && vm.item.description_long.trim().length > 0 ? vm.item.description_long.trim() : null,
        warranty: angular.isUndefined(vm.item.warranty) || vm.item.warranty < 0 ? null : vm.item.warranty,
        description_meta: angular.isDefined(vm.item.description_meta) && vm.item.description_meta !== null && vm.item.description_meta.trim().length > 0 ? vm.item.description_meta.trim() : null,
        sellable: vm.item.sellable,
        sync_feed_status: vm.item.sync_feed_status,
        phy_name: vm.item.phy_name,
        price_vat: angular.isUndefined(vm.item.price_vat) ? 0 : vm.item.price_vat,
        price_no_vat: angular.isUndefined(vm.item.price_no_vat) ? 0 : vm.item.price_no_vat,
        weight: angular.isUndefined(vm.item.weight) || vm.item.weight < 0 ? null : vm.item.weight,
        option_title: angular.isDefined(vm.item.option_title) && vm.item.option_title !== null && vm.item.option_title.trim().length > 0 ? vm.item.option_title.trim() : null,
        shippings: shipping,
        categories: categories,
        files: files,
        images: images,
        group_discounts: group_discounts,
        payments: payments,
        menu_options: angular.isUndefined(vm.item.menu_options) ? [] : vm.item.menu_options,
        parameter_values: parameter_values,
        meta_keywords: vm.metaKeywords.selected,
        sale: vm.item.sale
      };

      ProductsService.put(data, $stateParams.put_id).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

  }


})();
