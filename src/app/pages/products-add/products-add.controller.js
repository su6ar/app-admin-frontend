(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ProductsAddController', ProductsAddController);

  /** @ngInject */
  function ProductsAddController(ProductsService, ParameterGroupsService, toastr, ManufacturerService, $filter, ImagesService, IMAGE_MEDIUM, IMAGE_PATH, IMAGE_ORIGINAL, ImageCategoriesService, FilesService, CategoriesService, PaymentsService, ShippingService, MetaKeywordsService, GroupsService) {
    var $translation = $filter('translate');
    var vm = this;
    vm.item = {};
    vm.pathImages = IMAGE_PATH;
    vm.originalImage = IMAGE_ORIGINAL;
    vm.mediumImage = IMAGE_MEDIUM;
    vm.selectedIcyId = null;
    vm.item.sellable = true;
    vm.item.active = true;
    vm.item.sale = false;
    vm.selectedAllFiles = false;
    vm.selectedAllImages = false;
    vm.isSelectedImage = false;
    vm.imageSelectedMain = null;
    vm.parameterGroups = { model: 0, options: [] };
    vm.manufacturer = { options: [] };
    vm.metaKeywords = { options: [], selected: [] };
    vm.categories = { items: [], selected: [], pceMain: null };
    vm.files = [];
    vm.filesSelected = [];
    vm.limit = 10;
    vm.offset = 0;
    vm.page = { current: 1, total: 1 };
    vm.search = {
      string: ""
    };
    vm.sort = {
      columns: [
        {name: "fie_name", sort: 1},
        {name: "fie_type", sort: 0},
        {name: "fie_size", sort: 0}
      ],
      options: [
        "sorting", "sorting_asc", "sorting_desc"
      ],
      active: 0
    };

    vm.changeNoVat = function () {
      vm.item.price_vat = parseFloat(vm.item.price_no_vat * 1.21);
    };

    vm.changeVat = function () {
      vm.item.price_no_vat = parseFloat(vm.item.price_vat/1.21);
    };

    vm.selectAllFiles = function () {
      angular.forEach(vm.files, function (value) {
        value.selected = vm.selectedAllFiles;
      });
    };

    vm.shippingChange = function (item) {
      if (item.dontShow) {
        item.selected = false;
        item.price = null;
      }
      if (!item.selected) {
        item.price = null;
      }
    };

    vm.selectAllImages = function () {
      angular.forEach(vm.images, function(image) {
        image.selected = vm.selectedAllImages;
      });
    };

    vm.selectImage = function(index) {
      vm.images[index].selected = !vm.images[index].selected;
    };

    vm.selectImageMain = function (index) {
      if (vm.imageSelectedMain !== null && angular.isDefined(vm.images[vm.imageSelectedMain])) {
        vm.images[vm.imageSelectedMain].selectedMain = false;
      }
      vm.imageSelectedMain = index;
      vm.images[index].selectedMain = true;
    };

    vm.selectImageCategory = function (icyId) {
      vm.selectedIcyId = icyId;
      ImagesService.getByIcyId(icyId).then(function (response) {
        vm.images = response.data;
      });
    };

    vm.selectCategory = function (item) {
      if (item.selected && !vm.categories.selected.includes(item.cey_id)) {
        vm.categories.selected.push(item.cey_id);
      }
      if (!item.selected) {
        vm.categories.selected = vm.categories.selected.filter(function (x) {
          return x !== item.cey_id;
        });
      }
    };

    vm.paramChanged = function ($tag, item) {
      item.value = [$tag.text];
    };

    vm.searchItem = function (options, $query) {
      return options.filter(function(name) {
        if (name !== null) {
          return name.includes($query);
        } else return false;
      });
    };

    vm.selectCategoryMain = function (ceyId) {
      vm.categories.pceMain = ceyId;
      if (!vm.categories.selected.includes(ceyId)) {
        vm.categories.selected.push(ceyId);
      }
    };

    ParameterGroupsService.get().then(function (response) {
      vm.parameterGroups.options = response.data;
    });

    ManufacturerService.get().then(function (response) {
      vm.manufacturer.options = response.data;
      vm.manufacturer.model = vm.manufacturer.options[0].id;
    });

    ImageCategoriesService.get().then(function (response) {
      vm.imageCategories = response.data;
    });

    CategoriesService.get().then(function (response) {
      vm.categories.items = response.data;
    });

    vm.searchData = function () {
      vm.files = FilesService.getList();
      vm.files = vm.files.filter(function (el) {
        for (var j = 0; j < vm.sort.columns.length; j++) {
          if (el[vm.sort.columns[j].name].toString().toLowerCase().indexOf(vm.search.string.toString().toLowerCase()) > -1) {
            return true;
          }
        }
        return false;
      });
      vm.filesSelected = vm.itemSelected(vm.offset, vm.limit);
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.files.length/vm.limit);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.updateSort(vm.sort.active, false);
      if (vm.search.string.length > 0) {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.files.length + ' ' + $translation('Global.Table.RECORDS') + ' (' + $translation('Global.Table.FILTERED_FROM') + ' ' + FilesService.getList().length + ' ' + $translation('Global.Table.RECORDS') + ')';
      } else {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.files.length + ' ' + $translation('Global.Table.RECORDS');
      }
    };

    vm.updateSort = function (index, change) {
      vm.sort.active = index;
      if (change) {
        if (vm.sort.columns[index].sort === 1) {
          vm.sort.columns[index].sort = 2;
        } else {
          vm.sort.columns[index].sort = 1;
        }
        for (var i = 0; i < vm.sort.columns.length; i++) {
          if (index === i) continue;
          vm.sort.columns[i].sort = 0;
        }
      }
      vm.files.sort(function(a,b) {
        var nameA, nameB;
        if (vm.sort.columns[index].sort===1) {
          if (index===2) {
            return a[vm.sort.columns[index].name] - b[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameA.localeCompare(nameB);
          }
        } else {
          if (index===2) {
            return b[vm.sort.columns[index].name] - a[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name].toUpperCase();
            return nameB.localeCompare(nameA);
          }
        }
      });
      vm.filesSelected = vm.itemSelected(vm.offset, vm.limit);
    };

    vm.updateData = function () {
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.files.length/vm.limit);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.offset = (vm.limit*vm.page.current)-vm.limit;
      vm.filesSelected = vm.itemSelected(vm.offset, vm.limit);
    };

    vm.itemSelected = function(offset, limit) {
      return vm.files.filter(function(value, index) {
        return index >= offset && index < offset+limit;
      });
    };

    FilesService.get().then(function (response) {
      FilesService.setList(response.data);
      vm.files = FilesService.getList();
      vm.page.current = 1;
      vm.updateSort(vm.sort.active, false);
      vm.updateData();
      vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.files.length + ' ' + $translation('Global.Table.RECORDS');
    });

    PaymentsService.get().then(function (response) {
      vm.payment = response.data;
      angular.forEach(vm.payment, function (value) {
        value.enable = true;
      });
    });

    ShippingService.get().then(function (response) {
      vm.shipping = response.data;
    });

    MetaKeywordsService.get().then(function (response) {
      vm.metaKeywords.options = response.data;
    });

    GroupsService.get().then(function (response) {
      vm.groups = response.data;
    });

    vm.add = function () {
      var shipping = [];
      var categories = [];
      var files = [];
      var images = [];
      var group_discounts = [];
      var payments = [];
      var parameter_values = [];
      angular.forEach(vm.shipping, function (value) {
        if (value.selected) {
          shipping.push({
            spg_id: value.spg_id,
            show_shipping: true,
            fixed_price: value.price
          });
        }
        if (value.dontShow) {
          shipping.push({
            spg_id: value.spg_id,
            show_shipping: false,
            fixed_price: null
          });
        }
      });
      angular.forEach(vm.categories.selected, function(value) {
        categories.push({
          cey_id: value,
          pce_main: value === vm.categories.pceMain
        });
      });
      angular.forEach(vm.files, function(value) {
        if (value.selected) {
          files.push(value.fie_id);
        }
      });
      angular.forEach(vm.images, function(value) {
        if (value.selected) {
          images.push({
            iae_id: value.iae_id,
            iae_main: angular.isDefined(value.selectedMain) ? value.selectedMain : false
          });
        }
      });
      angular.forEach(vm.groups, function(value) {
        if (value.discount >= 0) {
          group_discounts.push({
            discount: angular.isUndefined(value.discount) || value.discount === null ? 0 : parseFloat(value.discount),
            grp_id: value.grp_id
          });
        }
      });
      angular.forEach(vm.payment, function(value) {
        if (!value.enable) {
          payments.push(value.id);
        }
      });
      angular.forEach(vm.parameterGroups.model.parameters, function(value) {
        parameter_values.push({
          prr_id: value.prr_id,
          pve_value: angular.isDefined(value.value) && value.value.length > 0 && value.value[0].text !== null && value.value[0].text.trim().length > 0 ? value.value[0].text.trim() : null
        });
      });

      var data = {
        pgp_id: angular.isDefined(vm.parameterGroups.model.pgp_id) ? vm.parameterGroups.model.pgp_id : null,
        mur_id: vm.manufacturer.model,
        code: vm.item.code,
        availability: angular.isUndefined(vm.item.availability) ? -1 : vm.item.availability,
        active: vm.item.active,
        barcode: angular.isUndefined(vm.item.barcode) || vm.item.barcode === null || vm.item.barcode.trim().length !== 13 ? null : vm.item.barcode.trim(),
        description_short: angular.isDefined(vm.item.description_short) && vm.item.description_short !== null && vm.item.description_short.trim().length > 0 ? vm.item.description_short.trim() : null,
        description_long: angular.isDefined(vm.item.description_long) && vm.item.description_long !== null && vm.item.description_long.trim().length > 0 ? vm.item.description_long.trim() : null,
        warranty: angular.isUndefined(vm.item.warranty) || vm.item.warranty < 0 ? null : vm.item.warranty,
        description_meta: angular.isDefined(vm.item.description_meta) && vm.item.description_meta !== null && vm.item.description_meta.trim().length > 0 ? vm.item.description_meta.trim() : null,
        sellable: vm.item.sellable,
        phy_name: vm.item.phy_name,
        price_vat: angular.isUndefined(vm.item.price_vat) ? 0 : vm.item.price_vat,
        price_no_vat: angular.isUndefined(vm.item.price_no_vat) ? 0 : vm.item.price_no_vat,
        weight: angular.isUndefined(vm.item.weight) || vm.item.weight < 0 ? null : vm.item.weight,
        option_title: angular.isDefined(vm.item.option_title) && vm.item.option_title !== null && vm.item.option_title.trim().length > 0 ? vm.item.option_title.trim() : null,
        shippings: shipping,
        categories: categories,
        files: files,
        images: images,
        group_discounts: group_discounts,
        payments: payments,
        menu_options: angular.isUndefined(vm.item.menu_options) ? [] : vm.item.menu_options,
        parameter_values: parameter_values,
        meta_keywords: vm.metaKeywords.selected,
        sale: vm.item.sale
      };

      ProductsService.post(data).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

  }


})();
