(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('CategoriesAddController', CategoriesAddController);

  /** @ngInject */
  function CategoriesAddController(toastr, IMAGE_PATH, IMAGE_ORIGINAL, IMAGE_MEDIUM, $filter, ImagesService, ImageCategoriesService, CategoriesService, GroupsService, ParameterGroupsService) {
    var vm = this;
    var $translation = $filter('translate');

    vm.pathImages = IMAGE_PATH;
    vm.originalImage = IMAGE_ORIGINAL;
    vm.mediumImage = IMAGE_MEDIUM;
    vm.item = {};
    vm.parameterGroups = { model: 0, options: [] };
    vm.item.show_parameters = 0;
    vm.selectedParent = 0;
    vm.imageCategories = [];
    vm.selectedIcyId = null;
    vm.imageSelectedMain = null;
    vm.images = [];

    vm.selectImageCategory = function (icyId) {
      vm.selectedIcyId = icyId;
      ImagesService.getByIcyId(icyId).then(function (response) {
        vm.images = response.data;
      });
    };

    vm.isInArray = function (index) {
      for (var i = 0; i < vm.images.length; i++) {
        if (vm.images[i].iae_id === index) {
          return i;
        }
      }
      return false;
    };

    vm.selectImageMain = function (index) {
      var ind = vm.isInArray(index);
      if (vm.imageSelectedMain !== null && angular.isDefined(vm.images[vm.imageSelectedMain])) {
        vm.images[vm.imageSelectedMain].selectedMain = false;
      }
      vm.imageSelectedMain = ind;
      vm.images[ind].selectedMain = true;
    };

    ImageCategoriesService.get().then(function (response) {
      vm.imageCategories = response.data;
    });

    GroupsService.get().then(function (response) {
      GroupsService.setList(response.data);
      vm.item.groups = GroupsService.getList();
    });

    ParameterGroupsService.get().then(function (response) {
      ParameterGroupsService.setList(response.data);
      vm.parameterGroups.options = ParameterGroupsService.getList();
    });

    CategoriesService.get().then(function (response) {
      CategoriesService.setList(response.data);
      vm.items = [{
        "cey_id": 0,
        "cey_cey_id": null,
        "cey_name": "Hlavní kategorie",
        "cey_place": null,
        "items": CategoriesService.getList()
      }];
    });

    vm.add = function () {
      var discounts = [];
      angular.forEach(vm.item.groups, function (value) {
        discounts.push({
          grp_id: value.grp_id,
          discount: angular.isUndefined(value.discount) ? 0 : value.discount
        });
      });

      var data = {
        pgp_id: vm.parameterGroups.model === 0 ? null : vm.parameterGroups.model,
        cey_cey_id: vm.selectedParent === 0 ? null : vm.selectedParent,
        cey_name: vm.item.cey_name,
        show_parameters: vm.item.show_parameters,
        cey_description: angular.isUndefined(vm.item.cey_description) ? null : vm.item.cey_description,
        discounts: discounts,
        iae_id: vm.imageSelectedMain !== null && angular.isDefined(vm.images[vm.imageSelectedMain]) ? vm.images[vm.imageSelectedMain].iae_id : null
      };

      CategoriesService.post(data).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

