(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ArticlesDetailController', ArticlesDetailController);

  /** @ngInject */
  function ArticlesDetailController($state, $stateParams, ArticlesService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = {};

    ArticlesService.getOne($stateParams.ate_id).then(function (response) {
      ArticlesService.setList(response.data);
      vm.item = ArticlesService.getList();
      vm.item.ahy_content = angular.isUndefined(vm.item.history[0].ahy_content) ? "" : vm.item.history[0].ahy_content;
    });

    vm.update = function () {
      var data = {
        ate_name: vm.item.ate_name,
        ahy_content: vm.item.ahy_content
      };

      ArticlesService.put(data, vm.item.ate_id).then(function (response) {
        if (response.data.ate_id) {
          $state.go("app.articles.detail", {ate_id: response.data.ate_id});
        }
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
