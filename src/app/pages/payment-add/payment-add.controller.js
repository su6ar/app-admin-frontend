(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('PaymentAddController', PaymentAddController);

  /** @ngInject */
  function PaymentAddController(PaymentsService, $filter, toastr) {
    var vm = this;
    var $translation = $filter('translate');

    vm.cashOnDelivery = 1;

    vm.add = function() {
      var coy = vm.cashOnDelivery === 1 ? true : null;

      PaymentsService.post(coy, vm.poyName, vm.addPercentageOottPrice, vm.price).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
