(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ShippingController', ShippingController);

  /** @ngInject */
  function ShippingController(toastr, $uibModal, $filter, ShippingService) {
    var $translation = $filter('translate');
    var vm = this;
    vm.weightMin = 0;
    vm.weightMax = 1;
    vm.price = 0;
    vm.shyIds = [];
    vm.shipping = [];
    vm.shippingSelected = [];
    vm.selectedAll = false;
    vm.limit = { options: [
      {id: 1, name: 10},
      {id: 2, name: 25},
      {id: 3, name: 50},
      {id: 4, name: 100}
    ], selected: {id: 1, name: 10}};
    vm.offset = 0;
    vm.page = { current: 1, total: 1 };
    vm.search = {
      string: "",
      strings: []
    };
    vm.sort = {
      columns: [
        {name: "shy_id", sort: 1},
        {name: "shy_name", sort: 0},
        {name: "weight_min", sort: 0},
        {name: "weight_max", sort: 0},
        {name: "price", sort: 0},
        {name: "show_on_default", sort: 0},
        {name: "sum_weight_price", sort: 0}
      ],
      options: [
        "sorting", "sorting_asc", "sorting_desc"
      ],
      active: 0
    };

    vm.selectAll = function () {
      angular.forEach(vm.shipping, function(shipping) {
        shipping.selected = vm.selectedAll;
      });
    };

    vm.searchData = function (index) {
      var value = index !== -1 ? vm.search[vm.sort.columns[index].name].toString() : vm.search.string.toString();
      var a = "";
      if (index !== -1) {
        if (value.length === 0) {
          vm.search.strings = vm.search.strings.filter(function (item) {
            return item !== index;
          });
        } else if (vm.search.strings.indexOf(index) === -1) {
          vm.search.strings.push(index);
        }
      }
      vm.shipping = ShippingService.getList();
      for (var i = 0; i < vm.search.strings.length; i++) {
        vm.shipping = vm.shipping.filter(function (el) {
          if (el[vm.sort.columns[vm.search.strings[i]].name] === null) {
            return a.indexOf(vm.search[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase()) > -1;
          } else {
            return el[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase().indexOf(vm.search[vm.sort.columns[vm.search.strings[i]].name].toString().toLowerCase()) > -1;
          }
        });
      }
      if (index === -1 || vm.search.string.length > 0) {
        vm.shipping = vm.shipping.filter(function (el) {
          for (var j = 0; j < vm.sort.columns.length; j++) {
            if (j === 0||j === 1||j === 5||j === 6) {
              if (el[vm.sort.columns[j].name] === null) {
                if (a.indexOf(vm.search.string.toString().toLowerCase()) > -1) {
                  return true;
                }
              } else {
                if (el[vm.sort.columns[j].name].toString().toLowerCase().indexOf(vm.search.string.toString().toLowerCase()) > -1) {
                  return true;
                }
              }
            }
          }
          return false;
        });
      }
      vm.shippingSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.shipping.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.updateSort(vm.sort.active, false);
      if (vm.search.strings.length > 0 || vm.search.string.length > 0) {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.shipping.length + ' ' + $translation('Global.Table.RECORDS') + ' (' + $translation('Global.Table.FILTERED_FROM') + ' ' + ShippingService.getList().length + ' ' + $translation('Global.Table.RECORDS') + ')';
      } else {
        vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.shipping.length + ' ' + $translation('Global.Table.RECORDS');
      }
    };

    vm.updateSort = function (index, change) {
      vm.sort.active = index;
      if (change) {
        if (vm.sort.columns[index].sort === 1) {
          vm.sort.columns[index].sort = 2;
        } else {
          vm.sort.columns[index].sort = 1;
        }
        for (var i = 0; i < vm.sort.columns.length; i++) {
          if (index === i) continue;
          vm.sort.columns[i].sort = 0;
        }
      }
      vm.shipping.sort(function(a,b) {
        var nameA, nameB;
        if (vm.sort.columns[index].sort===1) {
          if (index===0||index===2||index===3||index===4||index===5||index===6) {
            return a[vm.sort.columns[index].name] - b[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name] === null ? "NULL" : a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name] === null ? "NULL" : b[vm.sort.columns[index].name].toUpperCase();
            return nameA.localeCompare(nameB);
          }
        } else {
          if (index===0||index===2||index===3||index===4||index===5||index===6) {
            return b[vm.sort.columns[index].name] - a[vm.sort.columns[index].name];
          } else {
            nameA = a[vm.sort.columns[index].name] === null ? "NULL" : a[vm.sort.columns[index].name].toUpperCase();
            nameB = b[vm.sort.columns[index].name] === null ? "NULL" : b[vm.sort.columns[index].name].toUpperCase();
            return nameB.localeCompare(nameA);
          }
        }
      });
      vm.shippingSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    vm.updateData = function () {
      vm.page.current = parseInt(vm.page.current);
      vm.page.total = Math.ceil(vm.shipping.length/vm.limit.selected.name);
      if (vm.page.current<1) {
        vm.page.current = 1;
      }
      if (vm.page.current>vm.page.total) {
        vm.page.current = vm.page.total;
      }
      vm.offset = (vm.limit.selected.name*vm.page.current)-vm.limit.selected.name;
      vm.shippingSelected = vm.itemSelected(vm.offset, vm.limit.selected.name);
    };

    vm.itemSelected = function(offset, limit) {
      return vm.shipping.filter(function(value, index) {
        return index >= offset && index < offset+limit;
      });
    };

    ShippingService.get().then(function (response) {
      ShippingService.setList(response.data);
      vm.shipping = ShippingService.getList();
      angular.forEach(vm.shipping, function (value) {
          vm.shyIds.push(value.shy_id);
      });
      vm.selectedShyId = vm.shyIds[0];
      vm.page.current = 1;
      vm.updateSort(vm.sort.active, false);
      vm.updateData();
      vm.page.msgFound = $translation('Global.Table.FOUND') + ' ' + vm.shipping.length + ' ' + $translation('Global.Table.RECORDS');
    });

    vm.add = function () {
      ShippingService.postOne(vm.selectedShyId, vm.weightMin, vm.weightMax, vm.price).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
        ShippingService.get().then(function (response) {
          ShippingService.setList(response.data);
          vm.shipping = ShippingService.getList();
        });
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

    vm.put = function (shyId, shtId) {
      ShippingService.putSht(shtId, shyId, vm.updateWeightMin, vm.updateWeightMax, vm.updatePrice).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
        ShippingService.get().then(function (response) {
          ShippingService.setList(response.data);
          vm.shipping = ShippingService.getList();
        });
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

    vm.modalDeleteSingleSpg = function (spg_id, sht_id) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_single_tmpl.html',
        controller: 'ModalsDeleteSingleSpgController',
        controllerAs: 'modal',
        resolve: {
          spg_id: function () {
            return spg_id;
          },
          sht_id: function () {
            return sht_id;
          },
          service: function () {
            return ShippingService;
          },
          that: function () {
            return vm;
          }
        }
      });
    };

    var getSelectedSpgs = function () {
      var spgs = [];
      var shts = [];
      for (var i = 0; i < vm.shipping.length; i++)
      {
        if (vm.shipping[i].selected) {
          spgs.push(vm.shipping[i].spg_id);
        }

        for (var j = 0; j < vm.shipping[i].items.length; j++)
        {
          if (vm.shipping[i].items[j].selected) {
            shts.push(vm.shipping[i].items[j].sht_id);
          }
        }
      }
      return {spgs: spgs, shts: shts};
    };

    vm.modalDeleteMultipleSpg = function () {
      var ids = getSelectedSpgs();

      $uibModal.open({
        animation: true,
        templateUrl: 'app/components/templates/modal_delete_multiple_spg_tmpl.html',
        controller: 'ModalsDeleteMultipleSpgController',
        controllerAs: 'modal',
        resolve: {
          items: function () {
            return ids;
          },
          service: function () {
            return ShippingService;
          },
          that: function () {
            return vm;
          }
        }
      });
    };
  }


})();

