(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ImagesCategoryAddController', ImagesCategoryAddController);

  /** @ngInject */
  function ImagesCategoryAddController(ImageCategoriesService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');

    vm.markings = [
      {
        mig_markings: [ "1", "2", "3", "4", "5", "6" ],
        mig_name: "Produkty",
        mig_id: 0
      },
      {
        mig_markings: [ "1", "m", "6" ],
        mig_name: "Články, novinky, stránky",
        mig_id: 1
      },
      {
        mig_markings: [ "1", "r", "6" ],
        mig_name: "Reference",
        mig_id: 2
      }
    ];

    vm.selectedMarkings = [];

    vm.markingChange = function (migId) {
      if (vm.selectedMarkings.includes(migId)) {
        vm.selectedMarkings = vm.selectedMarkings.filter(function (item) {
          return item !== migId;
        });
      } else {
        vm.selectedMarkings.push(migId);
      }
    };

    vm.add = function () {
      var migMarkings = [];
      for (var i = 0; i < vm.selectedMarkings.length; i++)
      {
        for (var j = 0; j < vm.markings[vm.selectedMarkings[i]].mig_markings.length; j++)
        {
          if (!migMarkings.includes(vm.markings[vm.selectedMarkings[i]].mig_markings[j]))
          {
            migMarkings.push(vm.markings[vm.selectedMarkings[i]].mig_markings[j]);
          }
        }
      }

      ImageCategoriesService.post(vm.icyName, migMarkings).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

