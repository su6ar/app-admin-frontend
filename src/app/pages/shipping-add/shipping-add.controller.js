(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ShippingAddController', ShippingAddController);

  /** @ngInject */
  function ShippingAddController(ShippingTypeService, ShippingService, PaymentsService, $filter, toastr) {
    var vm = this;
    var $translation = $filter('translate');

    vm.payments = vm.types = [];
    vm.sumWeightPrice = vm.showOnDefault = 1;

    ShippingTypeService.get().then(function (response) {
      ShippingTypeService.setList(response.data);
      vm.types = ShippingTypeService.getList();
      vm.selected = { type: vm.types[0], payments: [] };
    });

    PaymentsService.get().then(function (response) {
      PaymentsService.setList(response.data);
      vm.payments = PaymentsService.getList();
    });

    vm.selected = { type: vm.types[0], payments: [] };

    vm.add = function add() {
      var payments = [], items = [];

      angular.forEach(vm.weightMax, function (value) {
        items.push({
          "weight_min": 0,
          "weight_max": value,
          "price": 0
        });
      });

      angular.forEach(vm.weightMin, function (value, key) {
        items[key]["weight_min"] = value;
      });

      angular.forEach(vm.price, function (value, key) {
        items[key]["price"] = value;
      });

      angular.forEach(vm.selected.payments, function (value) {
        payments.push({
          "pmt_id": value
        });
      });

      if (items.length === 0)
      {
        items.push({
          "weight_min": 0,
          "weight_max": 1,
          "price": 0
        });
      }
      var stt = angular.isUndefined(vm.showTillMaxWeight) ? null : vm.showTillMaxWeight;
      var fpe = angular.isUndefined(vm.freePriceAbove) ? null : vm.freePriceAbove;

      ShippingService.post(vm.selected.type.id, payments, vm.showOnDefault, stt, vm.name, fpe, vm.sumWeightPrice, items).then(function() {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };

    vm.paymentChange = function paymentChange (id) {
      if (vm.selected.payments.includes(id)) {
        vm.selected.payments = vm.selected.payments.filter(function (item) {
          return item !== id;
        });
      } else {
        vm.selected.payments.push(id);
      }
    }
  }


})();
