(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('LockedController', LockedController);

  /** @ngInject */
  function LockedController(localStorageService, ALERT_OPTIONS, AlertsService, $log, AccountsService, $filter, $state) {
    var vm = this;
    var $translation = $filter('translate');
    vm.currentUser = localStorageService.get("currentUser", localStorageService.getStorageType());
    vm.alerts = [];
    $log.log("been in auth-locked");

    vm.logOut = function() {
      localStorageService.clearAll('.*', 'sessionStorage');
      localStorageService.clearAll('.*', 'localStorage');
      $state.go('auth.login');
    };

    vm.unlock = function() {
      AccountsService.getToken(vm.currentUser.act_email, vm.password).then(function(success) {
        vm.alerts = AlertsService.showAlert(vm.alerts,
          $translation('Global.Alert.SUCCESS') + '!',
          ALERT_OPTIONS.colors[0].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
        localStorageService.set("token", success.data.token, localStorageService.getStorageType());
        localStorageService.remove("locked");
        $state.go('app.dashboard');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        vm.alerts = AlertsService.showAlert(vm.alerts,
          message,
          ALERT_OPTIONS.colors[2].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
      });
    };
  }

})();
