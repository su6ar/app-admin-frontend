(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController(AccountsService, $state, $filter, localStorageService, ALERT_OPTIONS, AlertsService) {
    var vm = this;
    var $translation = $filter('translate');
    vm.rememberme = true;
    vm.login = function() {
      vm.alerts = [];

      AccountsService.getToken(vm.email, vm.password).then(function(success) {
        vm.alerts = AlertsService.showAlert(vm.alerts,
          $translation('Global.Alert.SUCCESS') + '!',
          ALERT_OPTIONS.colors[0].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
        /*if(vm.rememberme === "undefined" || vm.rememberme === false) {
          localStorageService.set("token", success.data.token, "sessionStorage");
        } else {*/
          localStorageService.set("token", success.data.token, "localStorage");
        //}
        $state.go('app.dashboard');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        vm.alerts = AlertsService.showAlert(vm.alerts,
          message,
          ALERT_OPTIONS.colors[2].name,
          ALERT_OPTIONS.icons[0].value,
          ALERT_OPTIONS.durations[0].value,
          false,
          true,
          true
        );
      });

    }
  }

})();
