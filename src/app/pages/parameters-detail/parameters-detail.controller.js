(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ParametersDetailController', ParametersDetailController);

  /** @ngInject */
  function ParametersDetailController($stateParams, ParametersService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');
    vm.item = null;

    ParametersService.getOne($stateParams.prr_id).then(function (response) {
      vm.item = response.data;
    });

    vm.update = function () {
      vm.item.prr_unit = angular.isUndefined(vm.item.prr_unit) || vm.item.prr_unit === null || vm.item.prr_unit.trim().length === 0 ? null : vm.item.prr_unit;

      ParametersService.put(vm.item, vm.item.prr_id).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

