(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ParametersAddController', ParametersAddController);

  /** @ngInject */
  function ParametersAddController(ParametersService, toastr, $filter) {
    var vm = this;
    var $translation = $filter('translate');

    vm.add = function () {
      vm.item.prr_unit = angular.isUndefined(vm.item.prr_unit) || vm.item.prr_unit === null || vm.item.prr_unit.trim().length === 0 ? null : vm.item.prr_unit;

      ParametersService.post(vm.item).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();
