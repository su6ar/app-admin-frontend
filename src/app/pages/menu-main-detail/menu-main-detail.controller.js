(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('MenuMainDetailController', MenuMainDetailController);

  /** @ngInject */
  function MenuMainDetailController($stateParams, toastr, $filter, MenuMainService, CompanyService, MetaKeywordsService, ArticlesService) {
    var vm = this;
    var $translation = $filter('translate');

    vm.item = {};
    vm.articles = { model: 0, options: [] };
    vm.company = { model: 0, options: [] };
    vm.selectedParent = 0;
    vm.item.metaKeywords = {};
    vm.item.metaKeywords.options = [];
    vm.item.metaKeywords.selected = [];

    CompanyService.get().then(function (response) {
      CompanyService.setList(response.data);
      vm.company.options = CompanyService.getList();
    });

    MetaKeywordsService.get().then(function (response) {
      MetaKeywordsService.setList(response.data);
      vm.item.metaKeywords.options = MetaKeywordsService.getList();
    });

    ArticlesService.get().then(function (response) {
      ArticlesService.setList(response.data);
      vm.articles.options = ArticlesService.getList();
    });

    MenuMainService.getOne($stateParams.mim_id).then(function (response) {
      var data = response.data;
      vm.articles.model = data.ate_id === null ? 0 : data.ate_id;
      vm.company.model = data.cct_id;
      vm.selectedParent = data.mim_mim_id === null ? 0 : data.mim_mim_id;
      vm.item.mim_name = data.mim_name;
      vm.item.metaKeywords.selected = data.meta_keywords;
      vm.item.mim_id = data.mim_id;
      vm.item.meta_description = data.meta_description;
      vm.item.mim_ecommerce = data.mim_ecommerce;
      vm.item.mim_home = data.mim_home;
    });

    MenuMainService.get().then(function (response) {
      MenuMainService.setList(response.data);
      vm.items = [{
        "mim_id": 0,
        "mim_mim_id": null,
        "mim_name": "Hlavní menu",
        "mim_place": null,
        "items": MenuMainService.getList()
      }];
    });

    vm.update = function () {
      var metaKeywords = [];
      angular.forEach(vm.item.metaKeywords.selected, function (value) {
        metaKeywords.push(value.id);
      });
      var data = {
        ate_id: vm.articles.model === 0 ? null : vm.articles.model,
        mim_mim_id: vm.selectedParent === 0 ? null : vm.selectedParent,
        mim_name: vm.item.mim_name,
        meta_description: angular.isUndefined(vm.item.meta_description) ? null : vm.item.meta_description,
        mim_ecommerce: angular.isUndefined(vm.item.mim_ecommerce) ? null : vm.item.mim_ecommerce,
        mim_home: angular.isUndefined(vm.item.mim_home) ? null : vm.item.mim_home,
        cct_id: vm.company.model,
        meta_keywords: metaKeywords
      };

      MenuMainService.putOne(data, vm.item.mim_id).then(function () {
        toastr.success($translation('Global.Alert.SUCCESS') + '! ');
      }, function(error) {
        var message;
        try {
          message = error.data.message
        } catch(e) {
          message = $translation('Global.Error.UNKNOWN')
        }
        toastr.error(message, $translation('Global.Alert.ERROR') + '! ');
      });
    };
  }


})();

