(function() {
  'use strict';

  angular
    .module('minotaur')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, localStorageService, $state, $stateParams, AccountsService, TokenService) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    if (angular.isUndefined(localStorageService.get("nightMode", "localStorage")) || localStorageService.get("nightMode", "localStorage") === null) {
      $rootScope.nightMode = 0;
      localStorageService.set("nightMode", 0, "localStorage");
    } else {
      $rootScope.nightMode = localStorageService.get("nightMode", "localStorage");
    }
    var auth = $rootScope.$on('$stateChangeStart', function (event, toState) {
      var token = TokenService.isToken();
      $rootScope.keepLoading = true;
      var requireLogin = toState.data.requireLogin;
      var currentUser = null;
      var locked = localStorageService.get("locked", "localStorage") === true ? localStorageService.get("locked", "localStorage") : false;
     /* if(localStorageService.get("currentUser", "localStorage") !== 'undefined' && localStorageService.get("currentUser", "sessionStorage") !== null) {
        currentUser = localStorageService.get("currentUser", "sessionStorage");
      } else */if (typeof localStorageService.get("currentUser", "localStorage") !== 'undefined' && localStorageService.get("currentUser", "localStorage") !== null) {
        currentUser = localStorageService.get("currentUser", "localStorage");
      }
      if (locked === true) {
        if (toState.name !== 'auth.locked') {
          $rootScope.keepLoading = false;
          event.preventDefault();
          $state.go('auth.locked');
        }
        else $rootScope.keepLoading = false;
      } else {
        if (requireLogin === true) {
            AccountsService.checkAdmin(token).then(function () {
              $rootScope.keepLoading = false;
            }, function (response) {
              if (typeof response.headers.authorization !== 'undefined') {
                localStorageService.set("token", response.headers.authorization, /*localStorageService.getStorageType()*/"localStorage");
                $rootScope.keepLoading = false;
              } else {
                event.preventDefault();
                $rootScope.keepLoading = true;
                localStorageService.remove("token", "currentUser");
                $state.go("auth.login");
              }
            });
        } else if (requireLogin === false && requireLogin !== null) {
          if (currentUser !== null) {
            AccountsService.checkAdmin(token).then(function () {
              event.preventDefault();
              if (typeof $rootScope.$state.previous !== 'undefined' && $rootScope.$state.previous !== '' && $rootScope.$state.previous.substr(0,3) === 'app') {
                $rootScope.keepLoading = true;
                $state.go($rootScope.$state.previous);
              } else {
                $rootScope.keepLoading = true;
                $state.go('app.dashboard');
              }
            }, function (response) {
              if (typeof response.headers.authorization !== 'undefined') {
                event.preventDefault();
                localStorageService.set("token", response.headers.authorization, /*localStorageService.getStorageType()*/"localStorage");
                if (typeof $rootScope.$state.previous !== 'undefined') {
                  $rootScope.keepLoading = true;
                  $state.go($rootScope.$state.previous);
                } else {
                  $rootScope.keepLoading = true;
                  $state.go('app.dashboard');
                }
              } else {
                localStorageService.remove("token", "currentUser", "locked");
                $rootScope.keepLoading = false;
              }
            });
          } else {
            if (toState.name === 'auth.locked') {
              $rootScope.keepLoading = true;
              event.preventDefault();
              $state.go('auth.login');
            } else $rootScope.keepLoading = false;
          }
        }
      }
    });

    var unregistered = $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, from) {
      event.targetScope.$watch('$viewContentLoaded', function () {
        angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);
      });
      $rootScope.$state.current = toState;
      $rootScope.specialClass = toState.specialClass;
      $rootScope.$state.previous = from.name;
    });

    var notSuccess = $rootScope.$on('$stateChangeError', function () {
      $state.go('error.page500');
    });

    $rootScope.$on('$destroy', unregistered);
    $rootScope.$on('$destroy', auth);
    $rootScope.$on('$destroy', notSuccess);
  }

})();
