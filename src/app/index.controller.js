(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($translate, tmhDynamicLocale) {
    var vm = this;
    vm.changeLanguage = function (langKey) {
      $translate.use(langKey);
      vm.currentLanguage = langKey;
      tmhDynamicLocale.set(langKey);
    };
    vm.currentLanguage = $translate.proposedLanguage() || $translate.use();
    tmhDynamicLocale.set(vm.currentLanguage);
  }
})();
