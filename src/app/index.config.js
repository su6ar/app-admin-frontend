(function() {
  'use strict';

  angular
    .module('minotaur')
    .config(config);

  /** @ngInject */
  function config($logProvider, treeConfig, toastrConfig, $provide, $translateProvider, $locationProvider, tmhDynamicLocaleProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    treeConfig.defaultCollapsed = treeConfig;

    // Set options third-party lib
    angular.extend(toastrConfig, {
      positionClass: 'toast-top-center',
      allowHtml: true,
      closeButton: true,
      preventDuplicates: false,
      preventOpenDuplicates: false,
      closeHtml: '<button>&times;</button>',
      extendedTimeOut: 1000,
      iconClasses: {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
      },
      messageClass: 'toast-message',
      onHidden: null,
      onShown: null,
      onTap: null,
      progressBar: true,
      tapToDismiss: true,
      templates: {
        toast: 'directives/toast/toast.html',
        progressbar: 'directives/progressbar/progressbar.html'
      },
      timeOut: 10000,
      titleClass: 'toast-title',
      toastClass: 'toast'
    });

    // angular-language
    $translateProvider.useStaticFilesLoader({
      prefix: 'assets/languages/',
      suffix: '.json'
    }).useLocalStorage()
      .preferredLanguage('cs')
      .useSanitizeValueStrategy(null);

    tmhDynamicLocaleProvider.defaultLocale('cs');
    tmhDynamicLocaleProvider.localeLocationPattern('/bower_components/angular-i18n/angular-locale_{{locale}}.js');

    // remove ! hash prefix
    $locationProvider.hashPrefix('');

    $provide.decorator('taTools', ['$delegate', function(taTools){
      taTools.insertVideo.iconclass = 'fab fa-youtube';
      taTools.redo.iconclass = 'fas fa-redo';
      taTools.insertImage.iconclass = 'fas fa-image';
      return taTools;
    }]);

  }

})();
