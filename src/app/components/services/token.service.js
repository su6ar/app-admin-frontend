(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('TokenService', TokenService);

  /** @ngInject */
  function TokenService(localStorageService) {
    this.isToken = function isToken() {
      var token = null;
      /*if(typeof localStorageService.get("token", "sessionStorage") !== 'undefined' && localStorageService.get("token", "sessionStorage") !== null) {
        localStorageService.setStorageType("sessionStorage");
        token = localStorageService.get("token", "sessionStorage");
      } else*/
      if (typeof localStorageService.get("token", "localStorage") !== 'undefined' && localStorageService.get("token", "localStorage") !== null) {
        localStorageService.setStorageType("localStorage");
        token = localStorageService.get("token", "localStorage");
      }
      return token;
    }
  }

})();
