(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('FileCategoriesService', FileCategoriesService);

  /** @ngInject */
  function FileCategoriesService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/file-categories',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (fcyId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/file-categories/' + fcyId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.post = function (fcyName) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/file-categories',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: {
          fcy_name: fcyName
        }
      });
    };

    this.put = function (fcyId, fcyName) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/file-categories/' + fcyId,
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: {
          fcy_name: fcyName
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/file-categories',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: ids
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(mur) {
      list = mur;
    };
  }

})();
