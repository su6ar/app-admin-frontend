(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('ManufacturerService', ManufacturerService);

  /** @ngInject */
  function ManufacturerService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var murList = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/manufacturer',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (id) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/manufacturer/' + id,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/manufacturer',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.put = function (id, murName) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/manufacturer/' + id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: {
          mur_name: murName
        }
      });
    };

    this.post = function (murName) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/manufacturer',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: {
          mur_name: murName
        }
      });
    };

    this.getList = function getList() {
      return murList;
    };

    this.setList = function setList(mur) {
      murList = mur;
    };
  }

})();
