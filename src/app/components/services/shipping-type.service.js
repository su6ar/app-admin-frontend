(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('ShippingTypeService', ShippingTypeService);

  /** @ngInject */
  function ShippingTypeService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var typeList = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/shipping-type',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (id) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/shipping-type/' + id,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/shipping-type',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.put = function (id, dtaName) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/shipping-type/' + id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: {
          dta_name: dtaName
        }
      });
    };

    this.post = function (dtaName) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/shipping-type',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: {
          dta_name: dtaName
        }
      });
    };

    this.getList = function getList() {
      return typeList;
    };

    this.setList = function setList(types) {
      typeList = types;
    };
  }

})();
