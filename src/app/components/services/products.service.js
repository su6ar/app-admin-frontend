(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('ProductsService', ProductsService);

  /** @ngInject */
  function ProductsService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function (ceyId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/products/category?cey_id=' + ceyId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (putId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/products/' + putId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/products',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.put = function (data, putId) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/products/single/' + putId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.post = function (data) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/products',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.updateMultiple = function (data) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/products/multiple',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(items) {
      list = items;
    };
  }

})();
