(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('AlertsService', AlertsService);

  /** @ngInject */
  function AlertsService($timeout, $location, $anchorScroll) {
    this.showAlert = function(alerts, message, type, icon, duration, closeable, closeall, focus) {
      var alert = {
        msg: message,
        type: type,
        timeout: duration,
        icon: icon,
        closeable: closeable,
        closeall: closeall,
        focus: focus
      };

      if (alert.closeall) {
        alerts = [];
      }

      if (alert.focus) {
        $location.hash('alertsPlaceholder');
        $anchorScroll();
      }

      alerts.push(alert);

      $timeout(function() {
        alerts.splice(alerts.indexOf(alert), 1);
      }, alerts[alerts.indexOf(alert)].timeout);

      return alerts;
    };

    this.closeAlert = function(alerts, index) {
      alerts.splice(index, 1);
      return alerts;
    };
  }

})();
