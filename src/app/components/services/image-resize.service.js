(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('ImageResizeService', ImageResizeService);

  /** @ngInject */
  function ImageResizeService($http, PHP_PATH, PHP_IMAGE_CHANGE_CATEGORY_FILENAME) {

    this.post = function (markings, items) {
      return $http({
        method: 'POST',
        url: PHP_PATH + PHP_IMAGE_CHANGE_CATEGORY_FILENAME,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          markings: markings,
          items: items
        }
      });
    };

  }

})();
