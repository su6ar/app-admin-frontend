(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('AccountsService', AccountsService);

  /** @ngInject */
  function AccountsService($http, SERVER, REST_PREFIX, TokenService) {

    this.getToken = function getToken(email, password) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/accounts/auth',
        data: {
          act_email: email,
          rur_password: password
        },
        headers: {
          'Content-Type': 'application/json'
        }
      });
    };

    this.put = function (data) {
      var token = TokenService.isToken();
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/accounts',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.createAccount = function createAccount(email, password, avatar, firstName, lastName) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/accounts/create',
        data: {
          act_email: email,
          rur_password: password,
          amr_avatar: avatar,
          amr_first_name: firstName,
          amr_last_name: lastName
        },
        headers: {
          'Content-Type': 'application/json'
        }
      });
    };

    this.generateCodeRefresh = function generateCodeRefresh(email) {
      return $http({
        method: 'PATCH',
        url: SERVER + REST_PREFIX + '/accounts/forgotpass/' + email
      });
    };

    this.codeRefreshExists = function codeRefreshExists(codeRefresh) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/accounts/updatepass?refreshcode=' + codeRefresh
      });
    };

    this.refreshPassword = function refreshPassword(codeRefresh, password) {
      return $http({
        method: 'PATCH',
        url: SERVER + REST_PREFIX + '/accounts/updatepass?refreshcode=' + codeRefresh + '&newpass=' + password
      });
    };

    this.checkAdmin = function checkAdmin(tok) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/accounts/token',
        headers: {
          'Authorization': 'Bearer ' + tok
        }
      });
    }

  }

})();
