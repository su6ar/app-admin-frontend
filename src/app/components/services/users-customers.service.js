(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('UsersCustomersService', UsersCustomersService);

  /** @ngInject */
  function UsersCustomersService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/users/customers',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (actId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/users/customers/' + actId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.put = function (data, grpId) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/users/customers/group/' + grpId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(items) {
      list = items;
    };
  }

})();
