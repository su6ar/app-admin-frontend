(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('ImageCategoriesService', ImageCategoriesService);

  /** @ngInject */
  function ImageCategoriesService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var icyList = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/image-categories',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (icyId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/image-categories/' + icyId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.post = function (icyName, migMarkings) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/image-categories',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: {
          icy_name: icyName,
          mig_markings: migMarkings
        }
      });
    };

    this.put = function (icyId, icyName, migMarkings) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/image-categories/' + icyId,
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: {
          icy_name: icyName,
          mig_markings: migMarkings
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/image-categories',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: ids
      });
    };

    this.getMarkings = function (icyId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/image-categories/marking/' + icyId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getList = function getList() {
      return icyList;
    };

    this.setList = function setList(mur) {
      icyList = mur;
    };
  }

})();
