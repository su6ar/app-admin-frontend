(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('MenuMainService', MenuMainService);

  /** @ngInject */
  function MenuMainService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/menu/main',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOnly = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/menu/main/only',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (mimId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/menu/main/' + mimId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/menu/main',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.put = function (data) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/menu/main',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.putOne = function (data, mimId) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/menu/main/' + mimId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.post = function (data) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/menu/main',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(items) {
      list = items;
    };
  }

})();
