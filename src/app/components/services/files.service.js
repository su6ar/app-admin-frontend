(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('FilesService', FilesService);

  /** @ngInject */
  function FilesService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/files',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.post = function (fcyId, fiePath, fieName, fieType, fieSize) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/files',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: {
          fcy_id: fcyId,
          fie_path: fiePath,
          fie_name: fieName,
          fie_type: fieType,
          fie_size: fieSize
        }
      });
    };

    this.patch = function (ids, fcyId) {
      return $http({
        method: 'PATCH',
        url: SERVER + REST_PREFIX + '/files/' + fcyId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/files',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(mur) {
      list = mur;
    };
  }

})();
