(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('CompanyService', CompanyService);

  /** @ngInject */
  function CompanyService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/company/contacts',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(items) {
      list = items;
    };
  }

})();
