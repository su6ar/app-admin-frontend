(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('GroupsService', GroupsService);

  /** @ngInject */
  function GroupsService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/groups',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (grpId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/groups/' + grpId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/groups',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.put = function (data, grpId) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/groups/' + grpId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.post = function (data) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/groups',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(items) {
      list = items;
    };
  }

})();
