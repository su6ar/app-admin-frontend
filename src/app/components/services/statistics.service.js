(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('StatisticsService', StatisticsService);

  /** @ngInject */
  function StatisticsService($http, moment, localStorageService, SERVER, REST_PREFIX) {
    var token = null;
    if(typeof localStorageService.get("token", "sessionStorage") !== 'undefined' && localStorageService.get("token", "sessionStorage") !== null) {
      localStorageService.setStorageType("sessionStorage");
      token = localStorageService.get("token", "sessionStorage");
    } else if (typeof localStorageService.get("token", "localStorage") !== 'undefined' && localStorageService.get("token", "localStorage") !== null) {
      localStorageService.setStorageType("localStorage");
      token = localStorageService.get("token", "localStorage");
    }

    this.getTimeFromOption = function (option) {
      var time;
      switch (option)
      {
        case 0:
          time = moment().startOf('month');
          break;
        case 1:
          time = moment().startOf('day');
          break;
        case 2:
          time = moment().endOf('day');
          break;
        case 3:
          time = moment().subtract(1, 'day').startOf('day');
          break;
        case 4:
          time = moment().subtract(1, 'day').endOf('day');
          break;
        case 5:
          time = moment().subtract(6, 'days').startOf('day');
          break;
        case 6:
          time = moment().subtract(29, 'days').startOf('day');
          break;
        case 7:
          time = moment().subtract(1, 'month').startOf('month');
          break;
        default:
          time = moment().subtract(1, 'month').endOf('month');
          break;
      }
      return time;
    };

    this.dashboardStats = function dashboardStats(startDate, endDate) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/dashboard?date_from=' + startDate + '&date_to=' + endDate,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    }
  }

})();
