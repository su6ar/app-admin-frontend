(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('MarkingService', MarkingService);

  /** @ngInject */
  function MarkingService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var migList = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/markings',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getList = function getList() {
      return migList;
    };

    this.setList = function setList(mur) {
      migList = mur;
    };
  }

})();
