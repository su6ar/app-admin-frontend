(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('PaymentsService', PaymentsService);

  /** @ngInject */
  function PaymentsService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var paymentList = [];

    this.get = function get() {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/payment',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function(pmtId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/payment/' + pmtId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function(ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/payment',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: ids
      });
    };

    this.put = function(pmtId, cashOnDelivery, poyName, addPercentageOottPrice, price) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/payment/' + pmtId,
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: {
          cash_on_delivery: cashOnDelivery,
          poy_name: poyName,
          add_percentage_oott_price: addPercentageOottPrice,
          price: price
        }
      });
    };

    this.post = function(cashOnDelivery, poyName, addPercentageOottPrice, price) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/payment',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: {
          cash_on_delivery: cashOnDelivery,
          poy_name: poyName,
          add_percentage_oott_price: addPercentageOottPrice,
          price: price
        }
      });
    };

    this.getList = function getList() {
      return paymentList;
    };

    this.setList = function setList(payments) {
      paymentList = payments;
    };
  }

})();
