(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('ShippingService', ShippingService);

  /** @ngInject */
  function ShippingService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var deliveriesList = [];

    this.delete = function (spg_ids, sht_ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/shipping',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: {
          spg_id: spg_ids,
          sht_id: sht_ids
        }
      });
    };

    this.get = function get() {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/shipping',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function(spg_id) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/shipping/' + spg_id,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.post = function post(dtaId, payments, showOnDefault, showTillMaxWeight, shyName, freePriceAbove, sumWeightPrice, items) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/shipping',
        data: {
          dta_id: dtaId,
          payments: payments,
          show_on_default: showOnDefault,
          show_till_max_weight: showTillMaxWeight,
          shy_name: shyName,
          free_price_above: freePriceAbove,
          sum_weight_price: sumWeightPrice,
          items: items
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.postOne = function (shyId, weightMin, weightMax, price) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/shipping/' + shyId,
        data: {
          weight_min: weightMin,
          weight_max: weightMax,
          price: price
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.putOne = function (spgId,
      dtaId,
      payments,
      showOnDefault,
      showTillMaxWeight,
      shyName,
      freePriceAbove,
      sumWeightPrice) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/shipping/spg/' + spgId,
        data: {
          dta_id: dtaId,
          payments: payments,
          show_on_default: showOnDefault,
          show_till_max_weight: showTillMaxWeight,
          shy_name: shyName,
          free_price_above: freePriceAbove,
          sum_weight_price: sumWeightPrice
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.putSht = function (shtId, shyId, weightMin, weightMax, price) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/shipping/sht/' + shtId,
        data: {
          shy_id: shyId,
          weight_min: weightMin,
          weight_max: weightMax,
          price: price
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getList = function getList() {
      return deliveriesList;
    };

    this.setList = function setList(deliveries) {
      deliveriesList = deliveries;
    };
  }

})();
