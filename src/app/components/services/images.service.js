(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('ImagesService', ImagesService);

  /** @ngInject */
  function ImagesService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var iaeList = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/images',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getByIcyId = function (icyId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/images/category/' + icyId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/images',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.post = function (icyId, iaePath, iaeName, iaeType, iaeSize, iaeMarking) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/images',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: {
          icy_id: icyId,
          iae_path: iaePath,
          iae_name: iaeName,
          iae_type: iaeType,
          iae_size: iaeSize,
          iae_marking: iaeMarking
        }
      });
    };

    this.patch = function (ids, icyId) {
      return $http({
        method: 'PATCH',
        url: SERVER + REST_PREFIX + '/images/category/' + icyId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.getList = function getList() {
      return iaeList;
    };

    this.setList = function setList(mur) {
      iaeList = mur;
    };
  }

})();
