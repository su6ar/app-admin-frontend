(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('CategoriesService', CategoriesService);

  /** @ngInject */
  function CategoriesService($http, SERVER, REST_PREFIX, TokenService) {
    var token = TokenService.isToken();
    var list = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/categories',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (ceyId) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/categories/' + ceyId,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.delete = function (ids) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/categories',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: ids
      });
    };

    this.put = function (items) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/categories',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: items
      });
    };

    this.putOne = function (ceyId, data) {
      return $http({
        method: 'PUT',
        url: SERVER + REST_PREFIX + '/categories/' + ceyId,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.post = function (data) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/categories',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
        data: data
      });
    };

    this.getList = function getList() {
      return list;
    };

    this.setList = function setList(items) {
      list = items;
    };
  }

})();
