(function() {
  'use strict';

  angular
    .module('minotaur')
    .service('OrdersService', OrdersService);

  /** @ngInject */
  function OrdersService($http, SERVER, REST_PREFIX, TokenService, $filter) {
    var token = TokenService.isToken();
    var $translation = $filter('translate');
    var ordersList = [];

    this.get = function () {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/orders',
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.getOne = function (id) {
      return $http({
        method: 'GET',
        url: SERVER + REST_PREFIX + '/orders/' + id,
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
    };

    this.update = function update(orders, status, sendEmail) {
      return $http({
        method: 'PATCH',
        url: SERVER + REST_PREFIX + '/orders?status=' + status + '&sendemail=' + sendEmail,
        headers: {
          'Authorization' : 'Bearer ' + token
        },
        data: orders
      });
    };

    this.postNote = function postNote(actId, oneContent, title, odrId) {
      return $http({
        method: 'POST',
        url: SERVER + REST_PREFIX + '/orders/' + odrId + '/note',
        headers: {
          'Authorization' : 'Bearer ' + token
        },
        data: {
          act_id: actId,
          one_content: oneContent,
          title: title
        }
      });
    };

    this.delete = function (orders) {
      return $http({
        method: 'DELETE',
        url: SERVER + REST_PREFIX + '/orders',
        headers: {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        data: orders
      });
    };

    this.getStatusList = function getStatusList() {
      var keys = [0,1,2,3,4];
      var statusList = [];
      angular.forEach(keys, function(key) {
        statusList.push($translation("Order.Shipping.State." + key));
      });
      return statusList;
    };

    this.getList = function getList() {
      return ordersList;
    };

    this.setList = function setList(orders) {
      ordersList = orders;
    };
  }

})();
