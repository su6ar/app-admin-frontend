(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('minotaurTileMinimize', minotaurTileMinimize);

  /** @ngInject */
  function minotaurTileMinimize($filter) {
    var $translation = $filter('translate');

    return {
      restrict: 'EA',
      template: '<i class="fa fa-angle-up"></i>' + $translation('Global.DropdownTools.MINIMIZE'),
      link: function (scope, element) {
        var tile = element.parents('.tile');
        element.on('click', function(){

          if (tile.hasClass('collapsed')) {
            element[0].innerHTML = '<i class="fa fa-angle-up"></i>' + $translation('Global.DropdownTools.MINIMIZE')
          } else {
            element[0].innerHTML = '<i class="fa fa-angle-down"></i>' + $translation('Global.DropdownTools.EXPAND')
          }

          tile.toggleClass('collapsed');
          tile.children().not('.tile-header').slideToggle(150);

        });
      }
    };
  }

})();
