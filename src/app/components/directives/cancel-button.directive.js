(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('cancelButton', cancelButton);

  /** @ngInject */
  function cancelButton( ) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        var $parent = element.parent();
        element.on('click', function() {
          $parent.children().toggleClass('d-none');
          var $minWeight = $parent.parent().find('td[data-attr="weight-min"]');
          var $maxWeight = $minWeight.next();
          var $price = $maxWeight.next();

          $minWeight.find('span').removeClass('d-none');
          $maxWeight.find('span').removeClass('d-none');
          $price.find('span').removeClass('d-none');

          $minWeight.find('.form-group').html('');
          $maxWeight.find('.form-group').html('');
          $price.find('.form-group').html('');
        });
      }
    };
  }

})();
