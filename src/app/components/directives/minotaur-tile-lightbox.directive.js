(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('minotaurTileLightbox', minotaurTileLightbox);

  /** @ngInject */
  function minotaurTileLightbox($filter) {
    var $translation = $filter('translate');
    return {
      restrict: 'EA',
      template: '<i class="fa fa-magnet"></i>' + $translation('Global.DropdownTools.OPEN_IN_LIGHTBOX'),
      link: function (scope, element) {
        var tile = element.parents('.tile');
        element.magnificPopup({
          items: {
            src: tile[0],
            type: 'inline'
          },
          closeBtnInside: false
        });
      }
    };
  }

})();
