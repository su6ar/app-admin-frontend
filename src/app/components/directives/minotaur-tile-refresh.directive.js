(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('minotaurTileRefresh', minotaurTileRefresh);

  /** @ngInject */
  function minotaurTileRefresh($timeout, cfpLoadingBar,$filter) {
    var $translation = $filter('translate');
    return {
      restrict: 'EA',
      template: '<i class="fa fa-sync-alt"></i>' + $translation('Global.DropdownTools.REFRESH'),
      link: function (scope, element) {
        var tile = element.parents('.tile');
        element.on('click', function(){
          tile.addClass('loading');
          cfpLoadingBar.start();

          $timeout(function(){
            tile.removeClass('loading');
            cfpLoadingBar.complete();
          },3000)
        });
      }
    };
  }

})();
