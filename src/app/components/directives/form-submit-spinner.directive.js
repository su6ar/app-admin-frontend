(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('formSubmitSpinner', formSubmitSpinner);

  /** @ngInject */
  function formSubmitSpinner($http, $filter) {
    var $translation = $filter('translate');
    var template = '<span class="spinner"><span class="spinner-icon"></span></span>';
    return {
      restrict: 'E',
      template: template,
      link: function ($scope, element) {
        var $button = element.parent();
        var text = $button.text().trim();
        var buttonHtml = $button.html();
        $scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        $scope.$watch($scope.isLoading, function (isLoading) {
          if(!isLoading) {
            element.hide();
            $button.html(buttonHtml.substring(0, buttonHtml.indexOf(text)).trim() + $translation("Global.Button." + text));
            $button.removeClass('btn-loading');
          } else {
            $button.html(template);
            element.show();
            $button.addClass('btn-loading');
          }
        });
      }
    };
  }

})();
