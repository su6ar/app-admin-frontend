(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('addWeightRange', addWeightRange);

  /** @ngInject */
  function addWeightRange($compile) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        var $parent = element.parents('.form-group');
        element.on('click', function(e) {
          var index = parseInt($parent.find('div[data-weight-range]:last').attr("data-weight-range")) + 1;
          $parent.append('<hr class="line-dashed line-full">\n' +
            '<div data-weight-range="'+(index)+'">\n' +
            '                <div class="row">\n' +
            '                  <div class="col-sm-2 text-right">\n' +
            '                    <div class="mb-10">\n' +
            '                      <label class="control-label kominexpres-label">{{ \'Shipping.Title.MIN_WEIGHT\' | translate }}: </label>\n' +
            '                    </div>\n' +
            '                    <div class="mb-10">\n' +
            '                      <label class="control-label kominexpres-label">{{ \'Shipping.Title.MAX_WEIGHT\' | translate }}: </label>\n' +
            '                    </div>\n' +
            '                    <div class="mb-10">\n' +
            '                      <label class="control-label kominexpres-label">{{ \'Shipping.Title.PRICE\' | translate }}: </label>\n' +
            '                    </div>\n' +
            '                  </div>\n' +
            '                  <div class="col-sm-10">\n' +
            '                    <div class="mb-10">\n' +
            '                      <div touch-spin ng-model="ctrl.weightMin['+(index)+']" options="{min: 0, max: 9999999, postfix: \'g\', decimals: 0, step: 1}"></div>\n' +
            '                    </div>\n' +
            '                    <div class="mb-10">\n' +
            '                      <div touch-spin ng-model="ctrl.weightMax['+(index)+']" options="{min: 0, max: 9999999, postfix: \'g\', decimals: 0, step: 1}"></div>\n' +
            '                    </div>\n' +
            '                    <div class="mb-10">\n' +
            '                      <div touch-spin ng-model="ctrl.price['+(index)+']" options="{min: 0, max: 9999999, postfix: \'Kč\', decimals: 2, step: 0.01}"></div>\n' +
            '                    </div>\n' +
            '                  </div>\n' +
            '                </div>\n' +
            '              </div>');
          $compile($parent.find('div[data-weight-range]:last'))(scope);
          e.preventDefault();
        });
      }
    };
  }

})();
