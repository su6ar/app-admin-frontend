(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('actionTools', actionTools);

  /** @ngInject */
  function actionTools($filter) {
    return {
      restrict: 'C',
      template: '<div class="btn-group" uib-dropdown>\n' +
      '<button id="btn-append-to-body" type="button" class="btn btn-primary" uib-dropdown-toggle>\n' +
      $filter('translate')('Global.Title.ACTIONS') + ' <span class="caret"></span>\n' +
      '</button>\n' +
      '<ul class="dropdown-menu" uib-dropdown-menu role="menu">\n' +
      '</ul>\n' +
      '</div>'
    };
  }

})();
