(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('updateButton', updateButton);

  /** @ngInject */
  function updateButton($compile) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        var $parent = element.parent();
        var $minWeight = $parent.parent().find('td[data-attr="weight-min"]');
        var $maxWeight = $minWeight.next();
        var $price = $maxWeight.next();
        element.on('click', function(e) {
          var minWeight = parseFloat(element.attr('data-weight-min'));
          var maxWeight = parseFloat(element.attr('data-weight-max'));
          var price = parseFloat(element.attr('data-price'));
          angular.element(".update-label").each(function() {
            this.parent().parent().find("span").toggleClass("d-none")            ;
            this.parents("tr").find("td:last").children().toggleClass("d-none");
            this.remove();
          });

          $parent.children().toggleClass('d-none');
          $minWeight.children('span').addClass('d-none');
          $maxWeight.children('span').addClass('d-none');
          $price.children('span').addClass('d-none');
          $minWeight.find('.form-group').html('<label class="update-label">\n' +
            '                        <input type="number" class="form-control input-sm" ng-model="ctrl.updateWeightMin" min="0" value="'+minWeight+'" ng-value="'+minWeight+'">\n' +
            '                        (g)\n' +
            '                      </label>');
          $maxWeight.find('.form-group').html('<label class="update-label">\n' +
            '                        <input type="number" class="form-control input-sm" ng-model="ctrl.updateWeightMax" min="1" value="'+maxWeight+'" ng-value="'+maxWeight+'">\n' +
            '                        (g)\n' +
            '                      </label>');
          $price.find('.form-group').html('<label class="update-label">\n' +
            '                        <input type="number" class="form-control input-sm" ng-model="ctrl.updatePrice" min="0" value="'+price+'" ng-value="'+price+'">\n' +
            '                        (Kč)\n' +
            '                      </label>');
          $compile($parent.parent().find('.form-group'))(scope);
          scope.ctrl.updateWeightMin = minWeight;
          scope.ctrl.updateWeightMax = maxWeight;
          scope.ctrl.updatePrice = price;
          e.preventDefault();
        });
      }
    };
  }

})();
