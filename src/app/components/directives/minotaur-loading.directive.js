(function() {
  'use strict';

  angular
    .module('minotaur')
    .directive('minotaurLoading', minotaurLoading);

  /** @ngInject */
  function minotaurLoading($rootScope, $timeout, cfpLoadingBar) {
    return {
      restrict: 'E',
      controller: MinotaurLoadingController,
      controllerAs: 'loading',
      bindToController: true,
      link: function (scope, element) {
        if (!element.hasClass('hide')){
          element.addClass('animate');
        }
        scope.$on('$stateChangeStart', function(event, toState, toParams, from) {
          if (from.name !== 'auth.locked') {
            if (element.hasClass('hide') && toState.specialClass === 'core') {
              element.removeClass('hide');
            }
            cfpLoadingBar.start();
          }
        });
        scope.$on('$stateChangeSuccess', function (event) {
          event.targetScope.$watch('$viewContentLoaded', function () {
            $timeout(function () {
              cfpLoadingBar.complete();
              element.addClass('hide');
            }, 1000);
          });
        });
        scope.$watch('$root.keepLoading', function() {
          if ($rootScope.keepLoading !== true) {
            $timeout(function () {
              cfpLoadingBar.complete();
              element.addClass('hide');
            }, 1000);
          }
        });
      }
    };

    /** @ngInject */
    function MinotaurLoadingController() {

    }
  }

})();
