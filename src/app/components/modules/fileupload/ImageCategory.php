<?php
/**
 * Created by Daniel Bill
 * Date: 18.07.2018
 * Time: 22:55
 */

class ImageCategory
{
    /**
     * @var integer
     */
    public $icyId;
    /**
     * @var string
     */
    public $icyName;

    /**
     * ImageCategory constructor.
     * @param $icyId
     * @param $icyName
     */
    public function __construct($icyId, $icyName)
    {
        $this->icyId = $icyId;
        $this->icyName = $icyName;
    }

    /**
     * @return int
     */
    public function getIcyId(): int
    {
        return $this->icyId;
    }

    /**
     * @return string
     */
    public function getIcyName(): string
    {
        return $this->icyName;
    }
}