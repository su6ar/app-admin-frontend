<?php
/**
 * Created by Daniel Bill
 * Date: 21.07.2018
 * Time: 00:32
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization');
header('Access-Control-Allow-Methods: POST, OPTIONS');

require __DIR__ . '/vendor/autoload.php';
require 'Global.php';

use Gumlet\ImageResize;

$body = file_get_contents('php://input');
$json = json_decode($body, TRUE);

try
{
    stream_context_set_default( [
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
        ],
    ]);
    foreach ($json["items"] as $image)
    {
        $imageResize = new ImageResize($PATH_TO_IMAGE . $image["iae_path"] . "_" . $MARKING_ORIGINAL . "." . $image["iae_type"]);
        foreach ($json["markings"] as $marking)
        {
            if (($marking["mig_marking"] == $MARKING_ORIGINAL) ||
                (strpos(get_headers($PATH_TO_IMAGE . $image["iae_path"] . "_" . $marking["mig_marking"] . "." . $image["iae_type"],1)[0],"200")))
            {
                continue;
            }
            if ($marking["mig_max_height"] !== NULL && $marking["mig_max_width"] !== NULL)
            {
                $imageResize->resizeToBestFit($marking["mig_max_width"], $marking["mig_max_height"]);
            }
            if ($marking["mig_max_height"] === NULL && $marking["mig_max_width"] !== NULL)
            {
                $imageResize->resizeToWidth($marking["mig_max_width"]);
            }
            if ($marking["mig_max_height"] !== NULL && $marking["mig_max_width"] === NULL)
            {
                $imageResize->resizeToHeight($marking["mig_max_height"]);
            }
            $imageResize->save($PATH_TO_IMAGE . $image["iae_path"] . "_" . $marking["mig_marking"] . "." . $image["iae_type"]);
        }
    }
    http_response_code(201);
}
catch (Exception $e)
{
    http_response_code(400);
    echo json_encode([
        "status_code" => 400,
        "status_message" => $e->getMessage(),
        "response" => $e->getMessage()
    ]);
}

