<?php
/**
 * Created by Daniel Bill
 * Date: 18.07.2018
 * Time: 22:56
 */

class Image
{
    public const ICY_ID = "icy_id";
    public const ICY_NAME = "icy_name";

    /**
     * @var integer
     */
    public $iaeId;
    /**
     * @var integer
     */
    public $icyId;
    /**
     * @var string
     */
    public $iaePath;
    /**
     * @var string
     */
    public $iaeName;
    /**
     * @var string
     */
    public $iaeType;
    /**
     * @var double
     */
    public $iaeSize;

    /**
     * @return int
     */
    public function getIaeId(): int
    {
        return $this->iaeId;
    }

    /**
     * @param int $iaeId
     * @return Image
     */
    public function setIaeId(int $iaeId): Image
    {
        $this->iaeId = $iaeId;
        return $this;
    }

    /**
     * @return int
     */
    public function getIcyId(): int
    {
        return $this->icyId;
    }

    /**
     * @param int $icyId
     * @return Image
     */
    public function setIcyId(int $icyId): Image
    {
        $this->icyId = $icyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getIaePath(): string
    {
        return $this->iaePath;
    }

    /**
     * @param string $iaePath
     * @return Image
     */
    public function setIaePath(string $iaePath): Image
    {
        $this->iaePath = $iaePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getIaeName(): string
    {
        return $this->iaeName;
    }

    /**
     * @param string $iaeName
     * @return Image
     */
    public function setIaeName(string $iaeName): Image
    {
        $this->iaeName = $iaeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getIaeType(): string
    {
        return $this->iaeType;
    }

    /**
     * @param string $iaeType
     * @return Image
     */
    public function setIaeType(string $iaeType): Image
    {
        $this->iaeType = $iaeType;
        return $this;
    }

    /**
     * @return float
     */
    public function getIaeSize(): float
    {
        return $this->iaeSize;
    }

    /**
     * @param float $iaeSize
     * @return Image
     */
    public function setIaeSize(float $iaeSize): Image
    {
        $this->iaeSize = $iaeSize;
        return $this;
    }
}
