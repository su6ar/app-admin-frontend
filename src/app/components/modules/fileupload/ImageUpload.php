<?php
/**
 * Created by Daniel Bill
 * Date: 18.07.2018
 * Time: 22:56
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization');
header('Access-Control-Allow-Methods: POST, OPTIONS');

require __DIR__ . '/vendor/autoload.php';
require 'Global.php';
require 'Image.php';
require 'ImageCategory.php';

use Gumlet\ImageResize;


if ($_SERVER['REQUEST_METHOD'] != 'POST')
{
    http_response_code(200);
    return true;
}

if (!empty($_FILES))
{
    try
    {
        stream_context_set_default( [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);
        require 'FileVariables.php';
        $icy = new ImageCategory($_POST[Image::ICY_ID], $_POST[Image::ICY_NAME]);
        $iae = new Image();
        $iae->setIaeName(substr($FILE_NAME, 0, strripos($FILE_NAME, ".")))
            ->setIaeSize($FILE["size"] / 1000)
            ->setIaeType(substr($FILE_NAME, strripos($FILE_NAME, ".") + 1));
        $migMarking = explode(',', $_POST["mig_marking"]);
        $migMaxHeight = explode(',', $_POST["mig_max_height"]);
        $migMaxWidth = explode(',', $_POST["mig_max_width"]);
        $hashPath = bin2hex(random_bytes(20));
        for ($i = 0; $i < count($migMarking); $i++)
        {
            while (strpos(get_headers($PATH_TO_IMAGE . $hashPath . "_" . $migMarking[$i] . "." . $iae->getIaeType(), 1)[0], "200"))
            {
                $hashPath = bin2hex(random_bytes(20));
                $i = 0;
            }
        }
        $iae->setIaePath($hashPath);
        $tempPath = $FILE['tmp_name'];
        move_uploaded_file($tempPath, $PATH_TO_IMAGE . $iae->getIaePath() . "_" . $MARKING_ORIGINAL . "." . $iae->getIaeType());

        $image = new ImageResize($PATH_TO_IMAGE . $iae->getIaePath() . "_" . $MARKING_ORIGINAL . "." . $iae->getIaeType());
        $image->quality_jpg = 100;
        $image->gamma(false);
        for ($i = 0; $i < count($migMarking); $i++)
        {
            if ($migMarking[$i] == $MARKING_ORIGINAL)
            {
                continue;
            }
            if ($migMaxHeight[$i] != -1 && $migMaxWidth[$i] != -1)
            {
                $image->resizeToBestFit($migMaxWidth[$i], $migMaxHeight[$i]);
            }
            if ($migMaxHeight[$i] == -1 && $migMaxWidth[$i] != -1)
            {
                $image->resizeToWidth($migMaxWidth[$i]);
            }
            if ($migMaxHeight[$i] != -1 && $migMaxWidth[$i] == -1)
            {
                $image->resizeToHeight($migMaxHeight[$i]);
            }
            $image->save($PATH_TO_IMAGE . $iae->getIaePath() . "_" . $migMarking[$i] . "." . $iae->getIaeType());
        }
        echo json_encode([
            "status_code" => 200,
            "status_message" => "OK!",
            "iae_path" => $iae->getIaePath(),
            "iae_name" => $iae->getIaeName(),
            "iae_type" => $iae->getIaeType(),
            "iae_size" => $iae->getIaeSize(),
            "icy_id" => $icy->getIcyId(),
            "mig_marking" => $migMarking
        ]);
    }
    catch (Exception $e)
    {
        http_response_code(400);
        echo json_encode([
            "status_code" => $e->getCode(),
            "status_message" => $e->getMessage(),
            "response" => $e->getMessage()
        ]);
    }
}
else
{
    http_response_code(400);
    echo json_encode([
        "status_code" => 400,
        "status_message" => "Bad request!",
        "response" => "No file"
    ]);
}
