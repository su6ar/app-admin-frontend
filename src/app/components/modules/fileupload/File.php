<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 0:13
 */

class File
{
    public const FCY_ID = "fcy_id";
    public const FCY_NAME = "fcy_name";

    /**
     * @var integer
     */
    public $fieId;
    /**
     * @var integer|null
     */
    public $fcyId;
    /**
     * @var string
     */
    public $fieName;
    /**
     * @var string
     */
    public $fiePath;
    /**
     * @var string
     */
    public $fieType;
    /**
     * @var double
     */
    public $fieSize;

    /**
     * @return int
     */
    public function getFieId(): int
    {
        return $this->fieId;
    }

    /**
     * @param int $fieId
     * @return File
     */
    public function setFieId(int $fieId): File
    {
        $this->fieId = $fieId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFcyId(): ?int
    {
        return $this->fcyId;
    }

    /**
     * @param int|null $fcyId
     * @return File
     */
    public function setFcyId(?int $fcyId): File
    {
        $this->fcyId = $fcyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieName(): string
    {
        return $this->fieName;
    }

    /**
     * @param string $fieName
     * @return File
     */
    public function setFieName(string $fieName): File
    {
        $this->fieName = $fieName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiePath(): string
    {
        return $this->fiePath;
    }

    /**
     * @param string $fiePath
     * @return File
     */
    public function setFiePath(string $fiePath): File
    {
        $this->fiePath = $fiePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieType(): string
    {
        return $this->fieType;
    }

    /**
     * @param string $fieType
     * @return File
     */
    public function setFieType(string $fieType): File
    {
        $this->fieType = $fieType;
        return $this;
    }

    /**
     * @return float
     */
    public function getFieSize(): float
    {
        return $this->fieSize;
    }

    /**
     * @param float $fieSize
     * @return File
     */
    public function setFieSize(float $fieSize): File
    {
        $this->fieSize = $fieSize;
        return $this;
    }
}