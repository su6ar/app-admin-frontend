<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 0:11
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

require __DIR__ . '/vendor/autoload.php';
require 'Global.php';
require 'FileVariables.php';
require 'File.php';


if ($_SERVER['REQUEST_METHOD'] != 'POST')
{
    http_response_code(200);
    return true;
}

if (!empty($_FILES))
{
    try
    {
        stream_context_set_default( [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);
        require 'FileVariables.php';
        $fie = new File();
        $fie->setFcyId($_POST["fcy_id"])
            ->setFieName(substr($FILE_NAME, 0, strripos($FILE_NAME, ".")))
            ->setFieSize($FILE["size"] / 1000)
            ->setFieType(substr($FILE_NAME, strripos($FILE_NAME, ".") + 1));
        $hashPath = bin2hex(random_bytes(20));
        while (strpos(get_headers($PATH_TO_FILE . $hashPath . "." . $fie->getFieType(),1)[0], "200"))
        {
            $hashPath = bin2hex(random_bytes(20));
        }
        $fie->setFiePath($hashPath);
        $tempPath = $FILE['tmp_name'];
        move_uploaded_file($tempPath, $PATH_TO_FILE . $fie->getFiePath() . "." . $fie->getFieType());

        echo json_encode([
            "status_code" => 200,
            "status_message" => "OK!",
            "fcy_id" => $fie->getFcyId(),
            "fie_path" => $fie->getFiePath(),
            "fie_name" => $fie->getFieName(),
            "fie_type" => $fie->getFieType(),
            "fie_size" => $fie->getFieSize()
        ]);
    }
    catch (Exception $e)
    {
        http_response_code(400);
        echo json_encode([
            "status_code" => $e->getCode(),
            "status_message" => $e->getMessage(),
            "response" => $e->getMessage()
        ]);
    }
}
else
{
    http_response_code(400);
    echo json_encode([
        "status_code" => 400,
        "status_message" => "Bad request!",
        "response" => "No file"
    ]);
}
