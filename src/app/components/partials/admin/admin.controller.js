(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('AdminController', AdminController);

  /** @ngInject */
  function AdminController(localStorageService, AccountsService, TokenService, $state, $rootScope) {
    var vm = this;
    var currentUser = localStorageService.get("currentUser", localStorageService.getStorageType());
    var token = TokenService.isToken();
    if (typeof currentUser === 'undefined' || currentUser === null) {
      if (token !== null) {
        AccountsService.checkAdmin(token).then(function (admin) {
          localStorageService.set("currentUser", admin.data, /*localStorageService.getStorageType()*/"localStorage");
          $rootScope.currentUser = admin.data;
        });
      }
    } else $rootScope.currentUser = currentUser;

    vm.night_mode = $rootScope.nightMode === 1;
    vm.logOut = function() {
      $rootScope.currentUser = undefined;
      localStorageService.clearAll('.*', 'sessionStorage');
      localStorageService.clearAll('.*', 'localStorage');
      $state.go('auth.login');
    };

    vm.lockMe = function() {
      localStorageService.remove("token", localStorageService.getStorageType());
      localStorageService.set("locked", true, "localStorage");
      $state.go('auth.locked');
    };

    vm.nightMode = function () {
      localStorageService.set("nightMode", $rootScope.nightMode===1?0:1, "localStorage");
      $rootScope.nightMode = localStorageService.get("nightMode", "localStorage");
      vm.night_mode = $rootScope.nightMode === 1;
    };
  }

})();
