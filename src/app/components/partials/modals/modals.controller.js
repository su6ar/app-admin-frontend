(function() {
  'use strict';

  angular
    .module('minotaur')
    .controller('ModalInstanceController', ModalInstanceController)
    .controller('ModalsDeleteMultipleController', ModalsDeleteMultipleController)
    .controller('ModalsChangeMultipleEmailController', ModalsChangeMultipleEmailController)
    .controller('ModalsDeleteSingleController', ModalsDeleteSingleController)
    .controller('ModalsDeleteSingleSpgController', ModalsDeleteSingleSpgController)
    .controller('ModalsDeleteMultipleSpgController', ModalsDeleteMultipleSpgController)
    .controller('ModalsChangeImageCategoryMultipleController', ModalsChangeImageCategoryMultipleController)
    .controller('ModalsChangeFileCategoryMultipleController', ModalsChangeFileCategoryMultipleController)
    .controller('ModalChangeMultipleProductsController', ModalChangeMultipleProductsController)
    .controller('ModalsChangeUsersController', ModalsChangeUsersController);

  /** @ngInject */
  function ModalsDeleteMultipleController($uibModalInstance, that, items, ceyId, service, $filter, toastr) {
    var vm = this;
    var $translate = $filter('translate');
    vm.items = items;

    var getOnlyIds = function (items) {
      var ids = [];
      for (var i = 0; i < items.length; i++) {
        ids.push(items[i].id);
      }
      return ids;
    };

    vm.ok = function () {
      var its = getOnlyIds(vm.items);
      service.delete(its).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
      }, function (response) {
        toastr.error(response.data.message, $translate('Global.Alert.DELETE_ERROR'));
      }).finally(function () {
        if (angular.isDefined(ceyId) && ceyId !== null) {
          service.get(ceyId).then(function (response) {
            service.setList(response.data);
            that.items = service.getList();
          });
        } else {
          service.get().then(function (response) {
            service.setList(response.data);
            that.items = service.getList();
          });
        }
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalChangeMultipleProductsController($uibModalInstance, items) {
    var vm = this;
    vm.items = items;

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalsChangeUsersController($uibModalInstance, that, items, service, item, $filter, toastr) {
    var vm = this;
    var $translate = $filter('translate');
    vm.items = items;

    var getOnlyIds = function (items) {
      var ids = [];
      for (var i = 0; i < items.length; i++) {
        ids.push(items[i].id);
      }
      return ids;
    };

    vm.ok = function () {
      var its = getOnlyIds(vm.items);
      service.put(its, item).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
      }, function (response) {
        toastr.error(response.data.message, $translate('Global.Alert.ERROR'));
      }).finally(function () {
        service.get().then(function (response) {
          service.setList(response.data);
          that.items = service.getList();
        });
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalsChangeMultipleEmailController($uibModalInstance, status, items, attri, service, $filter, toastr) {
    var vm = this;
    var $translate = $filter('translate');
    vm.items = items;
    vm.status = status;
    var getOnlyIds = function (items) {
      var ids = [];
      for (var i = 0; i < items.length; i++) {
        ids.push(items[i].id);
      }
      return ids;
    };

    vm.ok = function () {
      var its = getOnlyIds(vm.items);
      service.update(its, status, false).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
        var list = service.getList();
        for (var i = 0; i < items.length; i++) {
          list[items[i].index][attri] = status;
        }
        service.setList(list);
      }, function (response) {
        toastr.error(response.data.message, $translate('Global.Alert.DELETE_ERROR'));
      });
    };

    vm.email = function () {
      var its = getOnlyIds(vm.items);
      service.update(its, status, true).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
        var list = service.getList();
        for (var i = 0; i < items.length; i++) {
          list[items[i].index][attri] = status;
        }
        service.setList(list);
      }, function (response) {
        toastr.error(response.data.message, $translate('Global.Alert.DELETE_ERROR'));
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalsDeleteSingleController($uibModalInstance, that, id, ceyId, service, toastr, $filter) {
    var vm = this;
    var $translate = $filter('translate');

    vm.id = id;

    vm.ok = function () {
      service.delete([id]).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
      }, function (response) {
        toastr.error(response.data.message, $translate('Global.Alert.DELETE_ERROR'));
      }).finally(function () {
        if (angular.isDefined(ceyId) && ceyId !== null) {
          service.get(ceyId).then(function (response) {
            service.setList(response.data);
            that.items = service.getList();
          });
        } else {
          service.get().then(function (response) {
            service.setList(response.data);
            that.items = service.getList();
          });
        }
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalsChangeFileCategoryMultipleController($uibModalInstance, that, items, name, fcyId, FilesService, toastr, $filter) {
    var vm = this;
    var $translate = $filter('translate');

    vm.items = items;
    vm.name = name;

    vm.ok = function () {
      var ids = [];
      for (var i = 0; i <  items.length; i++)
      {
        ids.push(items[i].id);
      }

      FilesService.patch(ids, fcyId).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
        FilesService.get().then(function (response) {
          FilesService.setList(response.data);
          that.items = FilesService.getList();
        });
      }, function (response) {
        toastr.error(response.response, $translate('Global.Alert.CHANGE_ERROR'));
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalsChangeImageCategoryMultipleController($uibModalInstance, ImageResizeService, that, items, name, icyId, ImageCategoriesService, ImagesService, toastr, $filter) {
    var vm = this;
    var $translate = $filter('translate');

    vm.items = items;
    vm.name = name;

    vm.ok = function () {
      ImageCategoriesService.getMarkings(icyId).then(function (response) {
        var markings = response.data;
        ImageResizeService.post(markings, vm.items).then(function () {
          var ids = [];
          for (var i = 0; i <  items.length; i++)
          {
            ids.push(items[i].id);
          }
          ImagesService.patch(ids, icyId).then(function () {
            vm.cancel();
            toastr.success($translate('Global.Alert.SUCCESS'));
            ImagesService.get().then(function (response) {
              ImagesService.setList(response.data);
              that.items = ImagesService.getList();
            });
          }, function (response) {
            toastr.error(response.data.message, $translate('Global.Alert.CHANGE_ERROR'));
          });
        }, function (response) {
          toastr.error(response.response, $translate('Global.Alert.CHANGE_ERROR'));
        });
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalsDeleteMultipleSpgController($uibModalInstance, that, items, service, toastr, $filter) {
    var vm = this;
    var $translate = $filter('translate');

    vm.items = items;

    vm.ok = function () {
      service.delete(items["spgs"], items["shts"]).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
        service.get().then(function (response) {
          service.setList(response.data);
          that.shipping = service.getList();
        });
      }, function (response) {
        toastr.error(response.data.message, $translate('Global.Alert.DELETE_ERROR'));
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

  function ModalInstanceController($uibModalInstance, items, title) {
    var vm = this;

    vm.items = items;
    vm.title = title;

    vm.cancel = function () {
      $uibModalInstance.close();
    };

  }

  function ModalsDeleteSingleSpgController($uibModalInstance, that, spg_id, sht_id, service, toastr, $filter) {
    var vm = this;
    var $translate = $filter('translate');

    vm.id = spg_id === null ? sht_id : spg_id;

    vm.ok = function () {
      service.delete([spg_id], [sht_id]).then(function () {
        vm.cancel();
        toastr.success($translate('Global.Alert.SUCCESS'));
        service.get().then(function (response) {
          service.setList(response.data);
          that.shipping = service.getList();
        });
      }, function (response) {
        toastr.error(response.data.message, $translate('Global.Alert.DELETE_ERROR'));
      });
    };

    vm.cancel = function () {
      $uibModalInstance.close();
    };
  }

})();
