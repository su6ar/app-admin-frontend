(function() {
  'use strict';

  angular
    .module('minotaur')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      //app
      .state('app', {
        url: '/app',
        template: '<div ui-view></div>',
        abstract: true,
        data: {
          requireLogin: true
        }
      })
      //dashboard
      .state('app.dashboard', {
        url: '/dashboard',
        templateUrl: 'app/pages/dashboard/dashboard.html',
        controller: 'DashboardController',
        controllerAs: 'dashboard',
        resolve: {
          dashboardStats: function (StatisticsService, localStorageService, moment) {
            var startDate = "startDate";
            var endDate = "endDate";
            var startDateValue = angular.isUndefined(localStorageService.get(startDate)) || localStorageService.get(startDate) === null ? moment().startOf("day") : (localStorageService.get(startDate) < 10 ? StatisticsService.getTimeFromOption(localStorageService.get(startDate)).unix(): localStorageService.get(startDate));
            var endDateValue = angular.isUndefined(localStorageService.get(endDate)) || localStorageService.get(endDate) === null ? moment().endOf("day") : (localStorageService.get(endDate) < 10 ? StatisticsService.getTimeFromOption(localStorageService.get(endDate)).unix() : localStorageService.get(endDate));

            return StatisticsService.dashboardStats(startDateValue, endDateValue).then(function (response) {
              return response.data;
            });
          }
        }
      })
      .state('app.profile', {
        url: '/profile',
        templateUrl: 'app/pages/profile/profile.html',
        controller: 'ProfileController',
        controllerAs: 'ctrl'
      })
      .state('app.orders', {
        url: '/orders',
        template: '<div ui-view></div>'
      })
      .state('app.orders.list', {
        url: '/',
        templateUrl: 'app/pages/orders/orders.html',
        controller: 'OrdersController',
        controllerAs: 'ctrl'
      })
      .state('app.orders.detail', {
        url: '/:id',
        templateUrl: 'app/pages/orders-detail/orders-detail.html',
        controller: 'OrdersDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.shipping', {
        url: '/shipping',
        template: '<div ui-view></div>'
      })
      .state('app.shipping.list', {
        url: '/',
        templateUrl: 'app/pages/shipping/shipping.html',
        controller: 'ShippingController',
        controllerAs: 'ctrl'
      })
      .state('app.shipping.detail', {
        url: '/detail/:id',
        templateUrl: 'app/pages/shipping-detail/shipping-detail.html',
        controller: 'ShippingDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.shipping.add', {
        url: '/add',
        templateUrl: 'app/pages/shipping-add/shipping-add.html',
        controller: 'ShippingAddController',
        controllerAs: 'ctrl'
      })
      .state('app.shipping.type', {
        url: '/type',
        template: '<div ui-view></div>'
      })
      .state('app.shipping.type.list', {
        url: '/list',
        templateUrl: 'app/pages/shipping-type/shipping-type.html',
        controller: 'ShippingTypeController',
        controllerAs: 'ctrl'
      })
      .state('app.shipping.type.add', {
        url: '/add',
        templateUrl: 'app/pages/shipping-type-add/shipping-type-add.html',
        controller: 'ShippingTypeAddController',
        controllerAs: 'ctrl'
      })
      .state('app.shipping.type.detail', {
        url: '/detail/:id',
        templateUrl: 'app/pages/shipping-type-detail/shipping-type-detail.html',
        controller: 'ShippingTypeDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.payment', {
        url: '/payment',
        template: '<div ui-view></div>'
      })
      .state('app.payment.list', {
        url: '/list',
        templateUrl: 'app/pages/payment/payment.html',
        controller: 'PaymentController',
        controllerAs: 'ctrl'
      })
      .state('app.payment.add', {
        url: '/add',
        templateUrl: 'app/pages/payment-add/payment-add.html',
        controller: 'PaymentAddController',
        controllerAs: 'ctrl'
      })
      .state('app.payment.detail', {
        url: '/detail/:id',
        templateUrl: 'app/pages/payment-detail/payment-detail.html',
        controller: 'PaymentDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.images', {
        url: '/images',
        template: '<div ui-view></div>'
      })
      .state('app.images.list', {
        url: '/list',
        templateUrl: 'app/pages/images/images.html',
        controller: 'ImagesController',
        controllerAs: 'ctrl'
      })
      .state('app.images.add', {
        url: '/add',
        templateUrl: 'app/pages/images-add/images-add.html',
        controller: 'ImagesAddController',
        controllerAs: 'forms',
        specialClass: 'force-header-sm header-sm'
      })
      .state('app.images.category', {
        url: '/category',
        template: '<div ui-view></div>'
      })
      .state('app.images.category.list', {
        url: '/list',
        templateUrl: 'app/pages/images-category/images-category.html',
        controller: 'ImagesCategoryController',
        controllerAs: 'ctrl'
      })
      .state('app.images.category.add', {
        url: '/add',
        templateUrl: 'app/pages/images-category-add/images-category-add.html',
        controller: 'ImagesCategoryAddController',
        controllerAs: 'ctrl'
      })
      .state('app.images.category.detail', {
        url: '/detail/:id',
        templateUrl: 'app/pages/images-category-detail/images-category-detail.html',
        controller: 'ImagesCategoryDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.files', {
        url: '/files',
        template: '<div ui-view></div>'
      })
      .state('app.files.list', {
        url: '/list',
        templateUrl: 'app/pages/files/files.html',
        controller: 'FilesController',
        controllerAs: 'ctrl'
      })
      .state('app.files.add', {
        url: '/add',
        templateUrl: 'app/pages/files-add/files-add.html',
        controller: 'FilesAddController',
        controllerAs: 'forms',
        specialClass: 'force-header-sm header-sm'
      })
      .state('app.files.category', {
        url: '/category',
        template: '<div ui-view></div>'
      })
      .state('app.files.category.list', {
        url: '/list',
        templateUrl: 'app/pages/files-category/files-category.html',
        controller: 'FilesCategoryController',
        controllerAs: 'ctrl'
      })
      .state('app.files.category.add', {
        url: '/add',
        templateUrl: 'app/pages/files-category-add/files-category-add.html',
        controller: 'FilesCategoryAddController',
        controllerAs: 'ctrl'
      })
      .state('app.files.category.detail', {
        url: '/detail/:id',
        templateUrl: 'app/pages/files-category-detail/files-category-detail.html',
        controller: 'FilesCategoryDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.categories', {
        url: '/categories',
        template: '<div ui-view></div>'
      })
      .state('app.categories.list', {
        url: '/list',
        templateUrl: 'app/pages/categories/categories.html',
        controller: 'CategoriesController',
        controllerAs: 'ctrl'
      })
      .state('app.categories.add', {
        url: '/add',
        templateUrl: 'app/pages/categories-add/categories-add.html',
        controller: 'CategoriesAddController',
        controllerAs: 'ctrl'
      })
      .state('app.categories.detail', {
        url: '/detail/:cey_id',
        templateUrl: 'app/pages/categories-detail/categories-detail.html',
        controller: 'CategoriesDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.parameters', {
        url: '/parameters',
        template: '<div ui-view></div>'
      })
      .state('app.parameters.list', {
        url: '/list',
        templateUrl: 'app/pages/parameters/parameters.html',
        controller: 'ParametersController',
        controllerAs: 'ctrl'
      })
      .state('app.parameters.add', {
        url: '/add',
        templateUrl: 'app/pages/parameters-add/parameters-add.html',
        controller: 'ParametersAddController',
        controllerAs: 'ctrl'
      })
      .state('app.parameters.detail', {
        url: '/detail/:prr_id',
        templateUrl: 'app/pages/parameters-detail/parameters-detail.html',
        controller: 'ParametersDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.parameters.group', {
        url: '/group',
        template: '<div ui-view></div>'
      })
      .state('app.parameters.group.list', {
        url: '/list',
        templateUrl: 'app/pages/parameters-group/parameters-group.html',
        controller: 'ParametersGroupController',
        controllerAs: 'ctrl'
      })
      .state('app.parameters.group.add', {
        url: '/add',
        templateUrl: 'app/pages/parameters-group-add/parameters-group-add.html',
        controller: 'ParametersGroupAddController',
        controllerAs: 'ctrl'
      })
      .state('app.parameters.group.detail', {
        url: '/detail/:pgp_id',
        templateUrl: 'app/pages/parameters-group-detail/parameters-group-detail.html',
        controller: 'ParametersGroupDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.manufacturer', {
        url: '/manufacturer',
        template: '<div ui-view></div>'
      })
      .state('app.manufacturer.list', {
        url: '/list',
        templateUrl: 'app/pages/manufacturer/manufacturer.html',
        controller: 'ManufacturerController',
        controllerAs: 'ctrl'
      })
      .state('app.manufacturer.add', {
        url: '/add',
        templateUrl: 'app/pages/manufacturer-add/manufacturer-add.html',
        controller: 'ManufacturerAddController',
        controllerAs: 'ctrl'
      })
      .state('app.manufacturer.detail', {
        url: '/detail/:id',
        templateUrl: 'app/pages/manufacturer-detail/manufacturer-detail.html',
        controller: 'ManufacturerDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.groups', {
        url: '/groups',
        template: '<div ui-view></div>'
      })
      .state('app.groups.list', {
        url: '/list',
        templateUrl: 'app/pages/groups/groups.html',
        controller: 'GroupsController',
        controllerAs: 'ctrl'
      })
      .state('app.groups.add', {
        url: '/add',
        templateUrl: 'app/pages/groups-add/groups-add.html',
        controller: 'GroupsAddController',
        controllerAs: 'ctrl'
      })
      .state('app.groups.detail', {
        url: '/detail/:grp_id',
        templateUrl: 'app/pages/groups-detail/groups-detail.html',
        controller: 'GroupsDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.users', {
        url: '/users',
        template: '<div ui-view></div>'
      })
      .state('app.users.customers', {
        url: '/customers',
        template: '<div ui-view></div>'
      })
      .state('app.users.customers.list', {
        url: '/list',
        templateUrl: 'app/pages/users-customers/users-customers.html',
        controller: 'UsersCustomersController',
        controllerAs: 'ctrl'
      })
      .state('app.users.customers.detail', {
        url: '/detail/:act_id',
        templateUrl: 'app/pages/users-customers-detail/users-customers-detail.html',
        controller: 'UsersCustomersDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.users.admins', {
        url: '/admins',
        template: '<div ui-view></div>'
      })
      .state('app.users.admins.list', {
        url: '/list',
        templateUrl: 'app/pages/users-admins/users-admins.html',
        controller: 'UsersAdminsController',
        controllerAs: 'ctrl'
      })
      .state('app.users.admins.detail', {
        url: '/detail/:act_id',
        templateUrl: 'app/pages/users-admins-detail/users-admins-detail.html',
        controller: 'UsersAdminsDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.articles', {
        url: '/articles',
        template: '<div ui-view></div>'
      })
      .state('app.articles.list', {
        url: '/list',
        templateUrl: 'app/pages/articles/articles.html',
        controller: 'ArticlesController',
        controllerAs: 'ctrl'
      })
      .state('app.articles.detail', {
        url: '/detail/:ate_id',
        templateUrl: 'app/pages/articles-detail/articles-detail.html',
        controller: 'ArticlesDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.articles.add', {
        url: '/add',
        templateUrl: 'app/pages/articles-add/articles-add.html',
        controller: 'ArticlesAddController',
        controllerAs: 'ctrl'
      })
      .state('app.products', {
        url: '/products',
        template: '<div ui-view></div>'
      })
      .state('app.products.list', {
        url: '/list/:cey_id',
        templateUrl: 'app/pages/products/products.html',
        controller: 'ProductsController',
        controllerAs: 'ctrl'
      })
      .state('app.products.add', {
        url: '/add',
        templateUrl: 'app/pages/products-add/products-add.html',
        controller: 'ProductsAddController',
        controllerAs: 'ctrl'
      })
      .state('app.products.detail', {
        url: '/detail/:put_id',
        templateUrl: 'app/pages/products-detail/products-detail.html',
        controller: 'ProductsDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.products.multipleUpdate', {
        url: '/multiple/update',
        templateUrl: 'app/pages/products-multiple-update/products-multiple-update.html',
        controller: 'ProductsMultipleUpdateController',
        controllerAs: 'ctrl',
        params: {
          ids: null
        }
      })
      .state('app.metaKeywords', {
        url: '/meta-keywords',
        template: '<div ui-view></div>'
      })
      .state('app.metaKeywords.list', {
        url: '/list',
        templateUrl: 'app/pages/meta-keywords/meta-keywords.html',
        controller: 'MetaKeywordsController',
        controllerAs: 'ctrl'
      })
      .state('app.metaKeywords.detail', {
        url: '/detail/:id',
        templateUrl: 'app/pages/meta-keywords-detail/meta-keywords-detail.html',
        controller: 'MetaKeywordsDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.metaKeywords.add', {
        url: '/add',
        templateUrl: 'app/pages/meta-keywords-add/meta-keywords-add.html',
        controller: 'MetaKeywordsAddController',
        controllerAs: 'ctrl'
      })
      .state('app.menu', {
        url: '/menu',
        template: '<div ui-view></div>'
      })
      .state('app.menu.main', {
        url: '/main',
        template: '<div ui-view></div>'
      })
      .state('app.menu.main.list', {
        url: '/list',
        templateUrl: 'app/pages/menu-main/menu-main.html',
        controller: 'MenuMainController',
        controllerAs: 'ctrl'
      })
      .state('app.menu.main.detail', {
        url: '/detail/:mim_id',
        templateUrl: 'app/pages/menu-main-detail/menu-main-detail.html',
        controller: 'MenuMainDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.menu.main.add', {
        url: '/add',
        templateUrl: 'app/pages/menu-main-add/menu-main-add.html',
        controller: 'MenuMainAddController',
        controllerAs: 'ctrl'
      })
      .state('app.menu.footer', {
        url: '/footer',
        template: '<div ui-view></div>'
      })
      .state('app.menu.footer.list', {
        url: '/list',
        templateUrl: 'app/pages/menu-footer/menu-footer.html',
        controller: 'MenuFooterController',
        controllerAs: 'ctrl'
      })
      .state('app.menu.footer.detail', {
        url: '/detail/:mim_id',
        templateUrl: 'app/pages/menu-footer-detail/menu-footer-detail.html',
        controller: 'MenuFooterDetailController',
        controllerAs: 'ctrl'
      })
      .state('app.menu.footer.add', {
        url: '/add',
        templateUrl: 'app/pages/menu-footer-add/menu-footer-add.html',
        controller: 'MenuFooterAddController',
        controllerAs: 'ctrl'
      })
      //auth pages (login, sign up, forgot password)
      .state('auth', {
        url: '/auth',
        template: '<div ui-view></div>',
        abstract: true,
        data: {
          requireLogin: false
        }
      })
      //login
      .state('auth.login', {
        url: '/login',
        templateUrl: 'app/pages/auth-login/auth-login.html',
        controller: 'LoginController',
        controllerAs: 'ctrl',
        parent: 'auth',
        specialClass: 'core'
      })
      //register
      .state('auth.signup', {
        url: '/signup',
        templateUrl: 'app/pages/auth-signup/auth-signup.html',
        controller: 'SignupController',
        controllerAs: 'ctrl',
        parent: 'auth',
        specialClass: 'core'
      })
      //forgot password
      .state('auth.forgot-password', {
        url: '/forgot-password',
        templateUrl: 'app/pages/auth-forgot-password/auth-forgot-password.html',
        controller: 'ForgotPasswordController',
        controllerAs: 'ctrl',
        parent: 'auth',
        specialClass: 'core'
      })
      //refresh password
      .state('auth.refresh-password', {
        url: '/refresh-password/:codeRefresh',
        templateUrl: 'app/pages/auth-refresh-password/auth-refresh-password.html',
        controller: 'RefreshPasswordController',
        controllerAs: 'ctrl',
        parent: 'auth',
        specialClass: 'core'
      })
      //lock
      .state('auth.locked', {
        url: '/locked',
        templateUrl: 'app/pages/auth-locked/auth-locked.html',
        controller: 'LockedController',
        controllerAs: 'ctrl',
        parent: 'auth',
        specialClass: 'core'
      })
      //error pages (404, 500, offline)
      .state('error', {
        url: '/error',
        template: '<div ui-view></div>',
        data: {
          requireLogin: null
        }
      })
      //404
      .state('error.page404', {
        url: '/error-404',
        templateUrl: 'app/pages/error-404/error-404.html',
        controller: 'Page404Controller',
        controllerAs: 'ctrl',
        parent: 'error',
        specialClass: 'core'
      })
      //500
      .state('error.page500', {
        url: '/error-500',
        templateUrl: 'app/pages/error-500/error-500.html',
        controller: 'Page500Controller',
        controllerAs: 'ctrl',
        parent: 'error',
        specialClass: 'core'
      })
      //offline
      .state('error.offline', {
        url: '/error-offline',
        templateUrl: 'app/pages/error-offline/error-offline.html',
        controller: 'PageOfflineController',
        controllerAs: 'ctrl',
        parent: 'error',
        specialClass: 'core'
      });

    $urlRouterProvider.otherwise('/auth/login');
  }

})();
